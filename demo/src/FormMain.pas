unit FormMain;

interface

uses
  XAF.Process, XAF.RTTI, XAF.Anim, XAF.Notify,
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, xadGridBase, xadGridLevels, xadGridViews,
  StdCtrls, ImgList, Vcl.ExtCtrls, Vcl.Buttons;

type

  TfrmMain = class(TForm)
    GridProcess: TxagGrid;
    LevelProcess: TxagStandardLevel;
    xagViewDetail1: TxagViewDetail;
    clmID: TxagColumn;
    clmState: TxagColumn;
    clmLog: TxagColumn;
    clmProgress: TxagColumn;
    Button1: TButton;
    ImageList1: TImageList;
    clmGroup: TxagColumn;
    Button2: TButton;
    Button3: TButton;
    XP_Handle1: TXP_Handle;
    ListBox1: TListBox;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    ComboBox1: TComboBox;
    XA_EasingPreview1: TXA_EasingPreview;
    Button9: TButton;
    Button10: TButton;
    XN_Control1: TXN_Control;
    Memo1: TMemo;
    Button11: TButton;
    ComboBox2: TComboBox;
    XN_Notify1: TXN_Notify;
    Button12: TButton;
    procedure XP_Handle1ChangeQueue(Sender: TObject; AQueue: TXP_QueueList;
      AItem: TXP_QueueItem; AAction: TListNotification);
    procedure XP_Handle1ChangeProcess(Sender: TObject; AProc: TXP_Process);
    procedure Button1Click(Sender: TObject);
    procedure clmIDGetValueItem(Sender: TxagBaseColumn;
      ACell: TxagBaseDataItemCell; var AValue: Variant);
    procedure clmStateGetValueItem(Sender: TxagBaseColumn;
      ACell: TxagBaseDataItemCell; var AValue: Variant);
    procedure clmGroupGetValueItem(Sender: TxagBaseColumn;
      ACell: TxagBaseDataItemCell; var AValue: Variant);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure XN_Control1Buttons0Click(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure XN_Notify1ControlOptionsTitleClick(Sender: TObject);
    procedure Button12Click(Sender: TObject);
  private
    DefaultEasing: TXA_EasingType;
    FObject: IXR_Object;
    test: TXN_Window;
    { Private declarations }
    procedure WMWindowPosChanging(var Message: TWMWindowPosChanging); message WM_WINDOWPOSCHANGING;
    procedure DoMaximizeAnim(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;

    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses
  XAF.Handle,
  Math, XAF.Lib.Test, TypInfo, Winapi.GDIPOBJ, Winapi.GDIPAPI, Types;

{$R *.dfm}

procedure TfrmMain.Button10Click(Sender: TObject);
begin
  XN_Notify1.Show;
end;

procedure TfrmMain.Button11Click(Sender: TObject);
var
  w: TXN_Window;
begin
  w := TXN_Window.Create(Self);
//  w.Control.Assign(XN_Control1);
//  m.WindowParent := Self.WindowHandle;
  w.Show(3000);
  test := w;
//  XN_Control1.InDesign := not XN_Control1.InDesign;
end;

procedure TfrmMain.Button12Click(Sender: TObject);
var
  w: TXN_Window;
begin
  w := TXN_Window.Create(Self);
  w.Control.Assign(XN_Control1);
//  m.WindowParent := Self.WindowHandle;
  w.Show(3000);
  test := w;
//  XN_Control1.InDesign := not XN_Control1.InDesign;
end;

procedure TfrmMain.Button1Click(Sender: TObject);
begin
  Manager.DefaultQueue.Start;
  Manager.DefaultQueue.AddProcessAsync(TISP_Test.Create(10));
end;

procedure TfrmMain.Button2Click(Sender: TObject);
begin
  Manager.Queue['hola'].Start;
  Manager.Queue['hola'].AddProcessAsync(TISP_Test.Create(10));
end;

procedure TfrmMain.Button3Click(Sender: TObject);
begin
  Manager.DefaultQueue.Start;
  Manager.DefaultQueue.AddProcess(TISP_Test.Create(10));
end;

procedure TfrmMain.Button4Click(Sender: TObject);
var
  I: Integer;
begin
  ListBox1.Clear;
  for I := 0 to FObject.PropertiesCount - 1 do
  begin
    ListBox1.Items.Add(FObject.PropertiesFromIndex[i].Name);
  end;
end;

procedure TfrmMain.Button5Click(Sender: TObject);
begin
  FObject.Properties['Caption'].Value := 'asdasdasd';
end;

procedure TfrmMain.Button6Click(Sender: TObject);
begin
  FObject.Properties['Color'].Value := clSilver;
end;

procedure TfrmMain.Button7Click(Sender: TObject);
var
  a: TXA_Anim;
  v: Integer;
  o: TWinControl;
begin
  a := TXA_Anim.Create(Self);
  a.AutoFree := True;
  o := Self;
  a.ScopeComponent := o;
  a.ScopePropertyName := 'Left';
  a.DefaultEasing := TXA_EasingType(Combobox1.ItemIndex);
  v := o.Left;
  a.Values.AddValue(v, 0);
  a.Values.AddValue(0, v);
  a.Start;
end;

procedure TfrmMain.Button8Click(Sender: TObject);
var
  a: TXA_Anim;
  o: TfrmMain;
  v: Integer;
begin
  a := TXA_Anim.Create(Self);
  a.AutoFree := True;
  o := Self;
  a.ScopeComponent := o;
  a.ScopePropertyName := 'Color';
  a.ValueType := xvtColor;
  a.DefaultEasing := TXA_EasingType(Combobox1.ItemIndex);
  v := o.Color;
  a.Values.AddValue(v, clRed);
  a.Values.AddValue(clRed, clBlue);
  a.Values.AddValue(clBlue, v);
  a.Start;
end;

procedure TfrmMain.Button9Click(Sender: TObject);

  procedure Anim(AProp: String; AFrom, ATo: Integer);
  var
    a: TXA_Anim;
    o: TfrmMain;
  begin
    a := TXA_Anim.Create(Self);
    a.AutoFree := True;
    o := Self;
    a.ScopeComponent := o;
    a.ScopePropertyName := AProp;
    a.DefaultEasing := TXA_EasingType(Combobox1.ItemIndex);
    a.Values.AddValue(AFrom, ATo, 0.1);
    a.OnComplete := DoMaximizeAnim;
    a.Start;
  end;

begin

  Anim('Left', Left, 0);
  Anim('Top', Top, 0);
  Anim('Width', Width, Screen.WorkAreaWidth);
  Anim('Height', Height, Screen.WorkAreaHeight);

end;

procedure TfrmMain.clmGroupGetValueItem(Sender: TxagBaseColumn;
  ACell: TxagBaseDataItemCell; var AValue: Variant);
begin
  if not Assigned(ACell.Item.Data) then Exit;
  AValue := TXP_QueueItem(ACell.Item.Data).Process.QueueGroup;
end;

procedure TfrmMain.clmIDGetValueItem(Sender: TxagBaseColumn;
  ACell: TxagBaseDataItemCell; var AValue: Variant);
begin
  if not Assigned(ACell.Item.Data) then Exit;
  AValue := TXP_QueueItem(ACell.Item.Data).Process.ID.Id;
end;

procedure TfrmMain.clmStateGetValueItem(Sender: TxagBaseColumn;
  ACell: TxagBaseDataItemCell; var AValue: Variant);
begin
  if not Assigned(ACell.Item.Data) then Exit;
  AValue := Integer(TXP_QueueItem(ACell.Item.Data).Process.Status);
end;

procedure TfrmMain.ComboBox1Change(Sender: TObject);
begin
  XA_EasingPreview1.Easing := TXA_EasingType(ComboBox1.ItemIndex);
end;

procedure TfrmMain.ComboBox2Change(Sender: TObject);
begin
  TXN_Manager.Default.Align := TXN_AlignWindow(ComboBox2.ItemIndex);
end;

constructor TfrmMain.Create(AOwner: TComponent);
begin
  inherited;
  FObject := TXR_Object.Create(Self);
  LevelProcess.DataController.Clear;
  DefaultEasing := xeLinearTween;
end;

procedure TfrmMain.DoMaximizeAnim(Sender: TObject);
begin
  WindowState := wsMaximized;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  idx: Integer;
  I: TXA_EasingType;
  I2: TXN_AlignWindow;
begin
//  m.Align := awRightTop;
  idx := -1;
  ComboBox1.Clear;
  ComboBox2.Clear;
  for I := Low(TXA_EasingType) to High(TXA_EasingType) do
  begin
    if I = DefaultEasing then
      idx := Ord(I);
    ComboBox1.Items.Add(GetEnumName(TypeInfo(TXA_EasingType), Ord(I)));
  end;
  ComboBox1.ItemIndex := idx;
  idx := -1;
  for I2 := Low(TXN_AlignWindow) to High(TXN_AlignWindow) do
  begin
    if I2 = TXN_Manager.Default.Align then
      idx := Ord(I2);
    ComboBox2.Items.Add(GetEnumName(TypeInfo(TXN_AlignWindow), Ord(I2)));
  end;
  ComboBox2.ItemIndex := idx;
end;

procedure TfrmMain.Memo1Change(Sender: TObject);
begin
  XN_Control1.Options.Title.Text := Memo1.Text;
end;

procedure TfrmMain.WMWindowPosChanging(var Message: TWMWindowPosChanging);
var
  R: TRect;
begin
  inherited;
//  if not Assigned(m) then
//    Exit;
  R.TopLeft := ClientToScreen(Point(0, 0));
  R.BottomRight := ClientToScreen(Point(ClientWidth, ClientHeight));
//  m.CustomRect := R;
end;

procedure TfrmMain.XN_Control1Buttons0Click(Sender: TObject);
begin
ShowMessage('asd');
end;

procedure TfrmMain.XN_Notify1ControlOptionsTitleClick(Sender: TObject);
begin
ShowMessage('aasdasd');
end;

procedure TfrmMain.XP_Handle1ChangeProcess(Sender: TObject; AProc: TXP_Process);
var
  n: Integer;
  itm: TxagBaseDataItem;
begin
  n := LevelProcess.DataController.IndexOfData(AProc.Process.QueueItem);
  if n > -1 then
  begin
    LevelProcess.BeginUpdate;
    try
      itm := LevelProcess.DataController.Items[n];
      itm.Cells[clmProgress].Value := AProc.DisplayPosition * 100 / AProc.DisplayMax;
      itm.Cells[clmLog].Value := AProc.LogLine;
    finally
      LevelProcess.EndUpdate;
    end;
  end;
end;

procedure TfrmMain.XP_Handle1ChangeQueue(Sender: TObject; AQueue: TXP_QueueList;
  AItem: TXP_QueueItem; AAction: TListNotification);
var
  n: Integer;
begin
  case AAction of
    lnAdded:
    begin
      LevelProcess.BeginUpdate;
      try
        LevelProcess.DataController.Add.Data := AItem;
      finally
        LevelProcess.EndUpdate;
      end;
    end;
    lnExtracted,
    lnDeleted: begin
      LevelProcess.BeginUpdate;
      try
        n := LevelProcess.DataController.IndexOfData(AItem);
        if n > -1 then
          LevelProcess.DataController.Delete(n);
      finally
        LevelProcess.EndUpdate;
      end;
    end;
  end;
end;

end.
