unit XAF.RTTI;

interface

uses
  Messages, Windows, Controls, SysUtils, Classes, TypInfo;

type

  IXR_Property = interface;
  IXR_PropertyInstance = interface;
  IXR_Class = interface;

  TXR_ArrayProperties = array of IXR_Property;

  TXR_Value = record
  strict private
    FValue: Variant;
  private
    function GetAsString: String;
    function GetAsInteger: Integer;
  public
    property Value: Variant read FValue write FValue;
    class operator Implicit(AValue: Variant): TXR_Value;
    class operator Implicit(AValue: TXR_Value): Variant;

    property AsString: String read GetAsString;
    property AsInteger: Integer read GetAsInteger;

  end;

  IXR_Class = interface
    function GetPropertiesFromIndex(AIndex: Integer): IXR_Property;
    function GetProperty(AName: String): IXR_Property;
    function GetClass: TClass;
    function GetPropertiesCount: Integer;
    property &Class: TClass read GetClass;
    property PropertiesFromIndex[AIndex: Integer]: IXR_Property read GetPropertiesFromIndex;
    property Properties[AName: String]: IXR_Property read GetProperty;
    property PropertiesCount: Integer read GetPropertiesCount;
  end;

  IXR_Object = interface(IXR_Class)
    function GetPropertiesFromIndexInstance(AIndex: Integer): IXR_PropertyInstance;
    function GetPropertyInstance(AName: String): IXR_PropertyInstance;
    function GetObject: TObject;
    property &Object: TObject read GetObject;
    property PropertiesFromIndex[AIndex: Integer]: IXR_PropertyInstance read GetPropertiesFromIndexInstance;
    property Properties[AName: String]: IXR_PropertyInstance read GetPropertyInstance;
  end;

  IXR_Property = interface
    ['{82A4ACA2-E236-4DF0-B7B5-A44B47294202}']
    function GetName: String;
    function GetValue(AInstance: TObject): TXR_Value;
    function GetIsReadable: Boolean;
    function GetIsWriteable: Boolean;
    procedure SetValue(AInstance: TObject; const AValue: TXR_Value);
    property Name: String read GetName;
    property IsReadable: Boolean read GetIsReadable;
    property IsWriteable: Boolean read GetIsWriteable;
    property Value[AInstance: TObject]: TXR_Value read GetValue write SetValue;
  end;

  IXR_PropertyInstance = interface(IXR_Property)
    ['{E5B31C2A-CDDE-4014-965E-4AD278BFA239}']
    function GetValueInstance: TXR_Value;
    procedure SetValueInstance(const Value: TXR_Value);
    function GetObject: IXR_Object;
    procedure SetObject(const Value: IXR_Object);
    property &Object: IXR_Object read GetObject write SetObject;
    property Value: TXR_Value read GetValueInstance write SetValueInstance;
  end;

  TXR_Property = class(TInterfacedObject, IXR_Property)
  private
    FPropInfo: TPropInfo;
    function GetPropInfo: TPropInfo;
  protected
    constructor CreateInstance(AClass: IXR_Class; APropertyName: String); overload;
    constructor CreateInstance(APropInfo: TPropInfo); overload;
    property PropInfo: TPropInfo read GetPropInfo;
    { IIR_Property }
    function GetName: string;
    function GetValue(AInstance: TObject): TXR_Value;
    procedure SetValue(AInstance: TObject; const AValue: TXR_Value);
    function GetIsReadable: Boolean;
    function GetIsWriteable: Boolean;
  public
    property Value[AInstance: TObject]: TXR_Value read GetValue write SetValue;
    class function Create(AObject: IXR_Class; APropertyName: String): IXR_Property; overload;
    class function Create(APropInfo: TPropInfo): TXR_Property; overload;
  end;

  TXR_PropertyInstance = class(TXR_Property, IXR_PropertyInstance)
  private
    FObject: IXR_Object;
    function GetObject: IXR_Object;
    procedure SetObject(const Value: IXR_Object);
  protected
    { IXR_PropertyInstance }
    function GetValueInstance: TXR_Value;
    procedure SetValueInstance(const Value: TXR_Value);
    constructor CreateInstance(AObject: IXR_Object; APropInfo: TPropInfo); overload;
  public
    { IXR_PropertyInstance }
    property Value: TXR_Value read GetValueInstance write SetValueInstance;
    property &Object: IXR_Object read GetObject write SetObject;
    class function Create(AObject: IXR_Object; APropInfo: TPropInfo): TXR_Property; overload;
  end;

  TXR_Class = class(TInterfacedObject, IXR_Class)
  private
    FProperties: TXR_ArrayProperties;
    FPropertiesLoaded: Boolean;
    FClass: TClass;
    procedure LoadProperties;
  protected
    { IXR_Class }
    function CreateProperty(AProp: TPropInfo): IXR_Property; virtual;
    function GetProperty(AName: String): IXR_Property;
    function GetPropertiesFromIndex(AIndex: Integer): IXR_Property;
    function GetPropertiesCount: Integer;
    function GetClass: TClass;
    constructor CreateInstance(AClass: TClass);
    function ReadProperties: TXR_ArrayProperties;
    function ReadPropList(out PropList: PPropList): Integer; virtual;
  public
    class function Create(AClass: TClass): IXR_Class;
    destructor Destroy; override;
    property &Class: TClass read GetClass;

    { IXR_Class }
    property Properties[AName: String]: IXR_Property read GetProperty;
    property PropertiesFromIndex[AIndex: Integer]: IXR_Property read GetPropertiesFromIndex;
    property PropertiesCount: Integer read GetPropertiesCount;
  end;

  TXR_Object = class(TXR_Class, IXR_Object)
  private
    FObject: TObject;
    function GetPropertiesFromIndexInstance(AIndex: Integer): IXR_PropertyInstance;
    function GetPropertyInstance(AName: String): IXR_PropertyInstance;
  protected
    { IXR_Object }
    function GetObject: TObject;
    constructor CreateInstance(AObject: TObject);
    function CreateProperty(AProp: TPropInfo): IXR_Property; override;
    function ReadPropList(out PropList: PPropList): Integer; override;
  public
    class function Create(AObject: TObject): IXR_Object;
    { IXR_Object }
    property &Object: TObject read GetObject;
    property PropertiesFromIndex[AIndex: Integer]: IXR_PropertyInstance read GetPropertiesFromIndexInstance;
    property Properties[AName: String]: IXR_PropertyInstance read GetPropertyInstance;
  end;

implementation

uses
  VarUtils, Variants;

{ TXR_Object }

class function TXR_Class.Create(AClass: TClass): IXR_Class;
begin
  Result := TXR_Class.CreateInstance(AClass);
end;

constructor TXR_Class.CreateInstance(AClass: TClass);
begin
  FClass := AClass;
  FPropertiesLoaded := False;
end;

destructor TXR_Class.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FProperties) - 1 do
    FProperties := nil;
  inherited;
end;

function TXR_Class.GetClass: TClass;
begin
  Result := FClass;
end;

function TXR_Class.GetPropertiesCount: Integer;
begin
  LoadProperties;
  Result := Length(FProperties);
end;

function TXR_Class.GetPropertiesFromIndex(AIndex: Integer): IXR_Property;
begin
  LoadProperties;
  Result := FProperties[AIndex];
end;

function TXR_Class.GetProperty(AName: String): IXR_Property;
var
  I: Integer;
begin
  LoadProperties;
  Result := nil;
  for I := 0 to Length(FProperties) - 1 do
    if LowerCase(FProperties[I].Name) = LowerCase(AName) then
    begin
      Result := FProperties[I];
      Exit;
    end;

  raise Exception.Create('Property ''' + AName + ''' not exists.');

end;

function TXR_Class.ReadPropList(out PropList: PPropList): Integer;
begin
  Result := GetPropList(FClass.ClassInfo, PropList);
end;

function TXR_Class.CreateProperty(AProp: TPropInfo): IXR_Property;
begin
  Result := TXR_Property.Create(AProp);
end;

procedure TXR_Class.LoadProperties;
begin
  if not FPropertiesLoaded then
  begin
    FPropertiesLoaded := True;
    FProperties := ReadProperties;
  end;
end;

function TXR_Class.ReadProperties: TXR_ArrayProperties;
var
  p: PPropList;
  p2: TPropInfo;
  n: Integer;
  I: Integer;
begin
  n := ReadPropList(p);
  SetLength(Result, n);
  for I := 0 to n - 1 do
  begin
    p2 := p^[I]^;
    Result[I] := CreateProperty(p2);
  end;
end;

{ TXR_Object }

class function TXR_Object.Create(AObject: TObject): IXR_Object;
begin
  Result := TXR_Object.CreateInstance(AObject);
end;

constructor TXR_Object.CreateInstance(AObject: TObject);
begin
  inherited Create(AObject.ClassType);
  FObject := AObject;
end;

function TXR_Object.GetObject: TObject;
begin
  Result := FObject;
end;

function TXR_Object.GetPropertiesFromIndexInstance(
  AIndex: Integer): IXR_PropertyInstance;
begin
  (inherited PropertiesFromIndex[AIndex]).QueryInterface(IXR_PropertyInstance, Result);
end;

function TXR_Object.GetPropertyInstance(AName: String): IXR_PropertyInstance;
begin
  (inherited Properties[AName]).QueryInterface(IXR_PropertyInstance, Result);
end;

function TXR_Object.ReadPropList(out PropList: PPropList): Integer;
begin
  Result := GetPropList(FObject, PropList);
end;

function TXR_Object.CreateProperty(AProp: TPropInfo): IXR_Property;
begin
  Result := TXR_PropertyInstance.Create(Self, AProp);
end;

{ TXR_Property }

class function TXR_Property.Create(AObject: IXR_Class;
  APropertyName: String): IXR_Property;
begin
  Result := TXR_Property.CreateInstance(AObject, APropertyName);
end;

constructor TXR_Property.CreateInstance(AClass: IXR_Class;
  APropertyName: String);
var
  p: PPropInfo;
begin
  p := TypInfo.GetPropInfo(AClass.&Class.ClassInfo, APropertyName);
  CreateInstance(p^);
end;

class function TXR_Property.Create(APropInfo: TPropInfo): TXR_Property;
begin
  Result := TXR_Property.CreateInstance(APropInfo);
end;

constructor TXR_Property.CreateInstance(APropInfo: TPropInfo);
begin
  FPropInfo := APropInfo;
end;

function TXR_Property.GetIsReadable: Boolean;
begin
  Result := PropInfo.GetProc <> nil;
end;

function TXR_Property.GetIsWriteable: Boolean;
begin
  Result := PropInfo.SetProc <> nil;
end;

function TXR_Property.GetName: string;
begin
  Result := String(PropInfo.Name);
end;

function TXR_Property.GetPropInfo: TPropInfo;
begin
  Result := FPropInfo;
end;

function TXR_Property.GetValue(AInstance: TObject): TXR_Value;
var
  p: PPropInfo;
begin
  p := @FPropInfo;
  Result := GetPropValue(AInstance, p);
end;

procedure TXR_Property.SetValue(AInstance: TObject; const AValue: TXR_Value);
var
  p: PPropInfo;
begin
  p := @FPropInfo;
  SetPropValue(AInstance, p, AValue);
end;

{ TXR_Value }

class operator TXR_Value.Implicit(AValue: Variant): TXR_Value;
begin
  Result.Value := AValue;
end;

function TXR_Value.GetAsInteger: Integer;
begin
  Result := Self.Value;
end;

function TXR_Value.GetAsString: String;
begin
  Result := VarToStr(Self.Value);
end;

class operator TXR_Value.Implicit(AValue: TXR_Value): Variant;
begin
  Result := AValue.Value;
end;

{ TXR_PropertyInstance }

class function TXR_PropertyInstance.Create(AObject: IXR_Object;
  APropInfo: TPropInfo): TXR_Property;
begin
  Result := TXR_PropertyInstance.CreateInstance(AObject, APropInfo);
end;

constructor TXR_PropertyInstance.CreateInstance(AObject: IXR_Object;
  APropInfo: TPropInfo);
begin
  inherited CreateInstance(APropInfo);
  FObject := AObject;
end;

function TXR_PropertyInstance.GetObject: IXR_Object;
begin
  Result := FObject;
end;

function TXR_PropertyInstance.GetValueInstance: TXR_Value;
begin
  Result := inherited Value[FObject.&Object];
end;

procedure TXR_PropertyInstance.SetObject(const Value: IXR_Object);
begin
  FObject := Value;
end;

procedure TXR_PropertyInstance.SetValueInstance(const Value: TXR_Value);
begin
  inherited Value[FObject.&Object] := Value;
end;

end.
