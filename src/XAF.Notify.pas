unit XAF.Notify;

{$IF CompilerVersion >= 20}
  {$DEFINE DELPHI2009_UP}
{$IFEND}

interface

uses
  StdCtrls, ExtCtrls, Controls, Messages, Windows, Classes, Graphics, SysUtils,
  XAF.Handle, GDIPUtil, GDIPAPI, Forms, XAF.Anim, XAF.Process,
  GDIPOBJ;

{$R '.\..\res\XN_Notify.res'}

type
  TXN_Notify = class;
  TXN_NotifyWindow = class;
  TXN_Control = class;
  TXN_ControlImage = class;
  TXN_ControlOptions = class;
  TXN_ControlButtons = class;
  TXN_DesignSelect = class;
  TXN_ControlImageBase = class;
  TXN_ControlButtonsItem = class;
  TXN_Window = class;
  TXN_Manager = class;

  TXN_ControlBase = class abstract(TGraphicControl)
  private
    FInAnimHeight: Boolean;
    FAnimateHeightPos: Integer;
    FAnimHeight: TXA_Anim;
    FControl: TXN_Control;
    procedure SetAnimateHeightPos(const Value: Integer);
  protected
    procedure DoAnimComplete(ASender: TObject);
    function InternalCalcHeight(AW: Integer; ANoAnimHeight: Boolean = False): Integer; virtual;
    procedure PaintTo(AGraphic: TGPGraphics); virtual; abstract;
    procedure Paint; override;
    procedure Resize; override;
    procedure VisibleChanging; override;
    procedure ShowAnimHeight(AOpen: Boolean); overload;
    procedure ShowAnimHeight(AFrom, ATo: Integer); overload;
  public
    function CalcHeight(AW: Integer; ANoAnimHeight: Boolean = False): Integer;
    procedure SetBounds(ALeft: Integer; ATop: Integer; AWidth: Integer; AHeight: Integer); override;
    destructor Destroy; override;
    property Control: TXN_Control read FControl;
    constructor Create(AOwner: TComponent; AControl: TXN_Control); reintroduce; virtual;
  published
    property AnimateHeightPos: Integer read FAnimateHeightPos write SetAnimateHeightPos;
  end;

  TXN_ImageSrc_Proc = class(TXP_Base)
  private
    FUrl: String;
    FResult: TPicture;
    FResultOk: Boolean;
  public
    destructor Destroy; override;
    property Url: String read FUrl;
    procedure Execute(AProc: TXP_Process); override;
    constructor Create(AUrl: String); reintroduce;
    property Result: TPicture read FResult;
    property ResultOk: Boolean read FResultOk;
  end;

  TXN_ImageSrcType = (stSystem, stPicture, stUrl);
  TXN_ImageSystemType = (ssApplication, ssAsterisk, ssError, ssExclamation, ssHand, ssInformation, ssQuestion, ssShield, ssWarning, ssWinLogo);
  TXN_ImageSrc = class(TPersistent)
  private
    FPicture: TPicture;
    FSrcType: TXN_ImageSrcType;
    FURL: String;
    FImage: TXN_ControlImageBase;
    FUrlGraphic: TGraphic;
    FIsLoading: Boolean;
    FLoaded: Boolean;
    FSystemType: TXN_ImageSystemType;
    FSystemIcon: TIcon;
    FOldHasImage: Boolean;
    procedure DoPictureChange(Sender: TObject);
    procedure DoChange;
    procedure SetPicture(const Value: TPicture);
    procedure SetSrcType(const Value: TXN_ImageSrcType);
    procedure SetURL(const Value: String);
    function GetUrlGraphic: TGraphic;
    procedure SetSystemType(const Value: TXN_ImageSystemType);
  protected
    procedure DoProcFinish(ASender: TXP_Base);
    procedure LoadUrlGraphic;
    function IsLoading: Boolean;
//    function LoadUrlGraphic: TGraphic;
    property Image: TXN_ControlImageBase read FImage;
    property UrlGraphic: TGraphic read GetUrlGraphic;

  public
    function FinalGraphic: TGraphic;
    function HasGraphic: Boolean;
    constructor Create(AImage: TXN_ControlImageBase);
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Picture: TPicture read FPicture write SetPicture;
    property URL: String read FURL write SetURL;
    property SrcType: TXN_ImageSrcType read FSrcType write SetSrcType default stSystem;
    property SystemType: TXN_ImageSystemType read FSystemType write SetSystemType default ssApplication;
  end;

  TXN_ControlImageBase = class(TXN_ControlBase)
  private
    FImage: TXN_ImageSrc;
    FCustomHeight: Integer;
    FCustomAutosize: Boolean;
    FReady: Boolean;
    procedure SetCustomAutosize(const Value: Boolean);
    procedure UpdateAutoSize;
    procedure SetCustomHeight(const Value: Integer);
  protected
    const ImageMrg = 10;
    procedure PaintTo(AGraphic: TGPGraphics); override;
    procedure DoImageSrcChange; virtual;
    function InternalCalcHeight(AW: Integer; ANoAnim: Boolean = False): Integer; override;
    function CalcImageRect(AW: Integer; AHeightAnim: Boolean = False): TGPRectF;
    function CanAutoSize(var NewWidth: Integer; var NewHeight: Integer): Boolean; override;
    procedure DoSrcHasImage(AValue: Boolean); virtual;
  public
    constructor Create(AOwner: TComponent; AControl: TXN_Control); override;
    destructor Destroy; override;
  published
    property Image: TXN_ImageSrc read FImage write FImage;
    property CustomAutosize: Boolean read FCustomAutosize write SetCustomAutosize;
    property CustomHeight: Integer read FCustomHeight write SetCustomHeight;
  end;

  TXN_ControlImage = class(TXN_ControlImageBase)
  protected
    procedure DoImageSrcChange; override;
    procedure DoSrcHasImage(AValue: Boolean); override;
  public
    constructor Create(AOwner: TComponent; AControl: TXN_Control); override;
    destructor Destroy; override;
  published
    property Color;
  end;

  TXN_ControlIcon = class(TXN_ControlImageBase)
  public
    constructor Create(AOwner: TComponent; AControl: TXN_Control); override;
  published
    property Color;
  end;

  TXN_DesignSelect = class(TXN_ControlBase)
  private
    FAnimatePos: Integer;
    FAnim: TXA_Anim;
    procedure SetAnimatePos(const Value: Integer);
    procedure InflateAnimRect(var r: TGPRectF);
  protected
    const DesignMrg = 10;
    procedure PaintTo(AGraphic: TGPGraphics); override;
    procedure CMHitTest(var Message: TCMHitTest); message CM_HITTEST;
    procedure ShowAnim;
  public
    constructor Create(AOwner: TComponent; AControl: TXN_Control); override;
    destructor Destroy; override;

  published
    property AnimatePos: Integer read FAnimatePos write SetAnimatePos;
  end;

  TSplitTextTagType = (ttNone, ttOpen, ttClose, ttChar);
  TSplitText = record
    TagType: TSplitTextTagType;
    Text: String;
  end;
  TSplitTexts = array of TSplitText;

  TXN_ControlTextRender = class(TPersistent)
  private
    FText: String;
    FResultWidth: Single;
    FResultHeight: Single;
    procedure DrawString(AGraphic: TGPGraphics; IsFirst: Boolean; AText: string; ARect: TGPRectF; var X: Single; var Y: Single; var AMaxW: Single; var maxHLine: Single; var wordsXline: Integer; AFont: TGPFont; AStringFormat: TGPStringFormat; ABrush: TGPSolidBrush; AWordWrap, AMeasure: Boolean);
  protected
    function SplitText: TSplitTexts;
  public
    property ResultHeight: Single read FResultHeight;
    property ResultWidth: Single read FResultWidth;
    constructor Create();
    procedure PaintTo(AGraphic: TGPGraphics; ARect: TGPRectF; AFont: TFont; AWordWrap: Boolean; AMeasure: Boolean);
    property Text: String read FText write FText;
  end;

  TXN_ControlTextBase = class(TXN_ControlBase)
  private
    FUseTags: Boolean;
    FRender: TXN_ControlTextRender;
    function DoDrawText(AGraphic: TGPGraphics; AMeasure: Boolean; AW:Integer = -1): Integer;
    procedure SetUseTags(const Value: Boolean);
  protected
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
    procedure PaintTo(AGraphic: TGPGraphics); override;
    function InternalCalcHeight(AW:Integer; ANoAnimHeight: Boolean = False): Integer; override;
  public
    constructor Create(AOwner: TComponent; AControl: TXN_Control); override;
    destructor Destroy; override;

  published
    property Text;
    property UseTags: Boolean read FUseTags write SetUseTags;
  end;

  TXN_ControlTitle = class(TXN_ControlTextBase)
  public
    constructor Create(AOwner: TComponent; AControl: TXN_Control); override;
  end;

  TXN_ControlMessage = class(TXN_ControlTextBase)
  public
    constructor Create(AOwner: TComponent; AControl: TXN_Control); override;
  end;

  TXN_ControlProgressbar = class(TXN_ControlBase)
  private
    FMax: Integer;
    FPosition: Integer;
    FAnimated: Boolean;
    FToPosition: Integer;
    FAnim: TXA_Anim;
    FAnimatedPosition: Integer;
    procedure SetMax(const Value: Integer);
    procedure SetPosition(const Value: Integer);
    procedure SetAnimatedPosition(const Value: Integer);
  protected
    procedure GoAnim;
    procedure PaintTo(AGraphic: TGPGraphics); override;
    function GetStaticHeight: Integer;
  public
    constructor Create(AOwner: TComponent; AControl: TXN_Control); override;
    destructor Destroy; override;
  published
    property Position: Integer read FPosition write SetPosition;
    property Max: Integer read FMax write SetMax;
    property AnimatedPosition: Integer read FAnimatedPosition write SetAnimatedPosition;
    property Animated: Boolean read FAnimated write FAnimated;
  end;

  TXN_ControlCloseButton = class(TXN_ControlBase)
  private
    FMouseInControl: Boolean;
    FAnimatePos: Integer;
    FAnim: TXA_Anim;
    procedure SetMouseInControl(const Value: Boolean);
    procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure SetAnimatePos(const Value: Integer);
  protected
    procedure PaintTo(AGraphic: TGPGraphics); override;
    procedure VisibleChanging; override;
    procedure Click; override;
    property MouseInControl: Boolean read FMouseInControl write SetMouseInControl;
    procedure ShowAnim(AIn: Boolean);
  public
    constructor Create(AOwner: TComponent; AControl: TXN_Control); override;
    destructor Destroy; override;
  published
    property AnimatePos: Integer read FAnimatePos write SetAnimatePos;
  end;

  TXN_ControlButton = class(TXN_ControlBase)
  private
    FItem: TXN_ControlButtonsItem;
    FMouseInControl: Boolean;
    function DrawText(AGraphic: TGPGraphics; AW: Integer; AH: Integer; AMeasure: Boolean): Integer;
    procedure SetMouseInControl(const Value: Boolean);
  protected
    FRender: TXN_ControlTextRender;
    procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure PaintTo(AGraphic: TGPGraphics); override;
    function InternalCalcHeight(AW: Integer; ANoAnimHeight: Boolean = False): Integer; override;
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
  public
    property Item: TXN_ControlButtonsItem read FItem;
    constructor Create(AOwner: TComponent; AItem: TXN_ControlButtonsItem; AControl: TXN_Control); reintroduce;
    destructor Destroy; override;
    property MouseInControl: Boolean read FMouseInControl write SetMouseInControl;
  published
    property Text;
  end;

  TXN_DesignSelectEvent = function (Sender: TObject; AControl: TControl): Boolean of object;
  TXN_SelectComponentProc = procedure (AObj: TPersistent);
  TXN_DesignCreateControl = procedure (AControl: TXN_Control);
  TXN_DesignCreateNotify = procedure (ANotify: TXN_Notify);
  TXN_DesignerClosed = procedure ();

  TXN_ControlOptsBase = class(TPersistent)
  private
    FParent: TXN_ControlOptions;
    function GetOnClick: TNotifyEvent;
    function GetOnDblClick: TNotifyEvent;
    procedure SetOnClick(const Value: TNotifyEvent);
    procedure SetOnDblClick(const Value: TNotifyEvent);
    function GetOnCanResize: TCanResizeEvent;
    function GetOnConstrainedResize: TConstrainedResizeEvent;
    function GetOnDragDrop: TDragDropEvent;
    function GetOnDragOver: TDragOverEvent;
    function GetOnEndDock: TEndDragEvent;
    function GetOnEndDrag: TEndDragEvent;
    function GetOnMouseActivate: TMouseActivateEvent;
    function GetOnMouseDown: TMouseEvent;
    function GetOnMouseEnter: TNotifyEvent;
    function GetOnMouseLeave: TNotifyEvent;
    function GetOnMouseMove: TMouseMoveEvent;
    function GetOnMouseUp: TMouseEvent;
    function GetOnMouseWheel: TMouseWheelEvent;
    function GetOnMouseWheelDown: TMouseWheelUpDownEvent;
    function GetOnMouseWheelUp: TMouseWheelUpDownEvent;
    function GetOnResize: TNotifyEvent;
    function GetOnStartDock: TStartDockEvent;
    function GetOnStartDrag: TStartDragEvent;
    procedure SetOnCanResize(const Value: TCanResizeEvent);
    procedure SetOnConstrainedResize(const Value: TConstrainedResizeEvent);
    procedure SetOnDragDrop(const Value: TDragDropEvent);
    procedure SetOnDragOver(const Value: TDragOverEvent);
    procedure SetOnEndDock(const Value: TEndDragEvent);
    procedure SetOnEndDrag(const Value: TEndDragEvent);
    procedure SetOnMouseActivate(const Value: TMouseActivateEvent);
    procedure SetOnMouseDown(const Value: TMouseEvent);
    procedure SetOnMouseEnter(const Value: TNotifyEvent);
    procedure SetOnMouseLeave(const Value: TNotifyEvent);
    procedure SetOnMouseMove(const Value: TMouseMoveEvent);
    procedure SetOnMouseUp(const Value: TMouseEvent);
    procedure SetOnMouseWheel(const Value: TMouseWheelEvent);
    procedure SetOnMouseWheelDown(const Value: TMouseWheelUpDownEvent);
    procedure SetOnMouseWheelUp(const Value: TMouseWheelUpDownEvent);
    procedure SetOnResize(const Value: TNotifyEvent);
    procedure SetOnStartDock(const Value: TStartDockEvent);
    procedure SetOnStartDrag(const Value: TStartDragEvent);
  protected
    function GetControl: TControl; virtual; abstract;
    property Control: TControl read GetControl;
  public
    function GetNamePath: string; override;
    procedure Assign(Source: TPersistent); override;
    property Parent: TXN_ControlOptions read FParent;
    constructor Create(AParent: TXN_ControlOptions); virtual;
    procedure Change;
  published
    property OnClick: TNotifyEvent read GetOnClick write SetOnClick;
    property OnDblClick: TNotifyEvent read GetOnDblClick write SetOnDblClick;
    property OnCanResize: TCanResizeEvent read GetOnCanResize write SetOnCanResize;
    property OnConstrainedResize: TConstrainedResizeEvent read GetOnConstrainedResize write SetOnConstrainedResize;
    property OnDragDrop: TDragDropEvent read GetOnDragDrop write SetOnDragDrop;
    property OnDragOver: TDragOverEvent read GetOnDragOver write SetOnDragOver;
    property OnEndDock: TEndDragEvent read GetOnEndDock write SetOnEndDock;
    property OnEndDrag: TEndDragEvent read GetOnEndDrag write SetOnEndDrag;
    property OnMouseActivate: TMouseActivateEvent read GetOnMouseActivate write SetOnMouseActivate;
    property OnMouseDown: TMouseEvent read GetOnMouseDown write SetOnMouseDown;
    property OnMouseEnter: TNotifyEvent read GetOnMouseEnter write SetOnMouseEnter;
    property OnMouseLeave: TNotifyEvent read GetOnMouseLeave write SetOnMouseLeave;
    property OnMouseMove: TMouseMoveEvent read GetOnMouseMove write SetOnMouseMove;
    property OnMouseUp: TMouseEvent read GetOnMouseUp write SetOnMouseUp;
    property OnMouseWheel: TMouseWheelEvent read GetOnMouseWheel write SetOnMouseWheel;
    property OnMouseWheelDown: TMouseWheelUpDownEvent read GetOnMouseWheelDown write SetOnMouseWheelDown;
    property OnMouseWheelUp: TMouseWheelUpDownEvent read GetOnMouseWheelUp write SetOnMouseWheelUp;
    property OnResize: TNotifyEvent read GetOnResize write SetOnResize;
    property OnStartDock: TStartDockEvent read GetOnStartDock write SetOnStartDock;
    property OnStartDrag: TStartDragEvent read GetOnStartDrag write SetOnStartDrag;

  end;

  TXN_ControlOptsTextBase = class(TXN_ControlOptsBase)
  private
    function GetText: String;
    procedure SetText(const Value: String);
  protected
    function GetEditControl: TXN_ControlTextBase; virtual; abstract;
    function GetControl: TControl; override;
    property EditControl: TXN_ControlTextBase read GetEditControl;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Text: String read GetText write SetText;
  end;

  TXN_ControlOptsTitle = class(TXN_ControlOptsTextBase)
  private
    function GetTitle: TXN_ControlTitle;
  public
    function GetEditControl: TXN_ControlTextBase; override;
  protected
    property Title: TXN_ControlTitle read GetTitle;
  end;

  TXN_ControlOptsMessage = class(TXN_ControlOptsTextBase)
  private
    function GetMessage: TXN_ControlMessage;
    function GetVisible: Boolean;
    procedure SetVisible(const Value: Boolean);
  protected
    function GetEditControl: TXN_ControlTextBase; override;
    property Message: TXN_ControlMessage read GetMessage;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Visible: Boolean read GetVisible write SetVisible;
  end;

  TXN_ControlOptsProgressBar = class(TXN_ControlOptsBase)
  private
    procedure SetMax(const Value: Integer);
    procedure SetPosition(const Value: Integer);
    function GetMax: Integer;
    function GetPosition: Integer;
    function GetAnimated: Boolean;
    procedure SetAnimated(const Value: Boolean);
    function GetVisible: Boolean;
    procedure SetVisible(const Value: Boolean);
    function GetProgressBar: TXN_ControlProgressbar;
    property ProgressBar: TXN_ControlProgressbar read GetProgressBar;
  protected
    function GetControl: TControl; override;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Max: Integer read GetMax write SetMax;
    property Position: Integer read GetPosition write SetPosition;
    property Animated: Boolean read GetAnimated write SetAnimated;
    property Visible: Boolean read GetVisible write SetVisible;
  end;

  TXN_ControlOptsCloseButton = class(TXN_ControlOptsBase)
  private
    function GetVisible: Boolean;
    procedure SetVisible(const Value: Boolean);
  protected
    function GetControl: TControl; override;
  public
    function CloseButton: TXN_ControlCloseButton;
    procedure Assign(Source: TPersistent); override;
  published
    property Visible: Boolean read GetVisible write SetVisible;
  end;

  TXN_ControlOptsImageBase = class(TXN_ControlOptsBase)
  private
    function GetImage: TXN_ImageSrc;
    procedure SetImage(const Value: TXN_ImageSrc);
    function GetAutoSize: Boolean;
    procedure SetAutoSize(const Value: Boolean);
    function GetHeight: Integer;
    procedure SetHeight(const Value: Integer);
    function GetColor: TColor;
    procedure SetColor(const Value: TColor);
    function GetVisible: Boolean;
    procedure SetVisible(const Value: Boolean);
  protected
    function GetControlImage: TXN_ControlImageBase; virtual; abstract;
    function GetControl: TControl; override;
  public
    procedure Assign(Source: TPersistent); override;
    property ControlImage: TXN_ControlImageBase read GetControlImage;
    constructor Create(AParent: TXN_ControlOptions); override;
    property AutoSize: Boolean read GetAutoSize write SetAutoSize default True;
    property Height: Integer read GetHeight write SetHeight default -1;
  published
    property Image: TXN_ImageSrc read GetImage write SetImage;
    property Color: TColor read GetColor write SetColor;
    property Visible: Boolean read GetVisible write SetVisible;
  end;

  TXN_ControlOptsImage = class(TXN_ControlOptsImageBase)
  private
    function GetControlImage2: TXN_ControlImage;
  protected
    function GetControlImage: TXN_ControlImageBase; override;
  public
    procedure Assign(Source: TPersistent); override;
    property ControlImage: TXN_ControlImage read GetControlImage2;
  published
    property Height;
    property AutoSize;
  end;

  TXN_ControlOptsIcon = class(TXN_ControlOptsImageBase)
  protected
    function GetControlImage: TXN_ControlImageBase; override;
  end;

  TXN_ControlOptions = class(TPersistent)
  private
    FParent: TXN_Control;
    FTitle: TXN_ControlOptsTitle;
    FMessage: TXN_ControlOptsMessage;
    FProgressBar: TXN_ControlOptsProgressBar;
    FCloseButton: TXN_ControlOptsCloseButton;
    FImage: TXN_ControlOptsImageBase;
    FIcon: TXN_ControlOptsImageBase;
  public
    destructor Destroy; override;
    constructor Create(AParent: TXN_Control);
    procedure Assign(Source: TPersistent); override;
    property Parent: TXN_Control read FParent;
    procedure Change;
  published
    property Title: TXN_ControlOptsTitle read FTitle write FTitle;
    property Message: TXN_ControlOptsMessage read FMessage write FMessage;
    property ProgressBar: TXN_ControlOptsProgressBar read FProgressBar write FProgressBar;
    property CloseButton: TXN_ControlOptsCloseButton read FCloseButton write FCloseButton;
    property Image: TXN_ControlOptsImageBase read FImage write FImage;
    property Icon: TXN_ControlOptsImageBase read FIcon write FIcon;
  end;

  TXN_ControlButtonsItem = class(TCollectionItem)
  private
    FControl: TXN_ControlButton;
    FReady: Boolean;
    FCloseOnClick: Boolean;
    FOnClick: TNotifyEvent;
    function GetButtons: TXN_ControlButtons;
    procedure SetControlEmpty(const Value: TXN_ControlButton);
    function GetText: String;
    procedure SetText(const Value: String);
    function GetOnCanResize: TCanResizeEvent;
    function GetOnConstrainedResize: TConstrainedResizeEvent;
    function GetOnDblClick: TNotifyEvent;
    function GetOnDragDrop: TDragDropEvent;
    function GetOnDragOver: TDragOverEvent;
    function GetOnEndDock: TEndDragEvent;
    function GetOnEndDrag: TEndDragEvent;
    function GetOnMouseActivate: TMouseActivateEvent;
    function GetOnMouseDown: TMouseEvent;
    function GetOnMouseEnter: TNotifyEvent;
    function GetOnMouseLeave: TNotifyEvent;
    function GetOnMouseMove: TMouseMoveEvent;
    function GetOnMouseUp: TMouseEvent;
    function GetOnMouseWheel: TMouseWheelEvent;
    function GetOnMouseWheelDown: TMouseWheelUpDownEvent;
    function GetOnMouseWheelUp: TMouseWheelUpDownEvent;
    function GetOnResize: TNotifyEvent;
    function GetOnStartDock: TStartDockEvent;
    function GetOnStartDrag: TStartDragEvent;
    procedure SetOnCanResize(const Value: TCanResizeEvent);
    procedure SetOnConstrainedResize(const Value: TConstrainedResizeEvent);
    procedure SetOnDblClick(const Value: TNotifyEvent);
    procedure SetOnDragDrop(const Value: TDragDropEvent);
    procedure SetOnDragOver(const Value: TDragOverEvent);
    procedure SetOnEndDock(const Value: TEndDragEvent);
    procedure SetOnEndDrag(const Value: TEndDragEvent);
    procedure SetOnMouseActivate(const Value: TMouseActivateEvent);
    procedure SetOnMouseDown(const Value: TMouseEvent);
    procedure SetOnMouseEnter(const Value: TNotifyEvent);
    procedure SetOnMouseLeave(const Value: TNotifyEvent);
    procedure SetOnMouseMove(const Value: TMouseMoveEvent);
    procedure SetOnMouseUp(const Value: TMouseEvent);
    procedure SetOnMouseWheel(const Value: TMouseWheelEvent);
    procedure SetOnMouseWheelDown(const Value: TMouseWheelUpDownEvent);
    procedure SetOnMouseWheelUp(const Value: TMouseWheelUpDownEvent);
    procedure SetOnResize(const Value: TNotifyEvent);
    procedure SetOnStartDock(const Value: TStartDockEvent);
    procedure SetOnStartDrag(const Value: TStartDragEvent);
    procedure SetCloseOnClick(const Value: Boolean);
  protected
    procedure DoButtonClick(Sender: TObject);
  public
    procedure Click;
    destructor Destroy; override;
    constructor Create(Collection: TCollection); override;
    procedure Assign(Source: TPersistent); override;
    property Buttons: TXN_ControlButtons read GetButtons;
    property Control: TXN_ControlButton read FControl write SetControlEmpty;
  published
    property CloseOnClick: Boolean read FCloseOnClick write SetCloseOnClick default True;
    property Text: String read GetText write SetText;

    property OnClick: TNotifyEvent read FOnClick write FOnClick;
    property OnDblClick: TNotifyEvent read GetOnDblClick write SetOnDblClick;
    property OnCanResize: TCanResizeEvent read GetOnCanResize write SetOnCanResize;
    property OnConstrainedResize: TConstrainedResizeEvent read GetOnConstrainedResize write SetOnConstrainedResize;
    property OnDragDrop: TDragDropEvent read GetOnDragDrop write SetOnDragDrop;
    property OnDragOver: TDragOverEvent read GetOnDragOver write SetOnDragOver;
    property OnEndDock: TEndDragEvent read GetOnEndDock write SetOnEndDock;
    property OnEndDrag: TEndDragEvent read GetOnEndDrag write SetOnEndDrag;
    property OnMouseActivate: TMouseActivateEvent read GetOnMouseActivate write SetOnMouseActivate;
    property OnMouseDown: TMouseEvent read GetOnMouseDown write SetOnMouseDown;
    property OnMouseEnter: TNotifyEvent read GetOnMouseEnter write SetOnMouseEnter;
    property OnMouseLeave: TNotifyEvent read GetOnMouseLeave write SetOnMouseLeave;
    property OnMouseMove: TMouseMoveEvent read GetOnMouseMove write SetOnMouseMove;
    property OnMouseUp: TMouseEvent read GetOnMouseUp write SetOnMouseUp;
    property OnMouseWheel: TMouseWheelEvent read GetOnMouseWheel write SetOnMouseWheel;
    property OnMouseWheelDown: TMouseWheelUpDownEvent read GetOnMouseWheelDown write SetOnMouseWheelDown;
    property OnMouseWheelUp: TMouseWheelUpDownEvent read GetOnMouseWheelUp write SetOnMouseWheelUp;
    property OnResize: TNotifyEvent read GetOnResize write SetOnResize;
    property OnStartDock: TStartDockEvent read GetOnStartDock write SetOnStartDock;
    property OnStartDrag: TStartDragEvent read GetOnStartDrag write SetOnStartDrag;
  end;

  TXN_ControlButtons = class(TOwnedCollection)
  private
    FControl: TXN_Control;
    function GetItem(Index: Integer): TXN_ControlButtonsItem;
    procedure SetItem(Index: Integer; const Value: TXN_ControlButtonsItem);
  protected
    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
  public
    property Items[Index: Integer]: TXN_ControlButtonsItem read GetItem write SetItem;
    constructor Create(AControl: TXN_Control);
    property Control: TXN_Control read FControl;
  end;

  TXN_Control = class(TWinControl)
  private
    FIcon: TXN_ControlIcon;
    FSelect: TXN_DesignSelect;
    FTitle: TXN_ControlTitle;
    FMessage: TXN_ControlMessage;
    FProgressBar: TXN_ControlProgressbar;
    FCloseButton: TXN_ControlCloseButton;
    FImage: TXN_ControlImage;
    FCurrentSelect: TControl;
    FOptions: TXN_ControlOptions;
    FNeverSelected: Boolean;
    FInternalHeight: Integer;
    FHasInternalHeight: Boolean;
    FReady: Boolean;
    FInUpdateDesignBounds: Boolean;
    FButtons: TXN_ControlButtons;
    FInDesign: Boolean;
    procedure SetCurrentSelect(Value: TControl);
    procedure SetInDesign(const Value: Boolean);
  protected
    const IconWidth = 80;

    // WM

    procedure WMLButtonDown(var Message: TWMLButtonDown); message WM_LBUTTONDOWN;
    procedure WMWindowPosChanging(var Message: TWMWindowPosChanging); message WM_WINDOWPOSCHANGING;

    // TWinControl/TControl/TComponent

    procedure AlignControls(AControl: TControl; var ARect: TRect); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    function CanAutoSize(var NewWidth: Integer; var NewHeight: Integer): Boolean; override;
    function MouseActivate(Button: TMouseButton; Shift: TShiftState; X: Integer; Y: Integer; HitTest: Integer): TMouseActivate; override;
    function DesignWndProc(var Message: TMessage): Boolean; override;
    function ControlAtPosExt(const Pos: TPoint; AllowDisabled: Boolean; AllowWinControls: Boolean = False; AllLevels: Boolean = False): TControl;

    // Design

    property CurrentSelect: TControl read FCurrentSelect write SetCurrentSelect;
    function GetDesignObject(AControl: TControl): TPersistent;
    function GetDesignObjectOfOther(AOther: TXN_Control; AControl: TControl): TPersistent;
    function SelectDesign(AControl: TControl): Boolean; virtual;
    function IsSelect(AControl: TControl): Boolean;

    // Events

    procedure DoDesignSelectNotify(c: array of TPersistent); // Design -> Self
    procedure DoCloseRequest; virtual;
    procedure DoOptionsChanged;
    procedure DoControlAnimHeight(AControl: TXN_ControlBase); virtual;

    function InternalHeight: Integer;

    procedure UpdateAllBounds;
    procedure UpdateAutoSize;
    procedure UpdateDesignBounds;
    procedure UpdateControlsBounds(AW, AH: Integer); overload;
    procedure UpdateControlsBounds; overload;

    procedure PaintTo(AGraphic: TGPGraphics);

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    property InDesign: Boolean read FInDesign write SetInDesign;
  published

    property Buttons: TXN_ControlButtons read FButtons write FButtons;
    property Options: TXN_ControlOptions read FOptions write FOptions;

    property Font;
    property ParentFont;
    {$IFDEF DELPHI2009_UP}
    property ParentDoubleBuffered;
    property OnGesture;
    {$ENDIF}
    property DoubleBuffered;
    property Enabled;
    property Visible;
    property OnStartDrag;
    property OnStartDock;
    property OnExit;
    property OnEnter;
    property OnMouseWheelUp;
    property OnMouseWheelDown;
    property OnMouseWheel;
    property OnMouseUp;
    property OnMouseMove;
    property OnMouseLeave;
    property OnMouseEnter;
    property OnMouseDown;
    property OnMouseActivate;
    property OnEndDrag;
    property OnEndDock;
    property OnDragOver;
    property OnDragDrop;
    property OnConstrainedResize;
    property OnDblClick;
    property OnClick;
    property OnResize;
    property OnCanResize;
    property Align;
    property AutoSize;
    property Anchors;
    property Margins;
  end;

  TXN_Notify = class(TComponent)
  private
    FManagerDesigner: TXN_Manager;
    FWindow: TXN_NotifyWindow;
    FOnShow: TNotifyEvent;
    FOnHide: TNotifyEvent;
    FOnClose: TNotifyEvent;
    FStayOnTop: Boolean;
    FManager: TXN_Manager;
    function GetButtons: TXN_ControlButtons;
    function GetOptions: TXN_ControlOptions;
    procedure SetButtons(const Value: TXN_ControlButtons);
    procedure SetOptions(const Value: TXN_ControlOptions);
    procedure SetManager(const Value: TXN_Manager);
    function GetDuration: Integer;
    procedure SetDuration(const Value: Integer);
    function GetPauseOnMouseEnter: Boolean;
    procedure SetPauseOnMouseEnter(const Value: Boolean);
    procedure SetStayOnTap(const Value: Boolean);
  protected
    function DoSelectWindowDesign(ASender: TObject; AObject: TControl): Boolean;
    procedure SetName(const NewName: TComponentName); override;

    procedure DesignerOpened;
    procedure DesignerClosed;
    procedure ShowWindowDesigner;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Window: TXN_NotifyWindow read FWindow;
    property ManagerDesigner: TXN_Manager read FManagerDesigner;
    property Manager: TXN_Manager read FManager write SetManager;
    procedure Show;
    procedure Close;
  published
    property Options: TXN_ControlOptions read GetOptions write SetOptions;
    property Buttons: TXN_ControlButtons read GetButtons write SetButtons;
    property Duration: Integer read GetDuration write SetDuration default -1;
    property PauseOnMouseEnter: Boolean read GetPauseOnMouseEnter write SetPauseOnMouseEnter default True;
    property StayOnTop: Boolean read FStayOnTop write SetStayOnTap default True;

    property OnShow: TNotifyEvent read FOnShow write FOnShow;
    property OnHide: TNotifyEvent read FOnHide write FOnHide;
    property OnClose: TNotifyEvent read FOnClose write FOnClose;


  end;

  TXN_ControlWindowed = class(TXN_Control)
  private
    FWindow: TXN_Window;
  protected
    procedure PaintWindow(DC: HDC); override;
    procedure DoCloseRequest; override;
    function SelectDesign(AControl: TControl): Boolean; override;
    procedure DoControlAnimHeight(AControl: TXN_ControlBase); override;
  public
    constructor Create(AOwner: TComponent; AWindow: TXN_Window); reintroduce;
    property Window: TXN_Window read FWindow;
  end;

  TXN_Window = class(TCustomForm)
  private
    FAnimClose: TXA_Anim;
    FInAnimClose1: Boolean;
    FAnimCloseValue: Integer;
    FControl: TXN_ControlWindowed;
    FManager: TXN_Manager;
    FAnimCloseValue2: Single;
    FAutoFree: Boolean;
    FOnSelectDesign: TXN_DesignSelectEvent;
    FStayOnTop: Boolean;
    FInAnimOpen: Boolean;
    FDuration: Integer;
    FTimer: TTimer;
    FAnimTopValue: Integer;
    FToAnimTop: Integer;
    FAnimTop: TXA_Anim;
    FFirstWindowBounds: Boolean;
    FVisible: Boolean;
    FInAnimClose2: Boolean;
    FMouseInWindow: Boolean;
    FPauseOnMouseEnter: Boolean;
    FPaused: Boolean;
    procedure RenderShadow(g: TGPGraphics; AW, AH: Integer);
    function CreateRenderBitmap: TGPBitmap;
    procedure SetManager(const Value: TXN_Manager);
    procedure SetAnimHeightValue(const Value: Integer);
    procedure SetAnimCloseValue2(const Value: Single);
    procedure SetStayOnTop(const Value: Boolean);
    procedure SetToAnimTop(const Value: Integer);
    procedure SetAnimTopValue(const Value: Integer);
    function GetVisible: Boolean;
    procedure SetVisible(const Value: Boolean);
    procedure UpdateManagerBounds;
    procedure SetMouseInWindow(const Value: Boolean);
  protected
    const ShadowWidth = 4;
    procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure WMActivate(var Message: TMessage); message WM_ACTIVATE;
    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure DoAnimCloseComplete1(Sender: TObject);
    procedure DoAnimCloseComplete2(Sender: TObject);
    procedure DoAnimOpenComplete(Sender: TObject);
    procedure DoTimer(Sender: TObject);

    procedure ShowAnimOpen;
    procedure ShowAnimClose;
    procedure ShowAnimClose1;
    procedure ShowAnimClose2;

    procedure PaintToLayer(AImage: TGPBitmap);
    procedure InvalidateLayer;

    constructor CreateWindow(AOwner: TComponent);
    procedure Resize; override;
    procedure DoClose(var Action: TCloseAction); override;
    procedure DoHide; override;
    procedure VisibleChanging; override;

    property ToAnimTop: Integer read FToAnimTop write SetToAnimTop;

  public
    procedure SetBounds(ALeft: Integer; ATop: Integer; AWidth: Integer; AHeight: Integer); override;
    procedure SetWindowBounds(ALeft: Integer; ATop: Integer; AWidth: Integer; AHeight: Integer);
    property Manager: TXN_Manager read FManager write SetManager;
    function SelectDesign(AControl: TControl): Boolean; virtual;
    property AutoFree: Boolean read FAutoFree write FAutoFree;
    class function Create(AOwner: TComponent): TXN_Window; reintroduce;
    property Control: TXN_ControlWindowed read FControl;
    destructor Destroy; override;
    property OnSelectDesign: TXN_DesignSelectEvent read FOnSelectDesign write FOnSelectDesign;
    procedure Show(ADuration: Integer); reintroduce; overload;
    procedure Show; reintroduce; overload;
    property StayOnTop: Boolean read FStayOnTop write SetStayOnTop;
    property MouseInWindow: Boolean read FMouseInWindow write SetMouseInWindow;
    property PauseOnMouseEnter: Boolean read FPauseOnMouseEnter write FPauseOnMouseEnter;
  published
    property Visible: Boolean read GetVisible write SetVisible;
    property Duration: Integer read FDuration write FDuration default -1;
    property AnimCloseValue: Integer read FAnimCloseValue write SetAnimHeightValue;
    property AnimCloseValue2: Single read FAnimCloseValue2 write SetAnimCloseValue2;
    property AnimTopValue: Integer read FAnimTopValue write SetAnimTopValue;
  end;

  TXN_NotifyWindow = class(TXN_Window)
  private
    FNotify: TXN_Notify;
  protected
    constructor CreateWindow(AOwner: TComponent; ANotify: TXN_Notify); reintroduce;
    procedure DoShow; override;
    procedure DoClose(var Action: TCloseAction); override;
    procedure DoHide; override;
  public
    class function Create(AOwner: TComponent; ANotify: TXN_Notify): TXN_NotifyWindow; reintroduce;
    property Notify: TXN_Notify read FNotify;
  end;

  TXN_AlignWindow = (awLeftTop, awRightTop, awLeftBottom, awRightBottom);

  TXN_Manager = class(TComponent)
  private
    class var FDefault: TXN_Manager;
  private
    FList: TList;
    FAlign: TXN_AlignWindow;
    FEnableCustomRect: Boolean;
    FCustomRect: TRect;
    FWindowParent: THandle;
    FStayOnTop: Boolean;
    FHandleWindowParent: TXH_WinControl;
    function GetWindow(AIndex: Integer): TXN_Window;
    function GetWindowsCount: Integer;
    procedure SetAlign(const Value: TXN_AlignWindow);
    procedure SetCustomRect(const Value: TRect);
    procedure SetEnableCustomRect(const Value: Boolean);
    procedure SetWindowParent(const Value: THandle);
    procedure SetStayOnTop(const Value: Boolean);

  protected
    procedure DoWindowParentResize(Sender: TObject);
    procedure UpdateWindowsBounds;
    function FinalAreaRect: TRect;
    procedure DoWindowUpdate;
    procedure DoSetWindow(AWindow: TXN_Window);
  public

    class property Default: TXN_Manager read FDefault;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure SendToEnd(AWindow: TXN_Window);
    procedure Add(AWindow: TXN_Window);
    procedure Remove(AWindow: TXN_Window);

    property Window[AIndex: Integer]: TXN_Window read GetWindow;
    property WindowsCount: Integer read GetWindowsCount;
    property Align: TXN_AlignWindow read FAlign write SetAlign;
    property EnableCustomRect: Boolean read FEnableCustomRect write SetEnableCustomRect;
    property CustomRect: TRect read FCustomRect write SetCustomRect;
    property WindowParent: THandle read FWindowParent write SetWindowParent;
    property StayOnTop: Boolean read FStayOnTop write SetStayOnTop;
  end;

var
  XND_DesignSelectComponent: TXN_SelectComponentProc;
  XND_DesignCreateControl: TXN_DesignCreateControl;
  XND_DesignCreateNotify: TXN_DesignCreateNotify;

implementation

uses
  ActiveX, Math, Types, StrUtils, IdHttp, jpeg, pngimage, gifimg;

type
  THC = class(TControl);
  {$IFNDEF DELPHI2009_UP}TPNGImage = TPNGObject;{$ENDIF}

  function RectWidth(ARect: TRect): Integer;
  begin
    REsult := ARect.RIght - ARect.Left;
  end;

  function RectHeight(ARect: TRect): Integer;
  begin
    REsult := ARect.Bottom - ARect.Top;
  end;

{ TXN_Control }

procedure TXN_Control.AlignControls(AControl: TControl; var ARect: TRect);
begin
  UpdateControlsBounds;
end;

procedure TXN_Control.Assign(Source: TPersistent);
var
  c: TXN_Control;
begin

  if Source is TXN_Control then
  begin

    Self.CurrentSelect := nil;
    Self.FInternalHeight := 0;

    c := Source as TXN_Control;
    InDesign := c.InDesign;
    Self.Options.Assign(c.Options);
    Self.Buttons.Assign(c.Buttons);

  end;

end;

function TXN_Control.CanAutoSize(var NewWidth, NewHeight: Integer): Boolean;
begin
  if AutoSize then
    NewHeight :=  InternalHeight;
  Result := True;
end;

function TXN_Control.ControlAtPosExt(const Pos: TPoint; AllowDisabled,
  AllowWinControls, AllLevels: Boolean): TControl;
var
  I: Integer;
  P: TPoint;
  LControl: TControl;

  function GetControlAtPos(AControl: TControl): Boolean;
  var
    B: Boolean;
  begin
    with AControl do
    begin
      P := Point(Pos.X - Left, Pos.Y - Top);


      Result := PtInRect(AControl.ClientRect, P);
      B := (AControl <> FSelect) and (AControl.Visible or not (csNoDesignVisible in AControl.ControlStyle));
      B := B or (Visible and (AControl.Enabled or AllowDisabled) and (AControl.Perform(CM_HITTEST, 0, PointToLParam(P)) <> 0));
      Result := Result and B;

      if Result then
        LControl := AControl;
    end;
  end;

begin
  LControl := nil;
    for I := ControlCount - 1 downto 0 do
      if GetControlAtPos(Controls[I]) then
        Break;
  Result := LControl;
end;

constructor TXN_Control.Create(AOwner: TComponent);
begin
  FReady := False;
  inherited;
  FInDesign := csDesigning in ComponentState;
  ParentBackground := false;
  FHasInternalHeight := False;
  FInternalHeight := 0;
  AutoSize := True;
  FNeverSelected := True;
  FOptions := TXN_ControlOptions.Create(Self);
  Color := clWhite;
  FTitle := TXN_ControlTitle.Create(Self, Self);
  FTitle.Parent := Self;
  FTitle.Text := 'Title';
  FMessage := TXN_ControlMessage.Create(Self, Self);
  FMessage.Parent := Self;
  FMessage.Text := 'Message';
  FProgressBar := TXN_ControlProgressbar.Create(Self, Self);
  FProgressBar.Parent := Self;
  FIcon := TXN_ControlIcon.Create(Self, Self);
  FIcon.Parent := Self;
  FIcon.CustomHeight := IconWidth;
  FIcon.CustomAutosize := False;
  FCloseButton := TXN_ControlCloseButton.Create(Self, Self);
  FCloseButton.Parent := Self;
  FImage := TXN_ControlImage.Create(Self, Self);
  FImage.Parent := Self;
  FButtons := TXN_ControlButtons.Create(Self);
  FSelect := TXN_DesignSelect.Create(Self, Self);
  FSelect.Parent := Self;
  FSelect.Visible := False;
  FReady := True;
  if Assigned(XND_DesignCreateControl) then XND_DesignCreateControl(Self);

end;

function TXN_Control.DesignWndProc(var Message: TMessage): Boolean;
begin
  if Message.Msg = WM_MOUSEACTIVATE then
    Result := True
  else
    Result := inherited DesignWndProc(Message);
end;

destructor TXN_Control.Destroy;
begin
  FCurrentSelect := nil;
  FSelect.Free;
  FButtons.Free;
  FImage.Free;
  FCloseButton.Free;
  FProgressBar.Free;
  FMessage.Free;
  FTitle.Free;
  FIcon.Free;
  FOptions.Free;
  inherited;
end;

procedure TXN_Control.SetCurrentSelect(Value: TControl);
begin

  if (Value = Self) or (Value = FSelect) then
  begin
    Value := nil;
  end;

  if FCurrentSelect <> Value then
  begin
    FCurrentSelect := Value;
    if not Assigned(Value) then
    begin
       FSelect.Hide;
    end
    else
    begin
      FSelect.Show;
      FSelect.BringToFront;
      UpdateDesignBounds;
      FSelect.ShowAnim;
    end;

  end;
end;


procedure TXN_Control.SetInDesign(const Value: Boolean);
begin
  if FInDesign <> Value then
  begin
    FInDesign := Value;
    if not FInDesign then
      CurrentSelect := nil;
  end;
end;

procedure TXN_Control.DoCloseRequest;
begin

end;

procedure TXN_Control.DoControlAnimHeight(AControl: TXN_ControlBase);
begin

end;

procedure TXN_Control.DoDesignSelectNotify(c: array of TPersistent);
var
  I: Integer;
begin
  for I := 0 to Length(c) - 1 do
  begin
    if (c[I] is TControl) and Self.ContainsControl(c[I] as TControl) then
    begin
      CurrentSelect := c[I] as TControl;
      Exit;
    end;
  end;
  CurrentSelect := nil;
end;

function TXN_Control.GetDesignObject(AControl: TControl): TPersistent;
var
  I: Integer;
begin
  if AControl is TXN_ControlButton then
  begin
    for I := 0 to Buttons.Count - 1 do
      if Buttons.Items[I].Control = AControl then
      begin
        Result := Buttons.Items[I];
        Exit;
      end;
  end;
  if AControl = FTitle then
    Result := Options.Title
  else if AControl = FMessage then
    Result := Options.Message
  else if AControl = FProgressBar then
    Result := Options.ProgressBar
  else if AControl = FCloseButton then
    Result := Options.CloseButton
  else if AControl = FImage then
    Result := Options.Image
  else if AControl = FIcon then
    Result := Options.Icon
  else
    Result := nil;
end;

function TXN_Control.GetDesignObjectOfOther(AOther: TXN_Control;
  AControl: TControl): TPersistent;
var
  I: Integer;
begin
  if AControl is TXN_ControlButton then
  begin
    for I := 0 to AOther.Buttons.Count - 1 do
      if AOther.Buttons.Items[I].Control = AControl then
      begin
        Result := AOther.Buttons.Items[I];
        Exit;
      end;
  end;
  if AControl = FTitle then
    Result := AOther.Options.Title
  else if AControl = FMessage then
    Result := AOther.Options.Message
  else if AControl = FProgressBar then
    Result := AOther.Options.ProgressBar
  else if AControl = FCloseButton then
    Result := AOther.Options.CloseButton
  else if AControl = FImage then
    Result := AOther.Options.Image
  else if AControl = FIcon then
    Result := AOther.Options.Icon
  else
    Result := nil;
end;

function TXN_Control.InternalHeight: Integer;
begin
  if not FHasInternalHeight then
  begin
    UpdateControlsBounds;
  end;
  Result := FInternalHeight;
end;

function TXN_Control.IsSelect(AControl: TControl): Boolean;
begin
  Result := AControl = CurrentSelect;
end;

function TXN_Control.MouseActivate(Button: TMouseButton; Shift: TShiftState; X,
  Y, HitTest: Integer): TMouseActivate;
var
  p: TPoint;
  c: TControl;
  r: TMouseActivate;
begin
  if (HitTest = HTCLIENT) and InDesign then
  begin
    p := Point(X, Y);
    c := ControlAtPosExt(p, True, False, True);
    if Assigned(c) then
    begin
        SelectDesign(c);
      if SelectDesign(c) then
        r := maNoActivateAndEat
      else
        r := maNoActivate
    end
    else
      r := inherited MouseActivate(Button, Shift, X, Y, HitTest);
  end else
    r := inherited MouseActivate(Button, Shift, X, Y, HitTest);
  if InDesign then
    Result := r
  else
    Result := maNoActivate;
end;

procedure TXN_Control.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) and (AComponent = CurrentSelect) then
    CurrentSelect := nil;
end;

procedure TXN_Control.DoOptionsChanged;
begin
  Invalidate;
end;

procedure TXN_Control.PaintTo(AGraphic: TGPGraphics);
var
  I: Integer;
  c: TControl;
  br: TGPSolidBrush;
begin
  br := TGPSolidBrush.Create(aclWhite);
  try
    AGraphic.FillRectangle(br, MakeRect(0, 0, Width, Height));
  finally
    br.Free;
  end;
  for I := 0 to ControlCount - 1 do
  begin
    c := Controls[I];
    if not c.Visible then Continue;
    if c is TXN_ControlBase then
    begin
      AGraphic.TranslateTransform(c.Left, c.Top);
      AGraphic.SetClip(MakeRect(0, 0, c.Width, c.Height));
      try
        (c as TXN_ControlBase).PaintTo(AGraphic);
      finally
        AGraphic.ResetTransform;
        AGraphic.ResetClip;
      end;
    end
    else if c is TWinControl then
      (c as TWinControl).PaintTo(AGraphic.GetHDC, c.Left, c.Top);
  end;
end;

function TXN_Control.SelectDesign(AControl: TControl): Boolean;
var
  c: TPersistent;
begin

  Result := False;

  if AControl = FSelect then
    Exit;

  c := GetDesignObject(AControl);

  Result := Assigned(c);

  if not Result then
    Exit;

  if Assigned(XND_DesignSelectComponent) then
    XND_DesignSelectComponent(c);

  CurrentSelect := AControl;

end;

procedure TXN_Control.UpdateControlsBounds(AW, AH: Integer);
const
  mrg = 10;
  smrg = 5;
var
  RTitle: TRect;
  RMessage: TRect;
  w: Integer;
  RProgress: TRect;
  sContent: Integer;
  Y: Integer;
  RImage: TRect;
  w2: Integer;
  I: Integer;
  RButton: TRect;
  RIcon: TRect;
  x1: Integer;
begin

  if not FReady then
    Exit;

  w := AW;
  Y := smrg;
  x1 := mrg;

  if FIcon.Visible then
  begin
    RIcon := Types.Bounds(0, 0, IconWidth, Min(IconWidth, Height));
    FIcon.SetBounds(FIcon.Left, FIcon.Top, RectWidth(RIcon), RectHeight(RIcon));
    w := AW - IconWidth - mrg - mrg;
    x1 := IconWidth + mrg;
  end;

  w2 := w;

  if FCloseButton.Visible then
  begin
    FCloseButton.SetBounds(Width - 7 - 14, 7, 14, 14);
    w2 := w - 14;
  end;

  RTitle := Types.Bounds(x1, Y, w2, FTitle.CalcHeight(w2));
  FTitle.SetBounds(RTitle.Left, RTitle.Top, RectWidth(RTitle), RectHeight(RTitle));
  Y := RTitle.Bottom + smrg;

  if FMessage.Visible then
  begin
    RMessage := Types.Bounds(x1, Y, w, FMessage.CalcHeight(w));
    FMessage.SetBounds(RMessage.Left, RMessage.Top, RectWidth(RMessage), RectHeight(RMessage));
    Y := RMessage.Bottom + smrg;
  end;

  if FProgressBar.Visible then
  begin
    RProgress := Types.Bounds(x1, Y, w, FProgressBar.GetStaticHeight);
    FProgressBar.SetBounds(RProgress.Left, RProgress.Top, RectWidth(RProgress), RectHeight(RProgress));
    Y := RProgress.Bottom + mrg;
  end;

  Y := Max(IconWidth, Y);

  if FImage.Visible then
  begin
    RImage := Types.Bounds(0, Y, AW, Ceil(FImage.CalcHeight(AW)));
    FImage.SetBounds(RImage.Left, RImage.Top, RectWidth(RImage), RectHeight(RImage));
    Y := RImage.Bottom; // + mrg;
  end;

  // Buttons
  for I := 0 to Buttons.Count - 1 do
  begin

    RButton := Types.Bounds(0, Y, AW, Buttons.Items[I].Control.CalcHeight(AW));
    Buttons.Items[I].Control.SetBounds(RButton.Left, RButton.Top, RectWidth(RButton), RectHeight(RButton));
    Y := RButton.Bottom;

  end;

  sContent := Max(IconWidth, Y);

  UpdateDesignBounds;

  FInternalHeight := sContent;
  FHasInternalHeight := True;

//    inherited;
end;

procedure TXN_Control.UpdateAllBounds;
begin
  if AutoSize then
    UpdateAutoSize
  else
    UpdateControlsBounds;
end;

procedure TXN_Control.UpdateAutoSize;
var
  nw: Integer;
  nh: Integer;
begin
  nw := Width;
  nh := Height;
  UpdateControlsBounds(nw, nh);
  CanAutoSize(nw, nh);
  if (nw <> Width) or (nh <> Height) then
  begin
    SetBounds(Left, Top, nw, nh);
  end;
  Invalidate;
end;

procedure TXN_Control.UpdateControlsBounds;
begin
  UpdateControlsBounds(Width, Height);
end;

procedure TXN_Control.UpdateDesignBounds;
begin
  FInUpdateDesignBounds := True;
  try
    if Assigned(CurrentSelect) then
      with CurrentSelect do
        FSelect.SetBounds(Left - FSelect.DesignMrg, Top - FSelect.DesignMrg, Width + FSelect.DesignMrg * 2, Height + FSelect.DesignMrg * 2);
  finally
    FInUpdateDesignBounds := False;
  end;
end;

procedure TXN_Control.WMLButtonDown(var Message: TWMLButtonDown);
begin
  inherited;
end;

procedure TXN_Control.WMWindowPosChanging(var Message: TWMWindowPosChanging);
begin
  if ComponentState * [csReading, csDestroying] = [] then
  begin
    if (Message.WindowPos.flags and SWP_NOSIZE = 0) then
      with Message.WindowPos^ do
        UpdateControlsBounds(cx, cy);
  end;
  inherited;
end;

function TXN_ControlBase.CalcHeight(AW: Integer; ANoAnimHeight: Boolean): Integer;
begin
  if FInAnimHeight and (not ANoAnimHeight) then
    Result := FAnimateHeightPos
  else
    Result := InternalCalcHeight(AW, ANoAnimHeight);
end;

constructor TXN_ControlBase.Create(AOwner: TComponent; AControl: TXN_Control);
begin
  inherited Create(AOwner);
  FAnimateHeightPos := 0;
  FInAnimHeight := False;
  FAnimHeight := TXA_Anim.Create(Self);
  FAnimHeight.ScopeComponent := Self;
  FAnimHeight.ScopePropertyName := 'AnimateHeightPos';
  FAnimHeight.DefaultEasing := xeOutQuad;
  FAnimHeight.OnComplete := DoAnimComplete;
  ControlStyle := ControlStyle + [csNoDesignVisible];
  FControl := AControl;
  Align := alCustom;
end;

destructor TXN_ControlBase.Destroy;
begin
  FAnimHeight.Free;
  inherited;
end;

procedure TXN_ControlBase.DoAnimComplete(ASender: TObject);
begin
  FInAnimHeight := False;
end;

function TXN_ControlBase.InternalCalcHeight(AW: Integer;
  ANoAnimHeight: Boolean): Integer;
begin
  Result := Height;
end;

procedure TXN_ControlBase.Paint;
var
  g: TGPGraphics;
begin
  inherited;
  g := TGPGraphics.Create(Canvas.Handle);
  try
    g.SetSmoothingMode(SmoothingModeHighQuality);
    g.SetInterpolationMode(InterpolationModeHighQuality);
    g.SetPageUnit(UnitPixel);
    g.SetCompositingQuality(CompositingQualityHighQuality);
    PaintTo(g);
  finally
    g.Free;
  end;
end;

procedure TXN_ControlBase.Resize;
begin
  inherited;
end;

procedure TXN_ControlBase.SetAnimateHeightPos(const Value: Integer);
begin
  if FAnimateHeightPos <> Value then
  begin
    FAnimateHeightPos := Value;
    Control.UpdateAllBounds;
    Control.DoControlAnimHeight(Self);
  end;
end;

procedure TXN_ControlBase.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited;
  if (Control.FSelect <> Self) and (not Control.FInUpdateDesignBounds) and Assigned(Control) and Control.IsSelect(Self) then
    Control.UpdateDesignBounds;
end;

procedure TXN_ControlBase.ShowAnimHeight(AFrom, ATo: Integer);
begin
  FInAnimHeight := True;
  FAnimHeight.Stop;
  FAnimHeight.ClearValues;
  FAnimHeight.Values.AddValue(AFrom, ATo, 0.5);
  FAnimHeight.Start;
end;

procedure TXN_ControlBase.ShowAnimHeight(AOpen: Boolean);
var
  t: Integer;
  f: Integer;
  bInAnim: Boolean;
begin
  bInAnim := FInAnimHeight;
  FInAnimHeight := True;
  if AOpen then
  begin
    if bInAnim then
      f := Height
    else
      f := 0;
    t := Ceil(CalcHeight(Width, True));
  end else begin
    f := Height;
    t := 0;
  end;
//  if csLoading in ComponentState then
//  begin
//    FInAnimHeight := False;
//    AnimateHeightPos := t;
//    Exit;
//  end;
  ShowAnimHeight(f, t);
end;

procedure TXN_ControlBase.VisibleChanging;
begin
  inherited;
  if Assigned(Control) then
  begin
     if Visible and (Control.CurrentSelect = Self) then
      Control.CurrentSelect := nil
  end;
end;

{ TXN_DesignSelect }

procedure TXN_DesignSelect.CMHitTest(var Message: TCMHitTest);
begin
  Message.Result := HTNOWHERE;
end;

constructor TXN_DesignSelect.Create(AOwner: TComponent; AControl: TXN_Control);
begin
  inherited;
  FAnim := TXA_Anim.Create(Self);
  FAnim.ScopeComponent := Self;
  FAnim.ScopePropertyName := 'AnimatePos';
  FAnim.DefaultEasing := xeInCubic;
end;

destructor TXN_DesignSelect.Destroy;
begin
  FAnim.Free;
  inherited;
end;

procedure TXN_DesignSelect.InflateAnimRect(var r: TGPRectF);
var
  d: Single;
begin
  d := 5 * (AnimatePos / 100);
  d := 5 - d;
  r.X := (r.X + DesignMrg) - d;
  r.Y := (r.Y + DesignMrg) - d;
  r.Width := (r.Width - DesignMrg * 2) + d * 2;
  r.Height := (r.Height - DesignMrg * 2) + d * 2;
end;

procedure TXN_DesignSelect.PaintTo(AGraphic: TGPGraphics);
  procedure SetAlpha(var AColor: TGPColor; AValue: Byte);
  begin
    AColor := MakeColor(
      AValue,
      GetRed(AColor),
      GetGreen(AColor),
      GetBlue(AColor)
    );
  end;
var
  pn: TGPPen;
  r: TGPRectF;
  br: TGPSolidBrush;
  cPn: TGPColor;
  cBr: TGPColor;
  c: TGPColor;
begin
  with ClientRect do
  r := MakeRect(0.0 + Left, Top, Right - 1, Bottom - 1);

  InflateAnimRect(r);

  c := $FF0083ff;

  cPn := c;
  SetAlpha(cPn, Round($AA * AnimatePos / 100));

  cBr := c;
  SetAlpha(cBr, Round($22 * AnimatePos / 100));

  pn := TGPPen.Create(cPn);
  br := TGPSolidBrush.Create(cBr);
  try
    AGraphic.DrawRectangle(pn, r);
    AGraphic.FillRectangle(br, r)
  finally
    pn.Free;
    br.Free;
  end;
end;

procedure TXN_DesignSelect.SetAnimatePos(const Value: Integer);
begin
  if FAnimatePos <> Value then
  begin
    FAnimatePos := Value;
    Invalidate;
  end;
end;

procedure TXN_DesignSelect.ShowAnim;
begin
  AnimatePos := 0;
  FAnim.ClearValues;
  FAnim.Values.AddValue(0.0, 100.0, 0.1);
  FAnim.Start;
end;

{ TXN_ControlTitle }

function TXN_ControlTextBase.InternalCalcHeight(AW:Integer; ANoAnimHeight: Boolean): Integer;
var
  g: TGPGraphics;
  DC: THandle;
begin

  DC := GetDC(0);
  try
    g := TGPGraphics.Create(DC);
    try
      Result := DoDrawText(g, True, AW);
    finally
      g.Free;
    end;
  finally
    ReleaseDC(0, DC);
  end;
end;

procedure TXN_ControlTextBase.CMTextChanged(var Message: TMessage);
begin
  inherited;
  Control.UpdateAllBounds;
end;

constructor TXN_ControlTextBase.Create(AOwner: TComponent; AControl: TXN_Control);
begin
  inherited;
  AutoSize := True;
  FRender := TXN_ControlTextRender.Create();
end;

destructor TXN_ControlTextBase.Destroy;
begin
  FRender.Free;
  inherited;
end;

function TXN_ControlTextBase.DoDrawText(AGraphic: TGPGraphics; AMeasure: Boolean; AW:Integer): Integer;
var
  layoutRect: TGPRectF;
begin

  layoutRect := MakeRect(0.0 + ClientRect.Left, ClientRect.Top, RectWidth(ClientRect), RectHeight(ClientRect));
  if AW <> -1 then
    layoutRect.Width := AW;

  FRender.Text := Text;
  FRender.PaintTo(AGraphic, layoutRect, Font, True, AMeasure);
  Result := Round(FRender.ResultHeight);

end;

procedure TXN_ControlTextBase.PaintTo(AGraphic: TGPGraphics);
begin
  DoDrawText(AGraphic, False);
end;

procedure TXN_ControlTextBase.SetUseTags(const Value: Boolean);
begin
  FUseTags := Value;
end;

{ TXN_ControlTitle }

constructor TXN_ControlTitle.Create(AOwner: TComponent; AControl: TXN_Control);
begin
  inherited;
  Font.Size := 12;
  Font.Color := $005e5e5e;
end;

{ TXN_ControlMessage }

constructor TXN_ControlMessage.Create(AOwner: TComponent; AControl: TXN_Control);
begin
  inherited;
  Font.Size := 10;
  Font.Color := $00878787;
end;

{ TXN_ControlProgressbar }

constructor TXN_ControlProgressbar.Create(AOwner: TComponent;
  AControl: TXN_Control);
begin
  inherited;
  FAnim := TXA_Anim.Create(Self);
  FAnimated := True;
  FMax := 100;
end;

destructor TXN_ControlProgressbar.Destroy;
begin
  FAnim.Free;
  inherited;
end;

function TXN_ControlProgressbar.GetStaticHeight: Integer;
begin
  Result := 5;
end;

procedure TXN_ControlProgressbar.GoAnim;
begin
  FAnim.Stop;
  FAnim.ScopeComponent := Self;
  FAnim.ScopePropertyName := 'AnimatedPosition';
  FAnim.Values.AddValue(Position, FToPosition, 0.2);
  FAnim.Start;
end;

procedure TXN_ControlProgressbar.PaintTo(AGraphic: TGPGraphics);

  function GetRoundRectPath(R: TGPRect; rad: Integer): TGPGraphicsPath;
  var
    Corner: TGPRect;
  begin

    Result := TGPGraphicsPath.Create();

      if(rad > r.Width) then   rad := r.Width;
      if(rad > r.Height) then   rad := r.Height;

      Corner := MakeRect(r.X, r.Y, rad, rad);

      Result.AddArc(Corner, 180, 90);

      if(rad = 20) then
      begin
        Corner.Width := Corner.Width + 1;
        Corner.Height := Corner.Height +  1;
        r.Width := r.Width - 1;
        r.Height := r.Height - 1;
      end;

      Corner.X := Corner.X + (r.Width - rad - 1);
      Result.AddArc(Corner, 270, 90);

      Corner.Y := Corner.Y + (r.Height - rad - 1);
      Result.AddArc(Corner,   0, 90);

      Corner.X := Corner.X - (r.Width - rad - 1);
      Result.AddArc(Corner,  90, 90);

      Result.CloseFigure();
  end;

  procedure DrawBar(R: TRect; ACol: TGPColor);
  var
    rg: TGPGraphicsPath;
    br: TGPBrush;
    pn: TGPPen;
  begin
    rg := GetRoundRectPath(MakeRect(R), GetStaticHeight div 2);
    br := TGPSolidBrush.Create(ACol);
    pn := TGPPen.Create(ACol);
    try
      AGraphic.FillPath(br, rg);
      AGraphic.DrawPath(pn, rg);
    finally
      pn.Free;
      br.Free;
      rg.Free;
    end;
  end;

var
  R: TRect;
begin
  R := ClientRect;
  DrawBar(R, $FFd8d8d8);
  if (AnimatedPosition = 0) or (Max = 0) then
    R.Right := R.Left
  else
    R.Right := R.Left + Round(RectWidth(R) / (Max / Math.Min(Max, AnimatedPosition)));
  DrawBar(R, $FF787878);
end;

procedure TXN_ControlProgressbar.SetAnimatedPosition(const Value: Integer);
begin
  if FAnimatedPosition <> Value then
  begin
    FAnimatedPosition := Value;
    Invalidate;
  end;
end;

procedure TXN_ControlProgressbar.SetMax(const Value: Integer);
begin
  if FMax <> Value then
  begin
    FMax := Value;
    Invalidate;
    FAnim.Stop;
  end;
end;

procedure TXN_ControlProgressbar.SetPosition(const Value: Integer);
begin
  if FPosition <> Value then
  begin
    FToPosition := Value;
    if Animated and Assigned(Control) and not (csLoading in Control.ComponentState) then
    begin
      GoAnim;
    end
    else
    begin
      FAnimatedPosition := Value;
    end;
    FPosition := Value;
    Invalidate;
  end;
end;

{ TXN_ControlOptsBase }

procedure TXN_ControlOptsBase.Assign(Source: TPersistent);
var
  c: TXN_ControlOptsBase;
begin

  if Source is TXN_ControlOptsBase then
  begin   

    c := Source as TXN_ControlOptsBase;
    OnClick := c.OnClick;
    OnDblClick := c.OnDblClick;
    OnCanResize := c.OnCanResize;
    OnConstrainedResize := c.OnConstrainedResize;
    OnDragDrop := c.OnDragDrop;
    OnDragOver := c.OnDragOver;
    OnEndDock := c.OnEndDock;
    OnEndDrag := c.OnEndDrag;
    OnMouseActivate := c.OnMouseActivate;
    OnMouseDown := c.OnMouseDown;
    OnMouseEnter := c.OnMouseEnter;
    OnMouseLeave := c.OnMouseLeave;
    OnMouseUp := c.OnMouseUp;
    OnMouseWheel := c.OnMouseWheel;
    OnMouseMove := c.OnMouseMove;
    OnMouseWheelDown := c.OnMouseWheelDown;
    OnMouseWheelUp := c.OnMouseWheelUp;
  end;
end;

procedure TXN_ControlOptsBase.Change;
begin
  if Assigned(Parent) then Parent.Change;
end;

constructor TXN_ControlOptsBase.Create(AParent: TXN_ControlOptions);
begin
  inherited Create;
  FParent := AParent;
end;

function TXN_ControlOptsBase.GetNamePath: string;
const
  f = 'TXN_ControlOpts';
var
  n: String;
begin
  n := Self.ClassName;
  n := StringReplace(n, f, '', []);
  Result := Parent.Parent.Name + '.Options.' + n;
end;

function TXN_ControlOptsBase.GetOnCanResize: TCanResizeEvent;
begin
  Result := THC(Control).OnCanResize;
end;

function TXN_ControlOptsBase.GetOnClick: TNotifyEvent;
begin
  Result := THC(Control).OnClick;
end;

function TXN_ControlOptsBase.GetOnConstrainedResize: TConstrainedResizeEvent;
begin
  Result := THC(Control).OnConstrainedResize;
end;

function TXN_ControlOptsBase.GetOnDblClick: TNotifyEvent;
begin
  Result := THC(Control).OnDblClick;
end;

function TXN_ControlOptsBase.GetOnDragDrop: TDragDropEvent;
begin
  Result := THC(Control).OnDragDrop;
end;

function TXN_ControlOptsBase.GetOnDragOver: TDragOverEvent;
begin
  Result := THC(Control).OnDragOver;
end;

function TXN_ControlOptsBase.GetOnEndDock: TEndDragEvent;
begin
  Result := THC(Control).OnEndDock;
end;

function TXN_ControlOptsBase.GetOnEndDrag: TEndDragEvent;
begin
  Result := THC(Control).OnEndDrag;
end;

function TXN_ControlOptsBase.GetOnMouseActivate: TMouseActivateEvent;
begin
  Result := THC(Control).OnMouseActivate;
end;

function TXN_ControlOptsBase.GetOnMouseDown: TMouseEvent;
begin
  Result := THC(Control).OnMouseDown;
end;

function TXN_ControlOptsBase.GetOnMouseEnter: TNotifyEvent;
begin
  Result := THC(Control).OnMouseEnter;
end;

function TXN_ControlOptsBase.GetOnMouseLeave: TNotifyEvent;
begin
  Result := THC(Control).OnMouseLeave;
end;

function TXN_ControlOptsBase.GetOnMouseMove: TMouseMoveEvent;
begin
  Result := THC(Control).OnMouseMove;
end;

function TXN_ControlOptsBase.GetOnMouseUp: TMouseEvent;
begin
  Result := THC(Control).OnMouseUp;
end;

function TXN_ControlOptsBase.GetOnMouseWheel: TMouseWheelEvent;
begin
  Result := THC(Control).OnMouseWheel;
end;

function TXN_ControlOptsBase.GetOnMouseWheelDown: TMouseWheelUpDownEvent;
begin
  Result := THC(Control).OnMouseWheelDown;
end;

function TXN_ControlOptsBase.GetOnMouseWheelUp: TMouseWheelUpDownEvent;
begin
  Result := THC(Control).OnMouseWheelUp;
end;

function TXN_ControlOptsBase.GetOnResize: TNotifyEvent;
begin
  Result := THC(Control).OnResize;
end;

function TXN_ControlOptsBase.GetOnStartDock: TStartDockEvent;
begin
  Result := THC(Control).OnStartDock;
end;

function TXN_ControlOptsBase.GetOnStartDrag: TStartDragEvent;
begin
  Result := THC(Control).OnStartDrag;
end;

procedure TXN_ControlOptsBase.SetOnCanResize(const Value: TCanResizeEvent);
begin
  THC(Control).OnCanResize := Value;
end;

procedure TXN_ControlOptsBase.SetOnClick(const Value: TNotifyEvent);
begin
  THC(Control).OnClick := Value;
end;

procedure TXN_ControlOptsBase.SetOnConstrainedResize(
  const Value: TConstrainedResizeEvent);
begin
  THC(Control).OnConstrainedResize := Value;
end;

procedure TXN_ControlOptsBase.SetOnDblClick(const Value: TNotifyEvent);
begin
  THC(Control).OnDblClick := Value;
end;

procedure TXN_ControlOptsBase.SetOnDragDrop(const Value: TDragDropEvent);
begin
    THC(Control).OnDragDrop := Value;
end;

procedure TXN_ControlOptsBase.SetOnDragOver(const Value: TDragOverEvent);
begin
  THC(Control).OnDragOver := Value;
end;

procedure TXN_ControlOptsBase.SetOnEndDock(const Value: TEndDragEvent);
begin
  THC(Control).OnEndDock := Value;
end;

procedure TXN_ControlOptsBase.SetOnEndDrag(const Value: TEndDragEvent);
begin
  THC(Control).OnEndDrag := Value;
end;

procedure TXN_ControlOptsBase.SetOnMouseActivate(
  const Value: TMouseActivateEvent);
begin
  THC(Control).OnMouseActivate := Value;
end;

procedure TXN_ControlOptsBase.SetOnMouseDown(const Value: TMouseEvent);
begin
  THC(Control).OnMouseDown := Value;
end;

procedure TXN_ControlOptsBase.SetOnMouseEnter(const Value: TNotifyEvent);
begin
  THC(Control).OnMouseEnter := Value;
end;

procedure TXN_ControlOptsBase.SetOnMouseLeave(const Value: TNotifyEvent);
begin
  THC(Control).OnMouseLeave := Value;
end;

procedure TXN_ControlOptsBase.SetOnMouseMove(const Value: TMouseMoveEvent);
begin
  THC(Control).OnMouseMove := Value;
end;

procedure TXN_ControlOptsBase.SetOnMouseUp(const Value: TMouseEvent);
begin
  THC(Control).OnMouseUp := Value;
end;

procedure TXN_ControlOptsBase.SetOnMouseWheel(const Value: TMouseWheelEvent);
begin
  THC(Control).OnMouseWheel := Value;
end;

procedure TXN_ControlOptsBase.SetOnMouseWheelDown(
  const Value: TMouseWheelUpDownEvent);
begin
  THC(Control).OnMouseWheelDown := Value;
end;

procedure TXN_ControlOptsBase.SetOnMouseWheelUp(
  const Value: TMouseWheelUpDownEvent);
begin
  THC(Control).OnMouseWheelUp := Value;
end;

procedure TXN_ControlOptsBase.SetOnResize(const Value: TNotifyEvent);
begin
  THC(Control).OnResize := Value;
end;

procedure TXN_ControlOptsBase.SetOnStartDock(const Value: TStartDockEvent);
begin
  THC(Control).OnStartDock := Value;
end;

procedure TXN_ControlOptsBase.SetOnStartDrag(const Value: TStartDragEvent);
begin
  THC(Control).OnStartDrag := Value;
end;

{ TXN_ControlOptsTitle }

function TXN_ControlOptsTitle.GetEditControl: TXN_ControlTextBase;
begin
  Result := Parent.Parent.FTitle;
end;

function TXN_ControlOptsTitle.GetTitle: TXN_ControlTitle;
begin
  Result := EditControl as TXN_ControlTitle;
end;

{ TXN_ControlOptions }

procedure TXN_ControlOptions.Assign(Source: TPersistent);
var
  c: TXN_ControlOptions;
begin

  if Source is TXN_ControlOptions then
  begin

    c := Source as TXN_ControlOptions;
    Self.Title.Assign(c.Title);
    Self.Message.Assign(c.Message);
    Self.ProgressBar.Assign(c.ProgressBar);
    Self.CloseButton.Assign(c.CloseButton);
    Self.Image.Assign(c.Image);
    Self.Icon.Assign(c.Icon);

  end;

end;

procedure TXN_ControlOptions.Change;
begin
  if Assigned(Parent) then Parent.DoOptionsChanged;
end;

constructor TXN_ControlOptions.Create(AParent: TXN_Control);
begin
  inherited Create();
  FParent := AParent;
  FTitle := TXN_ControlOptsTitle.Create(Self);
  FMessage := TXN_ControlOptsMessage.Create(Self);
  FProgressBar := TXN_ControlOptsProgressBar.Create(Self);
  FCloseButton := TXN_ControlOptsCloseButton.Create(Self);
  FImage := TXN_ControlOptsImage.Create(Self);
  FIcon := TXN_ControlOptsIcon.Create(Self);
end;

destructor TXN_ControlOptions.Destroy;
begin
  FIcon.Free;
  FImage.Free;
  FCloseButton.Free;
  ProgressBar.Free;
  FMessage.Free;
  FTitle.Free;
  inherited;
end;

{ TXN_ControlOptsTextBase }

{ TXN_ControlOptsTextBase }

procedure TXN_ControlOptsTextBase.Assign(Source: TPersistent);
var
  c: TXN_ControlOptsTextBase;
begin
  inherited;

  if Source is TXN_ControlOptsTextBase then
  begin
    c := Source as TXN_ControlOptsTextBase;
    Text := c.Text;
  end;

end;

function TXN_ControlOptsTextBase.GetControl: TControl;
begin
  Result := GetEditControl;
end;

function TXN_ControlOptsTextBase.GetText: String;
begin
  Result := EditControl.Text;
end;

procedure TXN_ControlOptsTextBase.SetText(const Value: String);
begin
  EditControl.Text := Value;
end;

{ TXN_ControlOptsMessage }

procedure TXN_ControlOptsMessage.Assign(Source: TPersistent);
var
  c: TXN_ControlOptsMessage;
begin
  inherited;

  if Source is TXN_ControlOptsMessage then
  begin
    c := Source as TXN_ControlOptsMessage;
    Visible := c.Visible;
  end;
  
end;

function TXN_ControlOptsMessage.GetEditControl: TXN_ControlTextBase;
begin
  Result := Parent.Parent.FMessage;
end;

function TXN_ControlOptsMessage.GetMessage: TXN_ControlMessage;
begin
  Result := EditControl as TXN_ControlMessage;
end;

function TXN_ControlOptsMessage.GetVisible: Boolean;
begin
  Result := Message.Visible;
end;

procedure TXN_ControlOptsMessage.SetVisible(const Value: Boolean);
begin
  Message.Visible := Value;
end;

{ TXN_ControlOptsProgressBar }

procedure TXN_ControlOptsProgressBar.Assign(Source: TPersistent);
var
  c: TXN_ControlOptsProgressBar;
begin

  inherited;
    
  if Source is TXN_ControlOptsProgressBar then
  begin
    c := Source as TXN_ControlOptsProgressBar;
    Animated := False;
    Max := c.Max;
    Position := c.Position;
    Visible := c.Visible;
    Animated := c.Animated;
  end;

end;

function TXN_ControlOptsProgressBar.GetAnimated: Boolean;
begin
  Result := ProgressBar.Animated;
end;

function TXN_ControlOptsProgressBar.GetControl: TControl;
begin
  Result := ProgressBar;
end;

function TXN_ControlOptsProgressBar.GetMax: Integer;
begin
  Result := ProgressBar.Max;
end;

function TXN_ControlOptsProgressBar.GetPosition: Integer;
begin
  Result := ProgressBar.Position;
end;

function TXN_ControlOptsProgressBar.GetProgressBar: TXN_ControlProgressbar;
begin
  Result := Parent.Parent.FProgressBar;
end;

function TXN_ControlOptsProgressBar.GetVisible: Boolean;
begin
  Result := ProgressBar.Visible;
end;

procedure TXN_ControlOptsProgressBar.SetAnimated(const Value: Boolean);
begin
  ProgressBar.Animated := Value;
end;

procedure TXN_ControlOptsProgressBar.SetMax(const Value: Integer);
begin
  ProgressBar.Max := Value;
end;

procedure TXN_ControlOptsProgressBar.SetPosition(const Value: Integer);
begin
  ProgressBar.Position := Value;
end;

procedure TXN_ControlOptsProgressBar.SetVisible(const Value: Boolean);
begin
  ProgressBar.Visible := Value;
end;

{ TXN_ControlCloseButton }

procedure TXN_ControlCloseButton.Click;
begin
  inherited;
  Control.DoCloseRequest;
end;

procedure TXN_ControlCloseButton.CMMouseEnter(var Message: TMessage);
begin
  inherited;
  MouseInControl := True;
end;

procedure TXN_ControlCloseButton.CMMouseLeave(var Message: TMessage);
begin
  inherited;
  MouseInControl := False;
end;

constructor TXN_ControlCloseButton.Create(AOwner: TComponent;
  AControl: TXN_Control);
begin
  inherited;
  FAnim := TXA_Anim.Create(Self);
  FAnim.ScopeComponent := Self;
  FAnim.ScopePropertyName := 'AnimatePos';
  FAnim.DefaultEasing := xeLinearTween;
  FAnimatePos := 0;
end;

destructor TXN_ControlCloseButton.Destroy;
begin
  FAnim.Free;
  inherited;
end;

procedure TXN_ControlCloseButton.PaintTo(AGraphic: TGPGraphics);
const
  d = 4;
var
  br: TGPSolidBrush;
  c: TGPColor;
  pn: TGPPen;
  c2: TGPColor;
  bmp: TGPBitmap;
  g: TGPGraphics;
  dstR: TGPRectF;
  d2: Single;
begin

  bmp := TGPBitmap.Create(Width, Height, PixelFormat32bppARGB);
  g := TGPGraphics.Create(bmp);
  try

    g.SetSmoothingMode(SmoothingModeHighQuality);
    g.SetInterpolationMode(InterpolationModeHighQuality);
    g.SetPageUnit(UnitPixel);
    g.SetCompositingQuality(CompositingQualityHighSpeed);

    c := aclTransparent;
    c2 := $FF777777;
    if MouseInControl then
    begin
      c := $FF777777;
      c2 := aclWhite;
    end;
    br := TGPSolidBrush.Create(c);
    pn := TGPPen.Create(c2, 2);
    try
      g.FillEllipse(br, MakeRect(ClientRect));

      dstR := MakeRect(0.0, 0, Width, Height);
      if AnimatePos = 0 then
        d2 := 0
      else
        d2 := (Width / 2) * (AnimatePos / 100);

      d2 := (Width / 2) - d2;
      dstR.X := dstR.X + d2;
      dstR.Y := dstR.Y + d2;
      dstR.Width := dstR.Width - d2 * 2;
      dstR.Height := dstR.Height - d2 * 2;
      AGraphic.DrawImage(bmp, dstR, 0, 0, Width + 0.0, Height + 0.0, UnitPixel);

      AGraphic.DrawLine(pn, d, d, Width - d, Height - d);
      AGraphic.DrawLine(pn, Width - d, d, d, Height - d);
    finally
      br.Free;
      pn.Free;
    end;

  finally
    g.Free;
    bmp.Free;
  end;
end;

procedure TXN_ControlCloseButton.SetAnimatePos(const Value: Integer);
begin
  if FAnimatePos <> Value then
  begin
    FAnimatePos := Value;
    Invalidate;
  end;
end;

procedure TXN_ControlCloseButton.SetMouseInControl(const Value: Boolean);
begin
  if FMouseInControl <> Value then
  begin
    FMouseInControl := Value;
    ShowAnim(FMouseInControl);
  end;
end;

procedure TXN_ControlCloseButton.ShowAnim(AIn: Boolean);
begin
  AnimatePos := 0;
  FAnim.ClearValues;
  if AIn then
    FAnim.Values.AddValue(0.0, 100.0, 0.1)
  else
    FAnim.Values.AddValue(100.0, 0.0, 0.1);
  FAnim.Start;
end;

procedure TXN_ControlCloseButton.VisibleChanging;
begin
  inherited;
  if Assigned(Control) then Control.UpdateControlsBounds;
end;

{ TXN_ControlOptsCloseButton }

procedure TXN_ControlOptsCloseButton.Assign(Source: TPersistent);
var
  c: TXN_ControlOptsCloseButton;
begin
  inherited;

  if Source is TXN_ControlOptsCloseButton then
  begin
    c := Source as TXN_ControlOptsCloseButton;
    Visible := c.Visible;
  end;

end;

function TXN_ControlOptsCloseButton.CloseButton: TXN_ControlCloseButton;
begin
  Result := Parent.Parent.FCloseButton;
end;

function TXN_ControlOptsCloseButton.GetControl: TControl;
begin
  Result := CloseButton;
end;

function TXN_ControlOptsCloseButton.GetVisible: Boolean;
begin
  Result := CloseButton.Visible;
end;

procedure TXN_ControlOptsCloseButton.SetVisible(const Value: Boolean);
begin
  CloseButton.Visible := Value;
end;

{ TXN_ControlTextRender }

constructor TXN_ControlTextRender.Create();
begin
  inherited Create;
end;

function TXN_ControlTextRender.SplitText: TSplitTexts;
  procedure AddResult(AText: String; ATagType: TSplitTextTagType);
  var
    itm: TSplitText;
  begin
    SetLength(Result, Length(Result) + 1);
    itm.TagType := ATagType;
    itm.Text := AText;
    Result[Length(Result)-1] := itm;
  end;
var
  I: Integer;
  sText: String;
  tagType: TSplitTextTagType;
begin

  SetLength(Result, 0);

  sText := '';

  tagType := ttNone;

  for I := 1 to Length(Text) do
  begin

    case Text[I] of
      '&':
        begin
          if tagType = ttNone then
          begin
            tagType := ttChar;
            if sText <> '' then
              AddResult(sText, ttNone);
            sText := '';
          end else
            sText := sText + Text[I];
        end;
      ';':
        begin
          if tagType = ttChar then
          begin
            tagType := ttNone;
            if sText <> '' then
              AddResult(Trim(UpperCase(sText)), ttChar);
            sText := '';
          end else
            sText := sText + Text[I];
        end;
      '<':
        if tagType = ttNone then
        begin
          tagType := ttOpen;
          if Length(Text) > I + 1 then
            if Text[I+1] = '/' then
              tagType := ttClose;
          if sText <> '' then
            AddResult(sText, ttNone);
          sText := '';
        end else
          sText := sText + Text[I];
      '>':
        if tagType <> ttNone then
        begin
          if sText <> '' then
            AddResult(Trim(UpperCase(sText)), tagType);
          tagType := ttNone;
          sText := '';
        end else
          sText := sText + Text[I];
      else begin
        sText := sText + Text[I];
      end;
    end;

  end;

  if sText <> '' then
    AddResult(sText, tagType);

end;

procedure TXN_ControlTextRender.PaintTo(AGraphic: TGPGraphics; ARect: TGPRectF; AFont: TFont; AWordWrap: Boolean; AMeasure: Boolean);
var
  splits: TSplitTexts;
  I: Integer;
  fs: Integer;
  split: TSplitText;
  sl: TStringList;
  I2: Integer;
  font: TGPFont;
  stringFormat: TGPStringFormat;
  o: TGPRectF;
  maxHLine, X, Y: Single;
  wordsXline: Integer;
  br: TGPSolidBrush;
  maxW: Single;
  txt: string;
  c: Char;
  s: String;
  ri: Integer;
begin
  maxW := 0;
  X := ARect.X; Y := ARect.Y;
  maxHLine := 0;
  wordsXline := 0;
  fs := 0;
  splits := SplitText;
  for I := 0 to Length(splits) - 1 do
  begin

    font := TGPFont.Create(AFont.Name, AFont.Height * -1, fs, UnitPixel);
    stringFormat := TGPStringFormat.Create();
    br := TGPSolidBrush.Create(ColorRefToARGB(ColorToRGB(AFont.Color)));
    try
      split := splits[I];
      case split.TagType of
        ttOpen: begin
          if split.Text = 'B' then
            fs := fs or FontStyleBold
          else if split.Text = 'I' then
            fs := fs or FontStyleItalic
          else if split.Text = 'U' then
            fs := fs or FontStyleUnderline
          else if split.Text = 'T' then
            fs := fs or FontStyleStrikeout
          else if split.Text = 'BR' then
          begin
            X := ARect.X;
            if wordsXline > 0 then
            begin
              Y := Y + maxHLine;
            end else begin
              AGraphic.MeasureString('|', 1, font, MakePoint(X, Y), stringFormat, o);
              Y := Y + o.Height;
            end;
          end;
        end;
        ttClose: begin
          if split.Text = '/B' then
            fs := fs and not FontStyleBold
          else if split.Text = '/I' then
            fs := fs and not FontStyleItalic
          else if split.Text = '/U' then
            fs := fs and not FontStyleUnderline
          else if split.Text = '/T' then
            fs := fs and not FontStyleStrikeout;
        end;
        ttChar: begin
          c :=  #0;
          if split.Text = 'NBSP' then
            c := ' '
          else if (Length(split.Text) > 0) and (split.Text[1] = '#') then
          begin
            if (Length(split.Text) > 1) and (split.Text[2] = 'X') then
              s := '$' + Copy(split.Text, 3, Length(split.Text) - 1)
            else
              s := Copy(split.Text, 2, Length(split.Text) - 1);
            if TryStrToInt(s, ri) then
              c := Chr(ri);
          end;
          if c <> #0 then
            DrawString(AGraphic, (I = 0), c, ARect, X, Y, maxW, maxHLine, wordsXline, font, stringFormat, br, AWordWrap, AMeasure);
        end;
        ttNone: begin

          sl := TStringList.Create;
          try
            {$IFDEF DELPHI2007_UP}
            ExtractStrings([' '], [], PWideChar(split.Text), sl);
            {$ELSE}
            ExtractStrings([' '], [], PChar(split.Text), sl);
            {$ENDIF}

            for I2 := 0 to sl.Count - 1 do
            begin

              txt := sl[I2];

              if I2 = 0 then
              begin
                if Copy(split.Text, 1, 1) = ' ' then
                  txt := ' ' + txt;
              end;

              if (I2 = sl.Count - 1) then
              begin
                if Copy(split.Text, Length(split.Text), 1) = ' ' then
                  txt := txt + ' ';
              end
              else
                txt := txt + ' ';

              DrawString(AGraphic, (I = 0) and (I2 = 0), txt, ARect, X, Y, maxW, maxHLine, wordsXline, font, stringFormat, br, AWordWrap, AMeasure);

            end;

          finally
            sl.Free;
          end;

        end;
      end;

    finally
      font.Free;
      stringFormat.Free;
      br.Free;
    end;

  end;

  FResultHeight := Y + maxHLine;
  FResultWidth := maxW;

end;

procedure TXN_ControlTextRender.DrawString(AGraphic: TGPGraphics; IsFirst: Boolean; AText: string; ARect: TGPRectF; var X: Single; var Y: Single; var AMaxW: Single; var maxHLine: Single; var wordsXline: Integer; AFont: TGPFont; AStringFormat: TGPStringFormat; ABrush: TGPSolidBrush; AWordWrap, AMeasure: Boolean);
var
  o: TGPRectF;
  o2: TGPRectF;
begin
  // Measure
  AGraphic.MeasureString(AText + '_', Length(AText) + 1, AFont, MakePoint(0.0, 0), AStringFormat, o);
  AGraphic.MeasureString('_', 1, AFont, MakePoint(0.0, 0), AStringFormat, o2);
  o.Width := o.Width - o2.Width;
  if not IsFirst and AWordWrap and (X + o.Width > ARect.Width) then
  begin
    // New Line
    Y := Y + o.Height;
    X := ARect.X;
    maxHLine := o.Height;
    wordsXline := 1;
  end
  else
  begin
    maxHLine := Max(maxHLine, o.Height);
    Inc(wordsXline);
  end;
  if not AMeasure then
    AGraphic.DrawString(AText, Length(AText), AFont, MakePoint(X, Y), AStringFormat, ABrush);
  X := X + o.Width;
  AMaxW := Max(AMaxW, X);
end;


{ TXN_ControlImage }

function TXN_ControlImageBase.CalcImageRect(AW: Integer; AHeightAnim: Boolean): TGPRectF;
var
  g: TGraphic;
  dw: Single;
  dh: Single;
  d: Single;
  rArea: TGPRectF;
  vY: Single;
  ch: Integer;
  inAnim: Boolean;
begin

  FillChar(Result, SizeOf(TGPRectF), 0);
  if not AutoSize then
    if CustomHeight = -1 then
      Result.Height := 100
    else
      Result.Height := CustomHeight;

  g := Image.FinalGraphic;
  if Assigned(g) then
  begin

    inAnim := FInAnimHeight and AHeightAnim;
    if inAnim then
    begin
      ch := FAnimateHeightPos;
    end else begin
      ch := CustomHeight;
      if ch = -1 then
        ch := 100;
    end;

    rArea := MakeRect(0.0 + ImageMrg, ImageMrg, AW - (ImageMrg * 2), ch - (ImageMrg * 2));
//    rArea.Height := Max(rArea.Height, 50);

    dw := rArea.Width / g.Width;
//    dh := rArea.Height / g.Height;

    // No activar autoprop en anim
//    if not inAnim and CustomAutosize then
      d := dw;
//    else
//      d := Min(dw, dh);

    d := Min(d, 1);

    dw := g.Width * d;
    dh := g.Height * d;

    vY := 0;
    if not CustomAutoSize then
      vY := (rArea.Height - dh) / 2;

    Result := MakeRect(
      rArea.X + (rArea.Width - dw) / 2,
      rArea.Y + vY,
      dw, dh
    );

  end;

end;

function TXN_ControlImageBase.CanAutoSize(var NewWidth,
  NewHeight: Integer): Boolean;
begin
  if not FReady then
  begin
    Result := inherited CanAutoSize(NewWidth, NewHeight);
    Exit;
  end;
  if AutoSize then
    NewHeight := Ceil(CalcHeight(NewWidth));
  Result := True;
end;

constructor TXN_ControlImageBase.Create(AOwner: TComponent; AControl: TXN_Control);
begin
  inherited;
  FReady := False;
  FCustomHeight := -1;
  FCustomAutosize := True;
  FImage := TXN_ImageSrc.Create(Self);
  AutoSize := True;
  FReady := True;
end;

destructor TXN_ControlImageBase.Destroy;
begin
  FImage.Free;
  inherited;
end;

procedure TXN_ControlImageBase.UpdateAutoSize;
begin
  if Control.AutoSize then
    Control.AdjustSize
  else
    Control.UpdateControlsBounds;
end;

procedure TXN_ControlImageBase.DoImageSrcChange;
begin

  Invalidate;
  if CustomAutoSize then
    UpdateAutoSize;

end;

procedure TXN_ControlImageBase.DoSrcHasImage(AValue: Boolean);
begin
end;

function TXN_ControlImageBase.InternalCalcHeight(AW: Integer; ANoAnim: Boolean): Integer;
var
  R: TGPRectF;
  s: Single;
begin
  R := CalcImageRect(AW);
  with R do
  if CustomAutosize then
  begin
    s := R.Y + R.Height;
    if s > 0 then
      s := s + ImageMrg;
  end
  else
  begin
    if FCustomHeight = -1 then
      s := 100
    else
      s := FCustomHeight
  end;
  Result := Ceil(s);
end;

procedure TXN_ControlImageBase.PaintTo(AGraphic: TGPGraphics);
var
  g: TGraphic;
  img: TGPImage;
  sa: IStream;
  ms: TMemoryStream;
  rImage: TGPRectF;
  br: TGPSolidBrush;
begin
  br := TGPSolidBrush.Create(ColorRefToARGB(ColorToRGB(Color)));
  try
    AGraphic.FillRectangle(br, 0, 0, ClientWidth, ClientHeight);
    g := Image.FinalGraphic;
    if Assigned(g) and not g.Empty then
    begin
      ms := TMemoryStream.Create;
      try
        g.SaveToStream(ms);
        ms.Position := 0;
        sa := TStreamAdapter.Create(ms);
        img := TGPImage.Create(sa);

        rImage := CalcImageRect(Width, True);

        try
          AGraphic.DrawImage(img, rImage);
        finally
          img.Free;
        end;
      finally
        ms.Free;
      end;
    end;
  finally
    br.Free;
  end;
end;

procedure TXN_ControlImageBase.SetCustomAutosize(const Value: Boolean);
begin
  FCustomAutosize := Value;
  UpdateAutoSize;
end;

procedure TXN_ControlImageBase.SetCustomHeight(const Value: Integer);
begin
  if FCustomHeight <> Value then
  begin
    FCustomHeight := Value;
    if not CustomAutoSize then UpdateAutoSize;
  end;
end;

{ TXN_ControlImage }

constructor TXN_ControlImage.Create(AOwner: TComponent; AControl: TXN_Control);
begin
  inherited;
  Image.SrcType := stPicture;
  Color := $00222222;
end;

procedure TXN_ControlImage.DoImageSrcChange;
begin
  inherited;
end;

procedure TXN_ControlImage.DoSrcHasImage(AValue: Boolean);
begin
  inherited;
  ShowAnimHeight(AValue);
end;

destructor TXN_ControlImage.Destroy;
begin
  inherited;
end;

{ TXN_ImageSrc }

procedure TXN_ImageSrc.Assign(Source: TPersistent);
var
  c: TXN_ImageSrc;
begin

  if Source is TXN_ImageSrc then
  begin
    c := Source as TXN_ImageSrc;
    Picture.Assign(c.Picture);
    FURL := c.URL;
    FSrcType := c.SrcType;
    if Assigned(FUrlGraphic) then
      FreeAndNil(FUrlGraphic);
    if Assigned(c.FUrlGraphic) then
    begin
      FUrlGraphic := TGraphicClass(c.FUrlGraphic.ClassType).Create;
      FUrlGraphic.Assign(c.FUrlGraphic);
    end;
    FLoaded := c.FLoaded;
    FOldHasImage := c.FOldHasImage;
    DoChange;

  end;

end;

constructor TXN_ImageSrc.Create(AImage: TXN_ControlImageBase);
begin
  inherited Create();
  FOldHasImage := False;
  FSystemType := ssApplication;
  FSystemIcon := TIcon.Create;
  FSrcType := stSystem;
  FLoaded := False;
  FImage := AImage;
  FPicture := TPicture.Create;
  FPicture.OnChange := DoPictureChange;
end;

function TXN_ImageSrc.FinalGraphic: TGraphic;
const
  a: array[TXN_ImageSystemType] of {$IFDEF DELPHI2009_UP}PWideChar{$ELSE}PChar{$ENDIF} = (
    IDI_APPLICATION,
    IDI_ASTERISK,
    IDI_ERROR,
    IDI_EXCLAMATION,
    IDI_HAND,
    IDI_INFORMATION,
    IDI_QUESTION,
    {$IFDEF DELPHI2009_UP}IDI_SHIELD{$ELSE}nil{$ENDIF},
    IDI_WARNING,
    IDI_WINLOGO
  );
begin
  case SrcType of
    stUrl: Result := UrlGraphic;
    stSystem:
    begin
      if FSystemIcon.Empty then
        if SystemType <> ssApplication then
          FSystemIcon.Handle := LoadIcon(0, a[SystemType])
        else
          if Assigned(Application.Icon) then
            FSystemIcon.Handle := Application.Icon.Handle
          else
            FSystemIcon.Handle := 0;
      Result := FSystemIcon;
    end;
    //stPicture: ;
    else Result := Picture.Graphic;
  end;
end;

destructor TXN_ImageSrc.Destroy;
begin
  FSystemIcon.Free;
  if Assigned(FUrlGraphic) then
    FreeAndNil(FUrlGraphic);
  FPicture.Free;
  inherited;
end;

procedure TXN_ImageSrc.DoChange;
var
  B: Boolean;
begin
  B := (FinalGraphic <> nil) and (not FinalGraphic.Empty);
  if FOldHasImage <> B then
  begin
    Image.DoSrcHasImage(B);
  end;
  FOldHasImage := B;
  Image.DoImageSrcChange;
end;

procedure TXN_ImageSrc.DoPictureChange(Sender: TObject);
var
  s: TXN_ImageSrcType;
begin
  s := SrcType;
  SrcType := stPicture;
  if s = stPicture then
    DoChange;
end;

procedure TXN_ImageSrc.DoProcFinish(ASender: TXP_Base);
var
  g: TGraphic;
begin
  with (ASender as TXN_ImageSrc_Proc) do
  begin
    if ResultOk then
    begin
      g := TGraphicClass(Result.Graphic.ClassType).Create;
      g.Assign(Result.Graphic);
      FUrlGraphic := g;
    end;
    FIsLoading := False;
    FLoaded := True;
    DoChange;
  end;
end;

function TXN_ImageSrc.GetUrlGraphic: TGraphic;
begin
  if (not FLoaded) and (not IsLoading) then
    LoadUrlGraphic;
  Result := FUrlGraphic;
end;

function TXN_ImageSrc.HasGraphic: Boolean;
var
  g: TGraphic;
begin
  g := FinalGraphic;
  if Assigned(g) then
    Result := not g.Empty
  else
    Result := False;
end;

function TXN_ImageSrc.IsLoading: Boolean;
begin
  Result := FIsLoading;
end;

procedure TXN_ImageSrc.LoadUrlGraphic;
var
  p: TXN_ImageSrc_Proc;
begin
  if not FIsLoading then
  begin
    FIsLoading := True;
    if Trim(URL) = '' then
      FIsLoading := False
    else begin
      p := TXN_ImageSrc_Proc.Create(URL);
      p.OnFinish := DoProcFinish;
      Manager.ExecuteProcessAsync(p);
    end;
  end;
end;

procedure TXN_ImageSrc.SetPicture(const Value: TPicture);
begin
  FPicture.Assign(Value);
end;

procedure TXN_ImageSrc.SetSrcType(const Value: TXN_ImageSrcType);
begin
  if Value <> FSrcType then
  begin
    FSrcType := Value;
    DoChange;
  end;
end;

procedure TXN_ImageSrc.SetSystemType(const Value: TXN_ImageSystemType);
begin
  if Value <> FSystemType then
  begin
    FSystemType := Value;
    FSystemIcon.Free;
    FSystemIcon := TIcon.Create;
    DoChange;
  end;
end;

procedure TXN_ImageSrc.SetURL(const Value: String);
begin
  if FURL <> Value then
  begin
    FURL := Value;
    FLoaded := False;
    if Assigned(FUrlGraphic) then
      FreeAndNil(FUrlGraphic);
    SrcType := stUrl;
    DoChange;
  end;
end;

{ TXN_ImageSrc_Proc }

constructor TXN_ImageSrc_Proc.Create(AUrl: String);
begin
  FUrl := AUrl;
  inherited Create();
  FResult := TPicture.Create;
end;

destructor TXN_ImageSrc_Proc.Destroy;
begin
  FResult.Free;
  inherited;
end;

procedure TXN_ImageSrc_Proc.Execute(AProc: TXP_Process);
var
  http: TIdHTTP;
  ms: TMemoryStream;
  c: string;
  gc: TGraphicClass;
  url, e: String;
  g: TGraphic;
begin
  inherited;

  FResultOk := False;
  try

    url := FUrl;

    http := TIdHTTP.Create(nil);
    try
      ms := TMemoryStream.Create;
      try
        http.Get(url, ms);
        ms.Position := 0;
        c := Trim(LowerCase(http.Response.ContentType));

        gc := nil;
        e := LowerCase(ExtractFileExt(url));
        if (c = 'image/jpeg') or (c = 'image/pjpeg') or (e = '.jpg') or (e = '.jpeg') then
          gc := TJPEGImage
        else if (c = 'image/png') or (e = '.png') then
          gc := TPngImage
        else if (c = 'image/bmp') or (c = 'image/x-windows-bmp') or (e = '.bmp') then
          gc := TBitmap
        else if (c = 'image/gif') or (e = '.gif') then
          gc := TGIFImage
        else if (c = 'image/ico') or (c = 'image/icon') or (c = 'text/ico') or (c = 'application/ico') or (c = 'image/vnd.microsoft.icon') or (e = '.ico') then
          gc := TIcon;

        if Assigned(gc) then
        begin
          g := gc.Create;
          try
            g.LoadFromStream(ms);
            Result.Graphic := g;
            FResultOk := True;
          finally
            g.Free;
          end;
        end;

      finally
        ms.Free;
      end;
    finally
      http.Free;
    end;
  except
    FResultOk := False;
  end;

end;

{ TXN_ControlOptsImage }

procedure TXN_ControlOptsImageBase.Assign(Source: TPersistent);
var
  c: TXN_ControlOptsImageBase;
begin
  inherited;

  if Source is TXN_ControlOptsImageBase then
  begin
    c := Source as TXN_ControlOptsImageBase;
    Image.Assign(c.Image);
    Color := c.Color;
    Visible := c.Visible;
    Height := c.Height;
    AutoSize := AutoSize;
  end;

end;

constructor TXN_ControlOptsImageBase.Create(AParent: TXN_ControlOptions);
begin
  inherited;
end;

function TXN_ControlOptsImageBase.GetAutoSize: Boolean;
begin
  Result := ControlImage.CustomAutosize;
end;

function TXN_ControlOptsImageBase.GetColor: TColor;
begin
  Result := ControlImage.Color;
end;

function TXN_ControlOptsImageBase.GetControl: TControl;
begin
  Result := ControlImage;
end;

function TXN_ControlOptsImageBase.GetImage: TXN_ImageSrc;
begin
  Result := ControlImage.Image;
end;

function TXN_ControlOptsImageBase.GetVisible: Boolean;
begin
  Result := ControlImage.Visible;
end;

function TXN_ControlOptsImageBase.GetHeight: Integer;
begin
  Result := ControlImage.CustomHeight;
end;

procedure TXN_ControlOptsImageBase.SetAutoSize(const Value: Boolean);
begin
  ControlImage.CustomAutosize := Value;
end;

procedure TXN_ControlOptsImageBase.SetColor(const Value: TColor);
begin
  ControlImage.Color := Value;
end;

procedure TXN_ControlOptsImageBase.SetImage(const Value: TXN_ImageSrc);
begin
  ControlImage.Image := Value;
end;

procedure TXN_ControlOptsImageBase.SetVisible(const Value: Boolean);
begin
  ControlImage.Visible := Value;
end;

procedure TXN_ControlOptsImageBase.SetHeight(const Value: Integer);
begin
  ControlImage.CustomHeight := Value;
end;

{ TXN_ControlOptsImage }

procedure TXN_ControlOptsImage.Assign(Source: TPersistent);
//var
//  c: TXN_ControlOptsImage;
begin
  inherited;
  if Source is TXN_ControlOptsImage then
  begin
//    c := Source as TXN_ControlOptsImage;
    ControlImage.FInAnimHeight := False;
  end;
end;

function TXN_ControlOptsImage.GetControlImage: TXN_ControlImageBase;
begin
  Result := Parent.Parent.FImage;
end;

function TXN_ControlOptsImage.GetControlImage2: TXN_ControlImage;
begin
  Result := inherited ControlImage as TXN_ControlImage;
end;

{ TXN_ControlButtons }

constructor TXN_ControlButtons.Create(AControl: TXN_Control);
begin
  inherited Create(AControl, TXN_ControlButtonsItem);
  FControl := AControl;
end;

function TXN_ControlButtons.GetItem(Index: Integer): TXN_ControlButtonsItem;
begin
  Result := inherited Items[Index] as TXN_ControlButtonsItem;
end;

procedure TXN_ControlButtons.Notify(Item: TCollectionItem;
  Action: TCollectionNotification);
begin
  inherited;
end;

procedure TXN_ControlButtons.SetItem(Index: Integer;
  const Value: TXN_ControlButtonsItem);
begin
  inherited Items[Index] := Value;
end;

{ TXN_ControlButtonsItem }

procedure TXN_ControlButtonsItem.Assign(Source: TPersistent);
var
  c: TXN_ControlButtonsItem;
begin

  if Source is TXN_ControlButtonsItem then
  begin
    c := Source as TXN_ControlButtonsItem;
    Text := c.Text;

    OnClick := c.OnClick;
    OnDblClick := c.OnDblClick;
    OnCanResize := c.OnCanResize;
    OnConstrainedResize := c.OnConstrainedResize;
    OnDragDrop := c.OnDragDrop;
    OnDragOver := c.OnDragOver;
    OnEndDock := c.OnEndDock;
    OnEndDrag := c.OnEndDrag;
    OnMouseActivate := c.OnMouseActivate;
    OnMouseDown := c.OnMouseDown;
    OnMouseEnter := c.OnMouseEnter;
    OnMouseLeave := c.OnMouseLeave;
    OnMouseUp := c.OnMouseUp;
    OnMouseWheel := c.OnMouseWheel;
    OnMouseMove := c.OnMouseMove;
    OnMouseWheelDown := c.OnMouseWheelDown;
    OnMouseWheelUp := c.OnMouseWheelUp;
    
  end;

end;

procedure TXN_ControlButtonsItem.Click;
begin
  if Assigned(FOnClick) then
    FOnClick(Self);
  if CloseOnClick then
    Buttons.Control.DoCloseRequest;
end;

constructor TXN_ControlButtonsItem.Create(Collection: TCollection);
begin
  inherited;
  FCloseOnClick := True;
  FReady := False;
  FControl := TXN_ControlButton.Create(nil, Self, Buttons.Control);
  FControl.Parent := Buttons.Control;
  FControl.Text := Self.ClassName + ' ' + IntToStr(Index+1);
  FControl.OnClick := DoButtonClick;
  Buttons.Control.UpdateAllBounds;
  FReady := True;
end;

destructor TXN_ControlButtonsItem.Destroy;
begin
  if Assigned(Buttons) and Assigned(Buttons.Control) then Buttons.Control.UpdateAllBounds;
  FControl.Free;
  inherited;
end;

procedure TXN_ControlButtonsItem.DoButtonClick(Sender: TObject);
begin                                                          
  Click;
end;

function TXN_ControlButtonsItem.GetButtons: TXN_ControlButtons;
begin
  Result := Collection as TXN_ControlButtons;
end;

function TXN_ControlButtonsItem.GetText: String;
begin
  Result := Control.Text;
end;

procedure TXN_ControlButtonsItem.SetCloseOnClick(const Value: Boolean);
begin
  FCloseOnClick := Value;
end;

procedure TXN_ControlButtonsItem.SetControlEmpty(
  const Value: TXN_ControlButton);
begin
end;

procedure TXN_ControlButtonsItem.SetText(const Value: String);
begin
  Control.Text := Value;
end;

function TXN_ControlButtonsItem.GetOnCanResize: TCanResizeEvent;
begin
  Result := THC(FControl).OnCanResize;
end;

function TXN_ControlButtonsItem.GetOnConstrainedResize: TConstrainedResizeEvent;
begin
  Result := THC(FControl).OnConstrainedResize;
end;

function TXN_ControlButtonsItem.GetOnDblClick: TNotifyEvent;
begin
  Result := THC(FControl).OnDblClick;
end;

function TXN_ControlButtonsItem.GetOnDragDrop: TDragDropEvent;
begin
  Result := THC(FControl).OnDragDrop;
end;

function TXN_ControlButtonsItem.GetOnDragOver: TDragOverEvent;
begin
  Result := THC(FControl).OnDragOver;
end;

function TXN_ControlButtonsItem.GetOnEndDock: TEndDragEvent;
begin
  Result := THC(FControl).OnEndDock;
end;

function TXN_ControlButtonsItem.GetOnEndDrag: TEndDragEvent;
begin
  Result := THC(FControl).OnEndDrag;
end;

function TXN_ControlButtonsItem.GetOnMouseActivate: TMouseActivateEvent;
begin
  Result := THC(FControl).OnMouseActivate;
end;

function TXN_ControlButtonsItem.GetOnMouseDown: TMouseEvent;
begin
  Result := THC(FControl).OnMouseDown;
end;

function TXN_ControlButtonsItem.GetOnMouseEnter: TNotifyEvent;
begin
  Result := THC(FControl).OnMouseEnter;
end;

function TXN_ControlButtonsItem.GetOnMouseLeave: TNotifyEvent;
begin
  Result := THC(FControl).OnMouseLeave;
end;

function TXN_ControlButtonsItem.GetOnMouseMove: TMouseMoveEvent;
begin
  Result := THC(FControl).OnMouseMove;
end;

function TXN_ControlButtonsItem.GetOnMouseUp: TMouseEvent;
begin
  Result := THC(FControl).OnMouseUp;
end;

function TXN_ControlButtonsItem.GetOnMouseWheel: TMouseWheelEvent;
begin
  Result := THC(FControl).OnMouseWheel;
end;

function TXN_ControlButtonsItem.GetOnMouseWheelDown: TMouseWheelUpDownEvent;
begin
  Result := THC(FControl).OnMouseWheelDown;
end;

function TXN_ControlButtonsItem.GetOnMouseWheelUp: TMouseWheelUpDownEvent;
begin
  Result := THC(FControl).OnMouseWheelUp;
end;

function TXN_ControlButtonsItem.GetOnResize: TNotifyEvent;
begin
  Result := THC(FControl).OnResize;
end;

function TXN_ControlButtonsItem.GetOnStartDock: TStartDockEvent;
begin
  Result := THC(FControl).OnStartDock;
end;

function TXN_ControlButtonsItem.GetOnStartDrag: TStartDragEvent;
begin
  Result := THC(FControl).OnStartDrag;
end;

procedure TXN_ControlButtonsItem.SetOnCanResize(const Value: TCanResizeEvent);
begin
  THC(FControl).OnCanResize := Value;
end;

procedure TXN_ControlButtonsItem.SetOnConstrainedResize(
  const Value: TConstrainedResizeEvent);
begin
  THC(FControl).OnConstrainedResize := Value;
end;

procedure TXN_ControlButtonsItem.SetOnDblClick(const Value: TNotifyEvent);
begin
  THC(FControl).OnDblClick := Value;
end;

procedure TXN_ControlButtonsItem.SetOnDragDrop(const Value: TDragDropEvent);
begin
    THC(FControl).OnDragDrop := Value;
end;

procedure TXN_ControlButtonsItem.SetOnDragOver(const Value: TDragOverEvent);
begin
  THC(FControl).OnDragOver := Value;
end;

procedure TXN_ControlButtonsItem.SetOnEndDock(const Value: TEndDragEvent);
begin
  THC(FControl).OnEndDock := Value;
end;

procedure TXN_ControlButtonsItem.SetOnEndDrag(const Value: TEndDragEvent);
begin
  THC(FControl).OnEndDrag := Value;
end;

procedure TXN_ControlButtonsItem.SetOnMouseActivate(
  const Value: TMouseActivateEvent);
begin
  THC(FControl).OnMouseActivate := Value;
end;

procedure TXN_ControlButtonsItem.SetOnMouseDown(const Value: TMouseEvent);
begin
  THC(FControl).OnMouseDown := Value;
end;

procedure TXN_ControlButtonsItem.SetOnMouseEnter(const Value: TNotifyEvent);
begin
  THC(FControl).OnMouseEnter := Value;
end;

procedure TXN_ControlButtonsItem.SetOnMouseLeave(const Value: TNotifyEvent);
begin
  THC(FControl).OnMouseLeave := Value;
end;

procedure TXN_ControlButtonsItem.SetOnMouseMove(const Value: TMouseMoveEvent);
begin
  THC(FControl).OnMouseMove := Value;
end;

procedure TXN_ControlButtonsItem.SetOnMouseUp(const Value: TMouseEvent);
begin
  THC(FControl).OnMouseUp := Value;
end;

procedure TXN_ControlButtonsItem.SetOnMouseWheel(const Value: TMouseWheelEvent);
begin
  THC(FControl).OnMouseWheel := Value;
end;

procedure TXN_ControlButtonsItem.SetOnMouseWheelDown(
  const Value: TMouseWheelUpDownEvent);
begin
  THC(FControl).OnMouseWheelDown := Value;
end;

procedure TXN_ControlButtonsItem.SetOnMouseWheelUp(
  const Value: TMouseWheelUpDownEvent);
begin
  THC(FControl).OnMouseWheelUp := Value;
end;

procedure TXN_ControlButtonsItem.SetOnResize(const Value: TNotifyEvent);
begin
  THC(FControl).OnResize := Value;
end;

procedure TXN_ControlButtonsItem.SetOnStartDock(const Value: TStartDockEvent);
begin
  THC(FControl).OnStartDock := Value;
end;

procedure TXN_ControlButtonsItem.SetOnStartDrag(const Value: TStartDragEvent);
begin
  THC(FControl).OnStartDrag := Value;
end;

{ TXN_ControlButton }

function TXN_ControlButton.InternalCalcHeight(AW: Integer; ANoAnimHeight: Boolean): Integer;
var
  g: TGPGraphics;
  DC: THandle;
begin

  DC := GetDC(0);
  try
    g := TGPGraphics.Create(DC);
    try
      Result := DrawText(g, Width, Height, True);
    finally
      g.Free;
    end;
  finally
    ReleaseDC(0, DC);
  end;
end;

procedure TXN_ControlButton.CMMouseEnter(var Message: TMessage);
begin
  inherited;
  MouseInControl := True;
end;

procedure TXN_ControlButton.CMMouseLeave(var Message: TMessage);
begin
  inherited;
  MouseInControl := False;
end;

procedure TXN_ControlButton.CMTextChanged(var Message: TMessage);
begin
  Control.UpdateAllBounds;
end;

constructor TXN_ControlButton.Create(AOwner: TComponent; AItem: TXN_ControlButtonsItem; AControl: TXN_Control);
begin
  inherited Create(AOwner, AControl);
  FItem := AItem;
  FRender := TXN_ControlTextRender.Create;
end;

destructor TXN_ControlButton.Destroy;
begin
  FRender.Free;
  inherited;
end;

function TXN_ControlButton.DrawText(AGraphic: TGPGraphics; AW: Integer; AH: Integer; AMeasure: Boolean): Integer;
const
  mrg = 10;
var
  R: TGPRectF;
  hasLine: Boolean;
  pn: TGPPen;
  br: TGPSolidBrush;
begin

  R := MakeRect(0.0, 0, AW, AH);

  if not AMeasure then
  begin

    if MouseInControl then
    begin
      br := TGPSolidBrush.Create($FFE3E3E3);
      try
        AGraphic.FillRectangle(br, R);
      finally
        br.Free;
      end;
    end;

  end;

  R := MakeRect(0.0 + mrg, mrg, AW - (mrg * 2), AH - (mrg * 2));

  if (Text = '') and AMeasure then
    FRender.Text := '|'
  else
    FRender.Text := Text;
  FRender.PaintTo(AGraphic, R, Self.Font, True, AMeasure);
  hasLine := (Item.Index < Item.Buttons.Count - 1);
  if hasLine and (not AMeasure) then
  begin
    pn := TGPPen.Create($FFeaeaea);
    try
      AGraphic.DrawLine(pn, 0, Height - 1, Width, Height - 1);
    finally
      pn.Free;
    end;
  end;
  Result := Round(R.Y + FRender.ResultHeight);

end;

procedure TXN_ControlButton.PaintTo(AGraphic: TGPGraphics);
begin
  DrawText(AGraphic, Width, Height, False);
end;

procedure TXN_ControlButton.SetMouseInControl(const Value: Boolean);
begin
  if FMouseInControl <> Value then
  begin
    FMouseInControl := Value;
    Invalidate;
  end;
end;

{ TXN_ControlIcon }

constructor TXN_ControlIcon.Create(AOwner: TComponent; AControl: TXN_Control);
begin
  inherited;
  Color := $00a0a0a0;
end;

{ TXN_ControlOptsIcon }

function TXN_ControlOptsIcon.GetControlImage: TXN_ControlImageBase;
begin
  Result := Parent.Parent.FIcon;
end;

{ TXN_Window }

procedure TXN_Window.CMMouseEnter(var Message: TMessage);
begin
  inherited;
  MouseInWindow := True;
end;

procedure TXN_Window.CMMouseLeave(var Message: TMessage);
begin
  inherited;
  MouseInWindow := False;
end;

procedure TXN_Window.CMVisibleChanged(var Message: TMessage);
begin
  inherited;
  FVisible := inherited Visible;
  UpdateManagerBounds;
end;

class function TXN_Window.Create(AOwner: TComponent): TXN_Window;
begin
  Result := TXN_Window.CreateWindow(AOwner);
  Result.InvalidateLayer;
end;

procedure TXN_Window.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  Params.Style := Params.Style or WS_POPUP and not (WS_MINIMIZEBOX or WS_MAXIMIZEBOX or WS_SYSMENU);
  Params.ExStyle := Params.ExStyle or WS_EX_NOACTIVATE and not WS_EX_APPWINDOW;
  if FStayOnTop then
    Params.ExStyle := Params.ExStyle or WS_EX_TOPMOST
  else
    Params.ExStyle := Params.ExStyle and not WS_EX_TOPMOST;
end;

constructor TXN_Window.CreateWindow(AOwner: TComponent);
begin
  FAutoFree := False;
  FFirstWindowBounds := True;
  inherited CreateNew(AOwner);
  FPaused := False;
  FPauseOnMouseEnter := True;
  FMouseInWindow := False;
  FVisible := False;
  FTimer := TTimer.Create(Self);
  FTimer.Enabled := False;
  FTimer.OnTimer := DoTimer;
  FDuration := -1;
  FInAnimOpen := False;
  Visible := False;
  SetDesigning(False);
  ControlStyle := ControlStyle + [csNoDesignVisible];
  FAnimCloseValue2 := 0;
  FInAnimClose1 := False;
  FInAnimClose2 := False;
  FAnimCloseValue := 0;
  FAnimClose := TXA_Anim.Create(Self);
  FAnimClose.ScopeComponent := Self;
  FAnimClose.DefaultEasing := xeOutQuad;
  FAnimTop := TXA_Anim.Create(Self);
  FAnimTop.ScopeComponent := Self;
  FAnimTop.ScopePropertyName := 'AnimTopValue';
  FAnimTop.DefaultEasing := xeOutQuad;
//  DoubleBuffered := True;
  BorderStyle := bsNone;

  FControl := TXN_ControlWindowed.Create(Self, Self);
  FControl.Margins.SetBounds(ShadowWidth, ShadowWidth, ShadowWidth, ShadowWidth);
  FControl.AlignWithMargins := True;
  FControl.Align := alTop;
  FControl.Parent := Self;
  FControl.AutoSize := True;
  AutoSize := True;

  ParentWindow := GetDesktopWindow;
  FStayOnTop := False;
  StayOnTop := True;

  Manager := TXN_Manager.Default;

end;

destructor TXN_Window.Destroy;
begin
  FAnimTop.Free;
  FAnimClose.Free;
  FTimer.OnTimer := nil;
  FTimer.Enabled := False;
  FTimer.Free;
  Manager := nil;
  FControl.Free;
  inherited;
end;

procedure TXN_Window.Show(ADuration: Integer);
begin
  Duration := ADuration;
  Show;
end;

procedure TXN_Window.Show;
begin
  if not Visible then begin
    FFirstWindowBounds := True;
    if Assigned(Manager) then
      Manager.SendToEnd(Self);
  end;
  Self.SetDesigning(False);

  if not Visible then
  begin
    AnimCloseValue2 := 1;
    if not FInAnimOpen then
    begin
      FInAnimOpen := True;
      ShowAnimOpen;
    end;
  end;

  Visible := True;

  FTimer.Enabled := False;
  if Duration <> -1 then
  begin
    FTimer.Interval := Duration;
    FTimer.Enabled := True;
  end;

end;

procedure TXN_Window.ShowAnimClose;
begin
  if Control.Options.Image.Image.HasGraphic or (Control.Buttons.Count > 0) then
    ShowAnimClose1
  else
    ShowAnimClose2;
end;

procedure TXN_Window.ShowAnimClose1;
begin
  Control.FImage.ShowAnimHeight(False);
  Control.AutoSize := False;
  FInAnimClose1 := True;
  FAnimClose.ScopePropertyName := 'AnimCloseValue';
  FAnimClose.OnComplete := DoAnimCloseComplete1;
  FAnimClose.Stop;
  FAnimClose.ClearValues;
  FAnimClose.Values.AddValue(Height, Control.FImage.Top, 0.5);
  FAnimClose.Start;
end;

procedure TXN_Window.ShowAnimClose2;
begin
  FInAnimClose2 := True;
  FAnimClose.ScopePropertyName := 'AnimCloseValue2';
  FAnimClose.OnComplete := DoAnimCloseComplete2;
  FAnimClose.Stop;
  FAnimClose.ClearValues;
  FAnimClose.Values.AddValue(0, 1, 0.5);
  FAnimClose.Start;
end;

procedure TXN_Window.ShowAnimOpen;
begin
  AutoSize := True;
  if not Assigned(FAnimClose) then Exit;
  FInAnimOpen := True;
  FAnimClose.ScopePropertyName := 'AnimCloseValue2';
  FAnimClose.OnComplete := DoAnimOpenComplete;
  FAnimClose.Stop;
  FAnimClose.ClearValues;
  FAnimClose.Values.AddValue(1, 0, 0.5);
  FAnimClose.Start;
end;

procedure TXN_Window.VisibleChanging;
begin
  inherited;
end;

procedure TXN_Window.WMActivate(var Message: TMessage);
begin
  Message.Result := 0;
end;

procedure TXN_Window.DoAnimCloseComplete1(Sender: TObject);
begin
  FAnimCloseValue := 0;
  FInAnimClose1 := False;
  ShowAnimClose2;
end;

procedure TXN_Window.DoAnimCloseComplete2(Sender: TObject);
begin
  if AutoFree then
    Free
  else begin
    Hide;
    Control.FImage.ShowAnimHeight(True);
    Control.AutoSize := True;
    FAnimCloseValue2 := 0;
    FInAnimClose2 := False;
  end;
end;

procedure TXN_Window.DoAnimOpenComplete(Sender: TObject);
begin
  FAnimCloseValue2 := 0;
  FInAnimClose2 := False;
end;

procedure TXN_Window.DoClose(var Action: TCloseAction);
begin
  inherited;
  FTimer.Enabled := False;
  Action := caNone;
  if (not FInAnimClose1) and (not FInAnimClose2) then
  begin
    ShowAnimClose;
  end;
end;

procedure TXN_Window.DoHide;
begin
  inherited;
  FTimer.Enabled := False;
  FInAnimOpen := False;
  UpdateManagerBounds;
end;

procedure TXN_Window.DoTimer(Sender: TObject);
begin
  FTimer.Enabled := False;
  Close;
end;

function TXN_Window.GetVisible: Boolean;
begin
  Result := FVisible;
end;

function TXN_Window.CreateRenderBitmap: TGPBitmap;
var
  g: TGPGraphics;
begin
  Result := TGPBitmap.Create(Width - (ShadowWidth * 2), Height - (ShadowWidth * 2), PixelFormat32bppARGB);
  g := TGPGraphics.Create(Result);
  try
    Control.PaintTo(g);
  finally
    g.Free;
  end;
end;

procedure TXN_Window.RenderShadow(g: TGPGraphics; AW, AH: Integer);
var
  rs: TResourceStream;
  img: TGPImage;
  s: TStreamAdapter;
  fromR, toR: TGPRectF;
begin
  rs := TResourceStream.Create(HInstance, 'SHADOW', RT_RCDATA);
  s := TStreamAdapter.Create(rs);
  try
    img := TGPImage.Create(s);
    try

      // NO
      fromR := MakeRect(0.0, 0.0, ShadowWidth, ShadowWidth);
      toR := fromR;
      g.DrawImage(img, toR, fromR.x, fromR.y, fromR.Width, fromR.Height, UnitPixel);

      // N
      fromR := MakeRect(ShadowWidth, 0.0, img.GetWidth - (ShadowWidth * 2), ShadowWidth);
      toR := MakeRect(ShadowWidth, 0.0, AW - (ShadowWidth * 2), ShadowWidth);
      g.DrawImage(img, toR, fromR.x, fromR.y, fromR.Width, fromR.Height, UnitPixel);

      // NE
      fromR := MakeRect(img.GetWidth - ShadowWidth, 0.0, ShadowWidth, ShadowWidth);
      toR := MakeRect(AW - ShadowWidth, 0.0, ShadowWidth, ShadowWidth);
      g.DrawImage(img, toR, fromR.x, fromR.y, fromR.Width, fromR.Height, UnitPixel);

      // SO
      fromR := MakeRect(0.0, img.GetHeight - ShadowWidth, ShadowWidth, ShadowWidth);
      toR := MakeRect(0.0, AH - ShadowWidth, ShadowWidth, ShadowWidth);
      g.DrawImage(img, toR, fromR.x, fromR.y, fromR.Width, fromR.Height, UnitPixel);

      // S
      fromR := MakeRect(ShadowWidth, img.GetHeight - ShadowWidth, img.GetWidth - (ShadowWidth * 2), ShadowWidth);
      toR := MakeRect(0.0 + ShadowWidth, AH - ShadowWidth, AW - (ShadowWidth * 2), ShadowWidth);
      g.DrawImage(img, toR, fromR.x, fromR.y, fromR.Width, fromR.Height, UnitPixel);

      // SE
      fromR := MakeRect(img.GetWidth - ShadowWidth, img.GetHeight - ShadowWidth, ShadowWidth, ShadowWidth);
      toR := MakeRect(0.0 + AW - ShadowWidth, AH - ShadowWidth, ShadowWidth, ShadowWidth);
      g.DrawImage(img, toR, fromR.x, fromR.y, fromR.Width, fromR.Height, UnitPixel);

      // O
      fromR := MakeRect(0.0, ShadowWidth, ShadowWidth, img.GetHeight - (ShadowWidth * 2));
      toR := MakeRect(0.0, ShadowWidth, ShadowWidth, AH - (ShadowWidth * 2));
      g.DrawImage(img, toR, fromR.x, fromR.y, fromR.Width, fromR.Height, UnitPixel);

      // E
      fromR := MakeRect(img.GetWidth - ShadowWidth, ShadowWidth, ShadowWidth, img.GetHeight - (ShadowWidth * 2));
      toR := MakeRect(0.0 + AW - ShadowWidth, ShadowWidth, ShadowWidth, AH - (ShadowWidth * 2));
      g.DrawImage(img, toR, fromR.x, fromR.y, fromR.Width, fromR.Height, UnitPixel);

    finally
      img.Free;
    end;
  finally
    rs.Free;
  end;
end;

procedure TXN_Window.Resize;
begin
  inherited;
  InvalidateLayer;
end;

procedure TXN_Window.SetAnimHeightValue(const Value: Integer);
begin
  if FAnimCloseValue <> Value then
  begin
    FAnimCloseValue := Value;
    Control.Height := Value;
    if Assigned(Manager) then Manager.DoWindowUpdate;
  end;
end;

procedure TXN_Window.SetAnimTopValue(const Value: Integer);
begin
  if FAnimTopValue <> Value then
  begin
    FAnimTopValue := Value;
    Top := Value;
    if Assigned(Manager) then Manager.DoWindowUpdate;
  end;
end;

procedure TXN_Window.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
end;

procedure TXN_Window.SetMouseInWindow(const Value: Boolean);
begin
  if MouseInWindow <> Value then
  begin
    FMouseInWindow := Value;
    if Value and PauseOnMouseEnter then
    begin
      FPaused := FTimer.Enabled;
      FTimer.Enabled := False;
    end
    else
      if FPaused then
      begin
        FTimer.Enabled := True;
        FPaused := False;
      end;
  end;
end;

function TXN_Window.SelectDesign(AControl: TControl): Boolean;
begin
  Result := False;
  if Assigned(FOnSelectDesign) then
    Result := FOnSelectDesign(Self, AControl);
end;

procedure TXN_Window.SetAnimCloseValue2(const Value: Single);
begin
  if FAnimCloseValue2 <> Value then
  begin
    FAnimCloseValue2 := Value;
    InvalidateLayer;
  end;
end;

procedure TXN_Window.SetManager(const Value: TXN_Manager);
begin
  if FManager <> Value then
  begin
    if Assigned(FManager) then
    begin
      FManager.FList.Remove(Self);
      FManager.DoWindowUpdate;
    end;
    FManager := Value;
    if Assigned(FManager) then
    begin
      FManager.FList.Add(Self);
      FManager.DoSetWindow(Self);
      FManager.DoWindowUpdate;
    end;
  end;
end;

procedure TXN_Window.SetStayOnTop(const Value: Boolean);
begin
  FStayOnTop := Value;
  if FStayOnTop then
    SetWindowPos(Handle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE or SWP_NOMOVE or SWP_NOREDRAW or SWP_NOACTIVATE)
  else
    SetWindowPos(Handle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE or SWP_NOMOVE or SWP_NOREDRAW or SWP_NOACTIVATE);
end;

procedure TXN_Window.SetToAnimTop(const Value: Integer);
begin
  if not Visible then
  begin
    FToAnimTop := Value;
    AnimTopValue := Value
  end
  else if FToAnimTop <> Value then
  begin
    FToAnimTop := Value;
    FAnimTop.Stop;
    FAnimTop.ClearValues;
    FAnimTop.Values.AddValue(Top, FToAnimTop, 0.2);
    FAnimTop.Start;
  end;
end;

procedure TXN_Window.SetVisible(const Value: Boolean);
begin
  FVisible := Value;
  UpdateManagerBounds;
  if Visible then ShowWindow(Handle, SW_SHOWNOACTIVATE);
  inherited Visible := Value;
end;

procedure TXN_Window.SetWindowBounds(ALeft, ATop, AWidth, AHeight: Integer);
var
  t: Integer;
begin
  if FFirstWindowBounds then
  begin
    FAnimTopValue := ATop;
    FToAnimTop := ATop;
  end;
  FFirstWindowBounds := False;
  t := FAnimTopValue;
  SetBounds(ALeft, t, AWidth, AHeight);
  if (ATop <> t) then
    ToAnimTop := ATop;
end;

procedure TXN_Window.UpdateManagerBounds;
begin
  if Assigned(Manager) then
    Manager.UpdateWindowsBounds;
end;

procedure TXN_Window.InvalidateLayer;
var
  bmpR: TGPBitmap;
  g: TGPGraphics;
  bmp: TGPBitmap;
  bmp2: TGPBitmap;
  zoom: Single;
  offset: Single;
begin

  if not Assigned(Control) then
    Exit;
    
  bmp := TGPBitmap.Create(Width, Height, PixelFormat32bppARGB);
  bmpR := CreateRenderBitmap;
  try
    g := TGPGraphics.Create(bmp);
    try
      RenderShadow(g, Width, Height);
      g.DrawImage(bmpR, ShadowWidth, ShadowWidth);
    finally
      g.Free;
    end;

    zoom := 0.1;
    zoom := (zoom - (zoom * AnimCloseValue2)) + 0.9;
    offset := 1 * AnimCloseValue2;
    if zoom <> 1 then
    begin
      bmp2 := bmp;
      bmp := TGPBitmap.Create(bmp2.GetWidth, bmp2.GetHeight, PixelFormat32bppARGB);
      g := TGPGraphics.Create(bmp);
      try      
        g.TranslateTransform(
          ((bmp2.GetWidth - (bmp2.GetWidth * zoom)) / 2) + (offset * bmp2.GetWidth),
          (bmp2.GetHeight - (bmp2.GetHeight * zoom)) / 2
        );
        g.SetPageUnit(UnitPixel);
        g.ScaleTransform(zoom, zoom);
        g.DrawImage(bmp2, MakeRect(0, 0, bmp2.GetWidth, bmp2.GetHeight));                                                           
      finally
        g.Free;
        bmp2.Free;
      end;
    end;
    
    PaintToLayer(bmp);

    UpdateManagerBounds;

  finally
    bmp.Free;
    bmpR.Free;
  end;

end;

procedure TXN_Window.PaintToLayer(AImage: TGPBitmap);
var
  b: TBlendFunction;
  p: TPoint;
  s: TSize;
//  bmp: TBitmap;
  h: HBitmap;
  bmp: TBitmap;
begin

  if (GetWindowLong(Handle, GWL_EXSTYLE) and WS_EX_LAYERED) = 0 then
    SetWindowLong(Handle, GWL_EXSTYLE, GetWindowLong(Handle, GWL_EXSTYLE) or WS_EX_LAYERED);

  b.BlendOp := AC_SRC_OVER;
  b.BlendFlags := 0;
  b.SourceConstantAlpha := 255;
  b.AlphaFormat := AC_SRC_ALPHA;

  p := Point(0, 0);
  s.cx := AImage.GetWidth;
  s.cy := AImage.GetHeight;

  bmp := TBitmap.Create;
  try
    AImage.GetHBITMAP(0, h);
    bmp.Handle := h;
    UpdateLayeredWindow(Handle, 0, nil, @s, bmp.Canvas.Handle, @p, 0, @b, ULW_ALPHA);
  finally
    bmp.Free;
  end;

end;

{ TXN_ControlWindowed }

constructor TXN_ControlWindowed.Create(AOwner: TComponent;
  AWindow: TXN_Window);
begin
  FWindow := AWindow;
  inherited Create(AOwner);
end;

procedure TXN_ControlWindowed.DoCloseRequest;
begin
  inherited;
  Window.Close;
end;

procedure TXN_ControlWindowed.DoControlAnimHeight(AControl: TXN_ControlBase);
begin
  inherited;
  if Assigned(Window) then
    Window.UpdateManagerBounds;
end;

procedure TXN_ControlWindowed.PaintWindow(DC: HDC);
begin
  inherited;
  if Assigned(Window) then
    Window.InvalidateLayer
end;

function TXN_ControlWindowed.SelectDesign(AControl: TControl): Boolean;
begin
  Result := False;
  if AControl = FCloseButton then
    Exit;
  Result := Window.SelectDesign(AControl);
  CurrentSelect := AControl;
end;

{ TXN_Manager }

procedure TXN_Manager.Add(AWindow: TXN_Window);
begin
  AWindow.Manager := Self;
end;

constructor TXN_Manager.Create(AOwner: TComponent);
begin
  inherited;
  FHandleWindowParent := TXH_WinControl.Create(nil);
  FHandleWindowParent.OnResize := DoWindowParentResize;
  FStayOnTop := True;
  FEnableCustomRect := False;
  FList := TList.Create;
  FAlign := awRightBottom;
end;

destructor TXN_Manager.Destroy;
begin
  FHandleWindowParent.Free;
  FList.Free;
  inherited;
end;

procedure TXN_Manager.DoSetWindow(AWindow: TXN_Window);
begin
  AWindow.StayOnTop := Self.StayOnTop;
  Windows.SetParent(AWindow.Handle, Self.WindowParent);
end;

procedure TXN_Manager.DoWindowParentResize(Sender: TObject);
begin
  UpdateWindowsBounds;
end;

procedure TXN_Manager.DoWindowUpdate;
begin
  UpdateWindowsBounds;
end;

function TXN_Manager.FinalAreaRect: TRect;
begin
  if EnableCustomRect then
    Result := CustomRect
  else
    if WindowParent <> 0 then
    begin
      Windows.GetClientRect(WindowParent, Result);
    end else
      Result := Screen.WorkAreaRect;
end;

function TXN_Manager.GetWindow(AIndex: Integer): TXN_Window;
begin
  Result := TXN_Window(FList[AIndex]);
end;

function TXN_Manager.GetWindowsCount: Integer;
begin
  Result := FList.Count;
end;

procedure TXN_Manager.Remove(AWindow: TXN_Window);
begin
  AWindow.Manager := nil;
end;

procedure TXN_Manager.SendToEnd(AWindow: TXN_Window);
begin
  FList.Move(FList.IndexOf(AWindow), FList.Count - 1);
end;

procedure TXN_Manager.SetAlign(const Value: TXN_AlignWindow);
begin
  if FAlign <> Value then
  begin
    FAlign := Value;
    UpdateWindowsBounds;
  end;
end;

procedure TXN_Manager.SetCustomRect(const Value: TRect);
  function EqualRect(const r1, r2: TRect): Boolean;
  begin
    Result := (r1.Left=r2.Left) and (r1.Right=r2.Right) and
              (r1.Top=r2.Top) and (r1.Bottom=r2.Bottom);
  end;
begin
  if not EqualRect(FCustomRect, Value) then
  begin
    FCustomRect := Value;
    FEnableCustomRect := True;
    UpdateWindowsBounds;
  end;
end;

procedure TXN_Manager.SetEnableCustomRect(const Value: Boolean);
begin
  if FEnableCustomRect <> Value then
  begin
    FEnableCustomRect := Value;
    UpdateWindowsBounds;
  end;
end;

procedure TXN_Manager.SetWindowParent(const Value: THandle);
var
  I: Integer;
begin
  if FWindowParent <> Value then
  begin
    FWindowParent := Value;
    FHandleWindowParent.Handle := FWindowParent;
  end;
  for I := 0 to WindowsCount - 1 do
    Windows.SetParent(Window[I].Handle, Self.WindowParent);
//    Window[I].ParentWindow := Value;
end;

procedure TXN_Manager.SetStayOnTop(const Value: Boolean);
var
  I: Integer;
begin
  if FStayOnTop <> Value then
  begin
    FStayOnTop := Value;
    for I := 0 to WindowsCount - 1 do
      Window[I].StayOnTop := Value;
  end;
end;

procedure TXN_Manager.UpdateWindowsBounds;
const
  mrg = 10;
var
  p: TPoint;
  I: Integer;
  h: Integer;
  cnt: Integer;
  w: Integer;
  area: TRect;
begin

  area := FinalAreaRect;

  cnt := 0;
  for I := 0 to WindowsCount - 1 do
  begin

    w := Window[I].Width;
    h := Window[I].Control.InternalHeight;

    if not Window[I].Visible or (h < mrg) then
      Continue;

    p := Point(0, 0);

    case Align of
      awLeftTop: begin
        p.X := area.Left;
        p.Y := area.Top + cnt;
      end;
      awRightTop: begin
        p.X := area.Right - w;
        p.Y := area.Top + cnt;
      end;
      awLeftBottom: begin
        p.X := area.Left;
        p.Y := area.Bottom - cnt - h - 10;
      end;
      awRightBottom: begin
        p.X := area.Right - w;
        p.Y := area.Bottom - cnt - h - 10;
      end;
    end;

    Window[I].SetWindowBounds(p.X, p.Y, w, h);

    case Align of
      awLeftTop,
      awRightTop: begin
        cnt := Window[I].Top + Window[I].Control.Height;
      end;
      awLeftBottom,
      awRightBottom: begin
        Inc(cnt, h);
//        cnt := (area.Bottom - Window[I].Top);
      end;
    end;

//    Inc(cnt, h);
    Inc(cnt, mrg);

  end;
end;

{ TXN_Notify }

procedure TXN_Notify.Close;
begin
  Window.Close;
end;

constructor TXN_Notify.Create(AOwner: TComponent);
begin
  inherited;
  FManagerDesigner := TXN_Manager.Create(Self);
  FManager := TXN_Manager.Default;
  FStayOnTop := True;
  FWindow := TXN_NotifyWindow.Create(Self, Self);
  FWindow.OnSelectDesign := DoSelectWindowDesign;
  FWindow.BorderStyle := bsNone; // force
  FWindow.Control.InDesign := csDesigning in ComponentState;
  FWindow.SetDesigning(False);
  if Assigned(XND_DesignCreateNotify) then XND_DesignCreateNotify(Self);
end;

procedure TXN_Notify.DesignerClosed;
begin
end;

procedure TXN_Notify.DesignerOpened;
begin
end;

destructor TXN_Notify.Destroy;
begin
  FWindow.Free;
  FManagerDesigner.Free;
  inherited;
end;

function TXN_Notify.DoSelectWindowDesign(ASender: TObject;
  AObject: TControl): Boolean;
var
  c: TPersistent;
begin
  Result := False;
  if Assigned(XND_DesignSelectComponent) then
  begin
    c := Window.Control.GetDesignObjectOfOther(Self.Window.Control, AObject);
    XND_DesignSelectComponent(c);
    Result := Assigned(c);
  end;
end;

function TXN_Notify.GetButtons: TXN_ControlButtons;
begin
  Result := Window.Control.Buttons;
end;

function TXN_Notify.GetDuration: Integer;
begin
  Result := Window.Duration;
end;

function TXN_Notify.GetOptions: TXN_ControlOptions;
begin
  Result := Window.Control.Options;
end;

function TXN_Notify.GetPauseOnMouseEnter: Boolean;
begin
  Result := Window.PauseOnMouseEnter;
end;

procedure TXN_Notify.SetButtons(const Value: TXN_ControlButtons);
begin
  Window.Control.Buttons := Value;
end;

procedure TXN_Notify.SetDuration(const Value: Integer);
begin
  Window.Duration := Value;
end;

procedure TXN_Notify.SetManager(const Value: TXN_Manager);
begin
  FManager := Value;
  Window.Manager := FManager;
end;

procedure TXN_Notify.SetName(const NewName: TComponentName);
begin
  inherited;
  Window.Control.Name := Name + 'Control';
end;

procedure TXN_Notify.SetOptions(const Value: TXN_ControlOptions);
begin
  Window.Control.Options := Value;
end;

procedure TXN_Notify.SetPauseOnMouseEnter(const Value: Boolean);
begin
  PauseOnMouseEnter := Value;
end;

procedure TXN_Notify.SetStayOnTap(const Value: Boolean);
begin
  FStayOnTop := Value;
  Window.StayOnTop := FStayOnTop;
end;

procedure TXN_Notify.Show;
begin
  Window.StayOnTop := StayOnTop;
  Window.Manager := Manager;
  Window.Show;
end;

procedure TXN_Notify.ShowWindowDesigner;
begin
  Window.Manager := ManagerDesigner;
  Window.Show(-1);
end;

{ TXN_NotifyWindow }

class function TXN_NotifyWindow.Create(AOwner: TComponent;
  ANotify: TXN_Notify): TXN_NotifyWindow;
begin
  Result := TXN_NotifyWindow.CreateWindow(AOwner, ANotify);
  Result.InvalidateLayer;
end;

constructor TXN_NotifyWindow.CreateWindow(AOwner: TComponent;
  ANotify: TXN_Notify);
begin
  FNotify := ANotify;
  inherited CreateWindow(AOwner);
end;

procedure TXN_NotifyWindow.DoClose(var Action: TCloseAction);
begin
  inherited;
  if Assigned(Notify.OnClose) then
    Notify.OnClose(Self);
end;

procedure TXN_NotifyWindow.DoHide;
begin
  inherited;
  if Assigned(Notify.OnHide) then
    Notify.OnHide(Self);
end;

procedure TXN_NotifyWindow.DoShow;
begin
  inherited;
  if Assigned(Notify.OnShow) then
    Notify.OnShow(Self);
end;

initialization
  TXN_Manager.FDefault := TXN_Manager.Create(nil);

finalization
  TXN_Manager.FDefault.Free;

end.
