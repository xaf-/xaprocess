unit XAF.Anim;

{$IF CompilerVersion >= 20}
  {$DEFINE DELPHI2009_UP}
{$IFEND}
// S'hauria de afegir la opcio de Anim AutoFree per eliminar comudament (Necesari a Preview)

interface

uses
  Messages, Windows, Controls, Classes, SysUtils, ExtCtrls, XAF.RTTI, Graphics;

const
  DefaultDuration = 1;
  DefaultDelay = 0;
type

  TXA_EasingType = (xeLinearTween, xeInQuad, xeOutQuad,
    xeInOutQuad, xeInCubic, xeOutCubic, xeInOutCubic,
    xeInQuart, xeOutQuart, xeInOutQuart, xeInQuint,
    xeOutQuint, xeInOutQuint, xeInSine, xeOutSine,
    xeInOutSine, xeInExpo, xeOutExpo, xeInOutExpo,
    xeInElastic, xeOutElastic, xeInOutElastic, xeInCirc,
    xeOutCirc, xeInOutCirc);

  TXA_Anim = class;
  TXA_AnimValues = class;

  TXA_ValueType = (xvtAuto, xvtInteger, xvtFloat, xvtColor);

  TXA_AnimValue = class(TCollectionItem)
  private
    FTimer: TTimer;
    FValueStart: Variant;
    FValueEnd: Variant;
    FDelay: Single;
    FDuration: Single;
    FStartTime: TDateTime;
    FRunning, FStarted: Boolean;
    FSaveCurrent: Variant;
    FEasing: TXA_EasingType;
    FOnStep: TNotifyEvent;
    FOnComplete: TNotifyEvent;
    FDestroyed: Boolean;
    function GetCollection: TXA_AnimValues;
  protected
    procedure DoTimer(Sender: TObject);
    procedure DoStep;
    procedure DoComplete;
    function Calculate(APercent: Extended): Variant;
  public

    procedure Start;
    procedure Stop;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    property Collection: TXA_AnimValues read GetCollection;
    property ValueStart: Variant read FValueStart write FValueStart;
    property ValueEnd: Variant read FValueEnd write FValueEnd;
    property Delay: Single read FDelay write FDelay;
    property Duration: Single read FDuration write FDuration;
    property Running: Boolean read FRunning;
    property Easing: TXA_EasingType read FEasing write FEasing;
    property OnStep: TNotifyEvent read FOnStep write FOnStep;
    property OnComplete: TNotifyEvent read FOnComplete write FOnComplete;
  end;

  TXA_AnimValues = class(TCollection)
  private
    FOwner: TXA_Anim;
    function GetItems(AIndex: Integer): TXA_AnimValue;
    procedure SetItems(AIndex: Integer; const Value: TXA_AnimValue);
  public
    property Anim: TXA_Anim read FOwner;
    constructor Create(AOwner: TXA_Anim); reintroduce;
  protected
    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
  public
    property Items[AIndex: Integer]: TXA_AnimValue read GetItems write SetItems; default;
    function AddValue(AStart, AEnd: Variant; ADuration: Single = DefaultDuration; ADelay: Single = DefaultDelay): TXA_AnimValue;
    function Add: TXA_AnimValue; reintroduce;
  end;

  TXA_Anim = class(TComponent)
  private
    FScope: TObject;
    FValues: TXA_AnimValues;
    FScopePropertyName: String;
    FStartTime: TDateTime;
    FRTTIObject: IXR_Object;
    FRunning: Boolean;
    FValueType: TXA_ValueType;
    FDefaultEasing: TXA_EasingType;
    FOnComplete: TNotifyEvent;
    FOnStep: TNotifyEvent;
    FAutoFree: Boolean;
    function GetScopeComponent: TComponent;
    procedure SetScopeComponent(const Value: TComponent);
    procedure SetScope(const Value: TObject);
    procedure SetRunning(const Value: Boolean);
  protected
    procedure DoComplete;
    procedure DoStep;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure GoNext;
    procedure InternalStart;

    procedure SetValue(AValue: Variant);
    function GetValue: Variant;

  public

    class function Animate(AComp: TComponent; AProp: String; AStart, AEnd: Variant; ADuration: Single = DefaultDuration; ADelay: Single = DefaultDelay): TXA_Anim;

    procedure ClearValues;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Start;
    procedure Stop;

    property Values: TXA_AnimValues read FValues;
    property Scope: TObject read FScope write SetScope;
    property ValueType: TXA_ValueType read FValueType write FValueType;
    property Running: Boolean read FRunning write SetRunning;
    property AutoFree: Boolean read FAutoFree write FAutoFree;
  published
    property ScopeComponent: TComponent read GetScopeComponent write SetScopeComponent;
    property ScopePropertyName: String read FScopePropertyName write FScopePropertyName;
    property DefaultEasing: TXA_EasingType read FDefaultEasing write FDefaultEasing;
    property OnComplete: TNotifyEvent read FOnComplete write FOnComplete;
    property OnStep: TNotifyEvent read FOnStep write FOnStep;
  end;

  TXA_EasingPreview = class(TGraphicControl)
  private
    FAnim: TXA_Anim;
    FEasing: TXA_EasingType;
    FPercent: Integer;
    FAnimDuration: Single;
    procedure SetEasing(const Value: TXA_EasingType);
    function GetPen: TPen;
    procedure SetPen(const Value: TPen);
    procedure SetPercent(const Value: Integer);
    function GetAnimEnable: Boolean;
    procedure SetAnimEnable(const Value: Boolean);
    procedure AddAnim;
    procedure ResetAnim;
    procedure SetAnimDuration(const Value: Single);
  protected
    procedure DoAnimComplete(Sender: TObject);
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property AnimEnable: Boolean read GetAnimEnable write SetAnimEnable;
    property AnimDuration: Single read FAnimDuration write SetAnimDuration;
    property Easing: TXA_EasingType read FEasing write SetEasing;
    property Pen: TPen read GetPen write SetPen;
    property Percent: Integer read FPercent write SetPercent;
  end;

  function XA_CalculateEase(ACurrentTime, AStartValue, AChangeInValue, ADuration: Extended; AEaseType: TXA_EasingType): Extended; overload;
  function XA_CalculateEase(AStartPos, AEndPos, APositionPct: Extended; AEaseType: TXA_EasingType): Extended; overload;

implementation

uses
  GDIPObj, GDIPApi, DateUtils, Variants, Math, TypInfo;

function XA_CalculateEase(ACurrentTime, AStartValue, AChangeInValue, ADuration: Extended;
  AEaseType: TXA_EasingType): Extended;
var
  s, p, a: Extended;
begin
  Result := 0;
  case AEaseType of
    xeLinearTween:
      begin
        Result := AChangeInValue * ACurrentTime / ADuration + AStartValue;
      end;

    xeInQuad:
      begin
        ACurrentTime := ACurrentTime / ADuration;
        Result := AChangeInValue * ACurrentTime * ACurrentTime + AStartValue;
      end;

    xeOutQuad:
      begin
        ACurrentTime := ACurrentTime / ADuration;
        Result := -AChangeInValue * ACurrentTime * (ACurrentTime - 2) + AStartValue;
      end;

    xeInOutQuad:
      begin
        ACurrentTime := ACurrentTime / (ADuration / 2);

        if ACurrentTime < 1 then
          Result := AChangeInValue / 2 * ACurrentTime * ACurrentTime + AStartValue
        else
        begin
          ACurrentTime := ACurrentTime - 1;
          Result := -AChangeInValue / 2 * (ACurrentTime * (ACurrentTime - 2) - 1) +
            AStartValue;
        end;
      end;

    xeInCubic:
      begin
        ACurrentTime := ACurrentTime / ADuration;

        Result := AChangeInValue * ACurrentTime * ACurrentTime * ACurrentTime +
          AStartValue;
      end;

    xeOutCubic:
      begin
        ACurrentTime := (ACurrentTime / ADuration) - 1;

        Result := AChangeInValue * (ACurrentTime * ACurrentTime * ACurrentTime + 1)
          + AStartValue;
      end;

    xeInOutCubic:
      begin
        ACurrentTime := ACurrentTime / (ADuration / 2);

        if ACurrentTime < 1 then
          Result := AChangeInValue / 2 * ACurrentTime * ACurrentTime * ACurrentTime
            + AStartValue
        else
        begin
          ACurrentTime := ACurrentTime - 2;

          Result := AChangeInValue / 2 * (ACurrentTime * ACurrentTime * ACurrentTime
            + 2) + AStartValue;
        end;
      end;

    xeInQuart:
      begin
        ACurrentTime := ACurrentTime / ADuration;

        Result := AChangeInValue * ACurrentTime * ACurrentTime * ACurrentTime *
          ACurrentTime + AStartValue;
      end;

    xeOutQuart:
      begin
        ACurrentTime := (ACurrentTime / ADuration) - 1;

        Result := -AChangeInValue * (ACurrentTime * ACurrentTime * ACurrentTime *
          ACurrentTime - 1) + AStartValue;
      end;

    xeInOutQuart:
      begin
        ACurrentTime := ACurrentTime / (ADuration / 2);

        if ACurrentTime < 1 then
          Result := AChangeInValue / 2 * ACurrentTime * ACurrentTime * ACurrentTime
            * ACurrentTime + AStartValue
        else
        begin
          ACurrentTime := ACurrentTime - 2;

          Result := -AChangeInValue / 2 *
            (ACurrentTime * ACurrentTime * ACurrentTime * ACurrentTime - 2) +
            AStartValue;
        end;
      end;

    xeInQuint:
      begin
        ACurrentTime := ACurrentTime / ADuration;

        Result := AChangeInValue * ACurrentTime * ACurrentTime * ACurrentTime *
          ACurrentTime * ACurrentTime + AStartValue;
      end;

    xeOutQuint:
      begin
        ACurrentTime := (ACurrentTime / ADuration) - 1;

        Result := AChangeInValue * (ACurrentTime * ACurrentTime * ACurrentTime *
          ACurrentTime * ACurrentTime + 1) + AStartValue;
      end;

    xeInOutQuint:
      begin
        ACurrentTime := ACurrentTime / (ADuration / 2);
        if ACurrentTime < 1 then
          Result := AChangeInValue / 2 * ACurrentTime * ACurrentTime * ACurrentTime
            * ACurrentTime * ACurrentTime + AStartValue
        else
        begin
          ACurrentTime := ACurrentTime - 2;

          Result := AChangeInValue / 2 * (ACurrentTime * ACurrentTime * ACurrentTime
            * ACurrentTime * ACurrentTime + 2) + AStartValue;
        end;
      end;

    xeInSine:
      begin
        Result := -AChangeInValue * Cos(ACurrentTime / ADuration * (PI / 2)) +
          AChangeInValue + AStartValue;
      end;

    xeOutSine:
      begin
        Result := AChangeInValue * Sin(ACurrentTime / ADuration * (PI / 2)) +
          AStartValue;
      end;

    xeInOutSine:
      begin
        Result := -AChangeInValue / 2 * (Cos(PI * ACurrentTime / ADuration) - 1) +
          AStartValue;
      end;

    xeInExpo:
      begin
        Result := AChangeInValue * Power(2, 10 * (ACurrentTime / ADuration - 1)) +
          AStartValue;
      end;

    xeOutExpo:
      begin
        Result := AChangeInValue * (-Power(2, -10 * ACurrentTime / ADuration) + 1)
          + AStartValue;
      end;

    xeInOutExpo:
      begin
        ACurrentTime := ACurrentTime / (ADuration / 2);

        if ACurrentTime < 1 then
          Result := AChangeInValue / 2 * Power(2, 10 * (ACurrentTime - 1)) +
            AStartValue
        else
        begin
          ACurrentTime := ACurrentTime - 1;

          Result := AChangeInValue / 2 * (-Power(2, -10 * ACurrentTime) + 2) +
            AStartValue;
        end;
      end;

    xeInElastic:
      begin

        p := 0;
        a := AChangeInValue;

        if (ACurrentTime = 0) then
          Result := AStartValue
        else begin
          ACurrentTime := ACurrentTime / ADuration;
          if (ACurrentTime = 1) then
            Result := AStartValue + AChangeInValue
          else if (p = 0) then
          begin
            p := ADuration * 0.3;
            if (a < abs(AChangeInValue)) then
            begin
              a := AChangeInValue;
              s := p / 4;
            end else

              s := p /(2 * PI) * Math.ArcSin(AChangeInValue / a);
            ACurrentTime := ACurrentTime - 1;
            Result := -(a * Power(2,10 * ACurrentTime) * Sin((ACurrentTime * ADuration - s)*(2 * PI) / p)) + AStartValue;
          end;
        end;

      end;
    xeOutElastic :
      begin

        p := 0;
        a := AChangeInValue;

        if (ACurrentTime = 0) then
          Result := AStartValue
        else begin
          ACurrentTime := ACurrentTime / ADuration;
          if (ACurrentTime = 1) then
            Result := AStartValue + AChangeInValue
          else if (p = 0) then
          begin
            p := ADuration * 0.3;
            if (a < abs(AChangeInValue)) then
            begin
              a := AChangeInValue;
              s := p / 4;
            end else
              s := p /(2 * PI) * Math.ArcSin(AChangeInValue / a);
            Result := a * Power(2,-10 * ACurrentTime) * Sin((ACurrentTime * ADuration - s)*(2 * PI) / p) + AChangeInValue + AStartValue;
          end;
        end;

      end;
    xeInOutElastic:
      begin

        p := 0;
        a := AChangeInValue;

        if (ACurrentTime = 0) then
          Result := AStartValue
        else begin
          ACurrentTime := ACurrentTime / (ADuration / 2);
          if (ACurrentTime = 2) then
            Result := AStartValue + AChangeInValue
          else if (p = 0) then
          begin
            p := ADuration * (0.3 * 1.5);
            if (a < abs(AChangeInValue)) then
            begin
              a := AChangeInValue;
              s := p / 4;
            end else
              s := p /(2 * PI) * Math.ArcSin(AChangeInValue / a);

            if (ACurrentTime < 1) then
            begin
              ACurrentTime := ACurrentTime - 1;
              Result := -0.5 * (a * Power(2,10 * ACurrentTime) * sin( (ACurrentTime * ADuration - s)*(2 * PI) / p )) + AStartValue;
            end else begin
              ACurrentTime := ACurrentTime - 1;
              Result := a*Power(2,-10 * ACurrentTime) * sin( (ACurrentTime * ADuration - s)*(2 * PI)/p) * 0.5 + AChangeInValue + AStartValue;
            end;
          end;
        end;

      end;

    xeInCirc:
      begin
        ACurrentTime := ACurrentTime / ADuration;

        Result := -AChangeInValue * (Sqrt(1 - ACurrentTime * ACurrentTime) - 1) +
          AStartValue;
      end;

    xeOutCirc:
      begin
        ACurrentTime := (ACurrentTime / ADuration) - 1;

        Result := AChangeInValue * Sqrt(1 - ACurrentTime * ACurrentTime) +
          AStartValue;
      end;

    xeInOutCirc:
      begin
        ACurrentTime := ACurrentTime / (ADuration / 2);

        if ACurrentTime < 1 then
          Result := -AChangeInValue / 2 *
            (Sqrt(1 - ACurrentTime * ACurrentTime) - 1) + AStartValue
        else
        begin
          ACurrentTime := ACurrentTime - 2;

          Result := AChangeInValue / 2 * (Sqrt(1 - ACurrentTime * ACurrentTime) +
            1) + AStartValue;
        end;
      end;
  end;
end;

function XA_CalculateEase(AStartPos, AEndPos, APositionPct: Extended;
  AEaseType: TXA_EasingType): Extended;
var
  t, b, c, d: Extended;
begin
  c := AEndPos - AStartPos;
  d := 100;
  t := APositionPct;
  b := AStartPos;

  Result := XA_CalculateEase(t, b, c, d, AEaseType);
end;

{ TXA_Anim }

class function TXA_Anim.Animate(AComp: TComponent; AProp: String; AStart, AEnd: Variant; ADuration,
  ADelay: Single): TXA_Anim;
begin
  Result := TXA_Anim.Create(nil);
  Result.ScopeComponent := AComp;
  Result.ScopePropertyName := AProp;
  Result.AutoFree := True;
  Result.Values.AddValue(AStart, AEnd, ADuration, ADelay);
  Result.Start;
end;

procedure TXA_Anim.ClearValues;
begin
  while Values.Count > 0 do
    Values[0].Free;
end;

constructor TXA_Anim.Create(AOwner: TComponent);
begin
  inherited;
  FAutoFree := False;
  FDefaultEasing := xeLinearTween;
  FValueType := xvtAuto;
  FValues := TXA_AnimValues.Create(Self);
end;

destructor TXA_Anim.Destroy;
begin
  FRTTIObject := nil;
  FValues.Free;
  inherited;
end;

procedure TXA_Anim.DoComplete;
begin
  if Assigned(FOnComplete) then
    FOnComplete(Self);
  if FAutoFree then
    Free;
end;

procedure TXA_Anim.DoStep;
begin
  if Assigned(FOnStep) then
    FOnStep(Self);
end;

function TXA_Anim.GetScopeComponent: TComponent;
begin
  Result := nil;
  if Scope is TComponent then
    Result := Scope as TComponent;
end;

function TXA_Anim.GetValue: Variant;
begin
  Result := FRTTIObject.Properties[ScopePropertyName].Value;
end;

procedure TXA_Anim.GoNext;
begin
  if Values.Count > 0 then
  begin
    if FRunning then
      Values[0].Start;
  end else begin
    DoComplete;
  end;
end;

procedure TXA_Anim.InternalStart;
begin
  if Values.Count > 0 then
    Values[0].Start;
end;

procedure TXA_Anim.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) and (AComponent = ScopeComponent) then
    ScopeComponent := nil;
end;

procedure TXA_Anim.SetRunning(const Value: Boolean);
begin
  if Value <> FRunning then
  begin
    if Value then
      Start
    else
      Stop;
  end;
end;

procedure TXA_Anim.SetScope(const Value: TObject);
begin
  if FScope <> Value then
  begin
    if Assigned(FScope) then
      FRTTIObject := nil;
    FScope := Value;
    if Assigned(FScope) then
      FRTTIObject := TXR_Object.Create(Scope);
  end;
end;

procedure TXA_Anim.SetScopeComponent(const Value: TComponent);
begin
  Scope := Value;
end;

procedure TXA_Anim.SetValue(AValue: Variant);
begin
  if Assigned(FRTTIObject) then
  begin
    FRTTIObject.Properties[ScopePropertyName].Value := AValue;
  end;
end;

procedure TXA_Anim.Start;
begin
  FStartTime := Now;
  FRunning := True;
  InternalStart;
end;

procedure TXA_Anim.Stop;
begin
  FRunning := False;
  ClearValues;
end;

{ TXA_AnimValues }

function TXA_AnimValues.Add: TXA_AnimValue;
begin
  Result := inherited Add as TXA_AnimValue;
end;

function TXA_AnimValues.AddValue(AStart, AEnd: Variant; ADuration: Single; ADelay: Single): TXA_AnimValue;
begin
  Result := Add;
  Result.ValueStart := AStart;
  Result.ValueEnd := AEnd;
  Result.Delay := ADelay;
  Result.Duration := ADuration;
end;

constructor TXA_AnimValues.Create(AOwner: TXA_Anim);
begin
  FOwner := AOwner;
  inherited Create(TXA_AnimValue);
end;

function TXA_AnimValues.GetItems(AIndex: Integer): TXA_AnimValue;
begin
  Result := inherited Items[AIndex] as TXA_AnimValue;
end;

procedure TXA_AnimValues.Notify(Item: TCollectionItem;
  Action: TCollectionNotification);
begin
  inherited;
  if Action = cnAdded then
  begin
    if Anim.FRunning and (Count = 1) then
      Anim.InternalStart;
  end else begin
//    if TXA_AnimValue(Item).Running then
//      TXA_AnimValue(Item).Stop;
  end;
end;

procedure TXA_AnimValues.SetItems(AIndex: Integer; const Value: TXA_AnimValue);
begin
  inherited Items[AIndex] := Value;
end;

{ TXA_AnimValue }

function TXA_AnimValue.Calculate(APercent: Extended): Variant;

  function CalcInteger: Integer;
  var
    d: Extended;
  begin
    d := ValueEnd - ValueStart;
    Result := Round(ValueStart + (d * (APercent / 100)));
  end;

  function CalcFloat: Extended;
  var
    d: Extended;
  begin
    d := ValueEnd - ValueStart;
    Result := ValueStart + (d * (APercent / 100));
  end;

  function CalcColor: TColor;
    function CalcV(AStart, AEnd: Byte): Byte;
    var
      d: Integer;
      v: Extended;
    begin
      d := AEnd - AStart;
      v := AStart + (d * (APercent /  100));
      v := Max(0, v);
      v := Min(255, v);
      Result := Round(v);
    end;
  var
    sR, sG, sB, eR, eG, eB, b, g, r: Byte;
    c: TColor;
    e: Integer;
    s: Integer;
  begin

    s := ColorToRGB(ValueStart);
    e := ColorToRGB(ValueEnd);

    sR := GetRValue(s);
    sG := GetGValue(s);
    sB := GetBValue(s);

    eR := GetRValue(e);
    eG := GetGValue(e);
    eB := GetBValue(e);

    r := CalcV(sR, eR);
    g := CalcV(sG, eG);
    b := CalcV(sB, eB);

    c := RGB(
      r,
      g,
      b
    );
    Result := c;
  end;

var
  v: TXA_ValueType;
begin

  v := xvtAuto;
  if xvtAuto = Collection.Anim.ValueType then
  begin

    case VarType(FSaveCurrent) of
      varInteger,
      {$IFDEF DELPHI2009_UP}varUInt64,{$ENDIF}
      varInt64: v := xvtInteger;

      varSingle,
      varDouble,
      varCurrency: v := xvtFloat;
    end;

  end else
    v := Collection.Anim.ValueType;

  case v of
    // xvtAuto: ;

    xvtInteger: Result := CalcInteger;
    xvtFloat: Result := CalcFloat;
    xvtColor: Result := CalcColor;

  end;

end;

constructor TXA_AnimValue.Create(Collection: TCollection);
begin
  FDestroyed := False;
  FTimer := TTimer.Create(nil);
  FTimer.Enabled := False;
  FTimer.Interval := 10;
  FTimer.OnTimer := DoTimer;
  inherited;
  FEasing := Self.Collection.Anim.DefaultEasing;
  FDuration := 1;
  FDelay := 0;
end;

destructor TXA_AnimValue.Destroy;
begin
  FDestroyed := True;
//  if Running then Stop;
  FTimer.Free;
  inherited;
end;

procedure TXA_AnimValue.DoComplete;
begin
  Stop;
  if Assigned(FOnComplete) then
    FOnComplete(Self);
end;

procedure TXA_AnimValue.DoStep;
begin
  if Assigned(FOnStep) then
    FOnStep(Self);
  if Assigned(Collection) then
    Collection.Anim.DoStep;
end;

procedure TXA_AnimValue.DoTimer(Sender: TObject);
var
  percent: Extended;
  d: Extended;
  c: Extended;
begin

  if not Assigned(Collection.Anim.Scope) then Exit;

  if FRunning then
  begin


    d := MilliSecondsBetween(Now, FStartTime) / 1000;

    if d > Delay + Duration then
    begin
      Collection.Anim.SetValue(ValueEnd);
      DoComplete;
      DoStep;
      Exit;
    end;

    if d >= Delay then
    begin

      if not FStarted then
      begin
        FStarted := True;
      end;

      c := d - Delay;
      percent := XA_CalculateEase(c, 0, 100, Duration, FEasing);

      Collection.Anim.SetValue(Calculate(percent));
      DoStep;

    end;

  end;

end;

function TXA_AnimValue.GetCollection: TXA_AnimValues;
begin
  Result := inherited Collection as TXA_AnimValues;
end;

procedure TXA_AnimValue.Start;
begin
  if not FRunning then
  begin
    FStartTime := Now;
    if (not (csDestroying in Collection.Anim.ComponentState)) and Assigned(Collection.Anim.Scope) then
      FSaveCurrent := Collection.Anim.GetValue
    else
      FSaveCurrent := Null;
    FRunning := True;
    FTimer.Enabled := True;
  end;
end;

procedure TXA_AnimValue.Stop;
var
  a: TXA_Anim;
begin
  if FRunning then
  begin
    FRunning := False;
    FStarted := False;
    a := Collection.Anim;
    if not FDestroyed then Free;
    a.GoNext;
  end;
end;

{ TXA_EasingPreview }

constructor TXA_EasingPreview.Create(AOwner: TComponent);
begin
  inherited;
  FAnimDuration := 3;
  FAnim := TXA_Anim.Create(nil);
  FAnim.ScopeComponent := Self;
  FAnim.ScopePropertyName := 'Percent';
  FAnim.OnComplete := DoAnimComplete;
  AddAnim;
end;

destructor TXA_EasingPreview.Destroy;
begin
  FAnim.Free;
  inherited;
end;

procedure TXA_EasingPreview.ResetAnim;
begin
  FAnim.ClearValues;
  AddAnim;
end;

procedure TXA_EasingPreview.AddAnim;
begin
  FAnim.Values.AddValue(0, 100, AnimDuration);
end;

procedure TXA_EasingPreview.DoAnimComplete(Sender: TObject);
begin
  if AnimEnable then
    AddAnim;
end;

function TXA_EasingPreview.GetAnimEnable: Boolean;
begin
  Result := FAnim.Running
end;

function TXA_EasingPreview.GetPen: TPen;
begin
  Result := Canvas.Pen;
end;

procedure TXA_EasingPreview.Paint;
const
  rPoint = 4;
var
  g: TGPGraphics;
  Y, minY, maxY: Single;
  I, m: Integer;
  rP: Boolean;
  p: TGPPointF;
  points: array of TGPPointF;
  pn: TGPPen;
  rct: TGPRectF;
  br: TGPSolidBrush;
  p2: TGPPointF;
begin
  inherited;

  minY := MaxSingle;
  maxY := 0;

  g := TGPGraphics.Create(Canvas.Handle);
  try

    g.SetSmoothingMode(SmoothingModeHighQuality);

    rP := False;

    m := Width - (rPoint * 2);
    if m > 0 then
    begin

      p := MakePoint(0.0, 0);

      SetLength(points, m + 1);

      for I := 0 to m do
      begin

        Y := XA_CalculateEase(I, 0, Height, m, FEasing);
        Y := Height - Y;

        if I = 0 then
        begin
          p := MakePoint(I + rPoint, Y);
          p2 := MakePoint(I + rPoint, Y);
        end
        else
        begin
          p2 := MakePoint(I + rPoint, Y);
        end;

        points[I] := p2;

        minY := Min(minY, p2.Y);
        maxY := Max(maxY, p2.Y);

        if (not rP) and (I * 100 / m > Percent) then
        begin
          rP := True;
          p := MakePoint(I + rPoint, Y);
        end;

      end;

      if Length(points) > 0 then
      begin
        p := MakePoint(p.X, rPoint + ((Height - (rPoint * 2)) * ((p.Y - minY) / (maxY - minY))));
        for I := 0 to Length(points) - 1 do
        begin
          points[I] := MakePoint(points[I].X, rPoint + ((Height - (rPoint * 2)) * ((points[I].Y - minY) / (maxY - minY))));
        end;
      end;

      pn := TGPPen.Create(aclBlack);
      try
        g.DrawLines(pn, PGPPointF(points), Length(points));
      finally
        pn.Free;
      end;

      br := TGPSolidBrush.Create(aclRed);
      pn := TGPPen.Create(aclWhite, 2);
      try
        rct := MakeRect(p.X - rPoint, p.Y - rPoint, rPoint * 2, rPoint * 2);
        g.FillEllipse(br, rct);
        rct.X := rct.X + 2;
        rct.Y := rct.Y + 2;
        rct.Width := rct.Width - 4;
        rct.Height := rct.Height - 4;
        g.DrawEllipse(pn, rct);
      finally
        br.Free;
        pn.Free;
      end;

    end;

  finally
    g.Free;
  end;

end;

procedure TXA_EasingPreview.SetEasing(const Value: TXA_EasingType);
begin
  if FEasing <> Value then
  begin
    FEasing := Value;
    Invalidate;
    ResetAnim;
  end;
end;

procedure TXA_EasingPreview.SetAnimDuration(const Value: Single);
begin
  if FAnimDuration <> Value then
  begin
    FAnimDuration := Value;
    ResetAnim;
  end;
end;

procedure TXA_EasingPreview.SetAnimEnable(const Value: Boolean);
begin
  FAnim.Running := Value;
end;

procedure TXA_EasingPreview.SetPen(const Value: TPen);
begin
  Canvas.Pen := Value;
end;

procedure TXA_EasingPreview.SetPercent(const Value: Integer);
begin
  if FPercent <> Value then
  begin
    FPercent := Value;
    Invalidate;
  end;
end;

end.

