unit XAF.Handle;

interface

uses
  Messages, Windows, Controls, Classes, Types;

type
  TXH_WinControl = class(TComponent)
  private
    FProcInstance: Pointer;
    FOldWndProc: TFarProc;
    FHandle: THandle;
    FOnResize: TNotifyEvent;
    procedure SetHandle(const Value: THandle);
  protected
    procedure WMDestroy(var Message: TWMDestroy); message WM_DESTROY;
    procedure WndProc(var Message: TMessage);
    procedure WMWindowPosChanged(var Message: TWMWindowPosChanged); message WM_WINDOWPOSCHANGED;

    procedure Resize; virtual;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Handle: THandle read FHandle write SetHandle;

    property OnResize: TNotifyEvent read FOnResize write FOnResize;

  end;

implementation

{ TXH_Base }

constructor TXH_WinControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Handle := 0;
end;

destructor TXH_WinControl.Destroy;
begin
  Handle := 0;
  inherited;
end;

procedure TXH_WinControl.Resize;
begin
  if Assigned(FOnResize) then FOnResize(Self);
end;

procedure TXH_WinControl.SetHandle(const Value: THandle);
begin

  if FHandle <> Value then
  begin
    if Assigned(FProcInstance) then
    begin
      FreeObjectInstance(FProcInstance);
      SetWindowLong(Handle, GWL_WNDPROC, LONG_PTR(FOldWndProc));
    end;
    FHandle := Value;
    FProcInstance := nil;
    if FHandle <> 0 then
    begin
      FOldWndProc := TFarProc(GetWindowLong(Handle, GWL_WNDPROC));
      FProcInstance := MakeObjectInstance(WndProc);
      SetWindowLong(Handle, GWL_WNDPROC, LongInt(FProcInstance));
    end;
  end;

end;

procedure TXH_WinControl.WMDestroy(var Message: TWMDestroy);
begin
  Handle := 0;
end;

procedure TXH_WinControl.WMWindowPosChanged(var Message: TWMWindowPosChanged);
begin
  if (Message.WindowPos.flags and SWP_NOSIZE = 0) then
    Resize;
end;

procedure TXH_WinControl.WndProc(var Message: TMessage);
begin
  if Assigned(FOldWndProc) then
    Message.Result := CallWindowProc(FOldWndProc, Handle, Message.Msg, Message.wParam, Message.lParam);
  Dispatch(Message);
end;

end.
