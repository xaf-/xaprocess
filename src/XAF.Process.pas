unit XAF.Process;

{$IF CompilerVersion >= 20}
  {$DEFINE DELPHI2009_UP}
{$IFEND}

interface

uses
  Messages, SysUtils, Classes;

type

  TXP_Manager = class;
  TXP_Process = class;
  TXP_ProcessThread = class;
  TXP_Base = class;
  TXP_Thread = class;
  TXP_Handle = class;
  TXP_QueueItem = class;
  TXP_QueueList = class;
  TXP_Event = procedure (AProc: TXP_Process) of object;
  TXP_ChangeThreadEvent = procedure (AProc: TXP_ProcessThread) of object;
  TXP_EndEvent = procedure (AProc: TXP_Base) of object;
  TXP_ItemEndEvent = procedure (AItem: TXP_QueueItem) of object;
  TXP_ExceptionEvent = procedure (AProc: TXP_Base; AException: Exception; var AHandle: Boolean) of object;

  TXP_ProcessEvent = procedure (Sender: TObject; AProc: TXP_Process) of object;
  TXP_ProcessExceptionEvent = procedure (Sender: TObject; AProc: TXP_Process; AException: Exception; var AHandle: Boolean) of object;
  TXP_ChangeQueueEvent = procedure (Sender: TObject; AQueue: TXP_QueueList; AItem: TXP_QueueItem; AAction: TListNotification) of object;

  TXP_Process = class(TObject)
  private
    FMax: Extended;
    FPosition: Extended;
    FOnChange: TXP_Event;
    FLogData: TStrings;
    FInUpdate: Integer;
    FProcess: TXP_Base;
    FLevel: Integer;
    FLevelSizes, FLevelPositions, FLevelMax: array of Extended;
    procedure SetMax(Value: Extended);
    procedure SetPosition(Value: Extended);
    function GetLogLine: String;
    procedure SetLevel(Value: Integer);
    function GetDisplayPosition: Integer;
    function GetDisplayMax: Integer;
  protected
    procedure DoChange; virtual;
    procedure SyncDoChange;
  public

    procedure IncLevel(ASize: Extended);
    procedure DecLevel;

    procedure Syncronize(AMethod: TThreadMethod); overload; virtual;
    {$IFDEF DELPHI2009_UP}
    procedure Syncronize(AThreadProc: TThreadProcedure); overload; virtual;
    {$ENDIF}
    constructor Create(AProc: TXP_Base); virtual;
    destructor Destroy; override;
    property LogData: TStrings read FLogData;
    property Max: Extended read FMax write SetMax;
    property Position: Extended read FPosition write SetPosition;
    property DisplayPosition: Integer read GetDisplayPosition;
    property DisplayMax: Integer read GetDisplayMax;
    property OnChange: TXP_Event read FOnChange write FOnChange;
    procedure Log(AStr: String);
    procedure SetValues(ALog: String; APosition, AMax: Integer); overload;
    procedure SetValues(APosition, AMax: Integer); overload;
    property LogLine: String read GetLogLine;
    property Process: TXP_Base read FProcess;
    property Level: Integer read FLevel write SetLevel;

    procedure BeginUpdate;
    procedure EndUpdate;
  end;

  TXP_ProcessThread = class(TXP_Process)
  private
    FThread: TXP_Thread;
    FOnChange: TXP_ChangeThreadEvent;
  protected
    procedure DoChange; override;
    procedure SyncDoChangeThread;
  public
    constructor Create(AProc: TXP_Base; AThread: TXP_Thread); reintroduce; virtual;
    procedure Syncronize(AMethod: TThreadMethod); overload; override;
    {$IFDEF DELPHI2009_UP}
    procedure Syncronize(AThreadProc: TThreadProcedure); overload; override;
    {$ENDIF}
    property Thread: TXP_Thread read FThread;
    property OnChange: TXP_ChangeThreadEvent read FOnChange write FOnChange;
  end;

  TXP_Thread = class(TThread)
  private
    FProcess: TXP_Base;
    FTmpProc: TXP_ProcessThread;
    FOnProcessChange: TXP_Event;
    FOnProcessEnd: TXP_EndEvent;
    procedure AsyncProcess(AProc: TXP_ProcessThread);
    procedure DoSyncProcess;
    procedure SyncEnd;
  protected
    procedure Execute; override;

    constructor Create(AProc: TXP_Base; AChange: TXP_Event; AEndProc: TXP_EndEvent);
  public
    destructor Destroy; override;
    property Process: TXP_Base read FProcess;
    property OnProcessChange: TXP_Event read FOnProcessChange write FOnProcessChange;
    property OnProcessEnd: TXP_EndEvent read FOnProcessEnd write FOnProcessEnd;
  end;

  TXP_ID = record
    class operator Implicit(AValue: TXP_ID): Integer;
    case Integer of
      0: (
        LowID: SmallInt;
        HighID: SmallInt
      );
      1: (
        Id: Integer
      );
  end;

  TXP_Status = (xsStart, xsRunning, xsEnd);
  TXP_Base = class
  private
    FQueueItem: TXP_QueueItem;
    FManager: TXP_Manager;
    FInited: Boolean;
    FID: TXP_ID;
    FStatus: TXP_Status;
    FTerminated: Boolean;
    FOnException: TXP_ExceptionEvent;
    FCurrentHandle: Boolean;
    FCurrentProc: TXP_Process;
    FCurrentException: Exception;
    FOnFinish: TXP_EndEvent;
    function GetQueueGroup: String;
    procedure GenerateNewID;
    procedure InternalBefore;
    procedure InternalAfter;
    procedure SyncDoException;
    procedure SyncDoFinish;
  protected
    procedure Before; virtual;
    procedure After; virtual;
    // ! Sync/Async: Nom�s pot interactuar amb AProc!
    procedure InternalExecute(AProc: TXP_Process);
    procedure Init;
    property Terminated: Boolean read FTerminated;
    procedure Terminate;
    function DoException(AProc: TXP_Process; E: Exception): Boolean;
    procedure DoFinish(AProc: TXP_Process);
  public
    property OnFinish: TXP_EndEvent read FOnFinish write FOnFinish;
    property OnException: TXP_ExceptionEvent read FOnException write FOnException;
    constructor Create; virtual;
    procedure Execute(AProc: TXP_Process); virtual; abstract;
    property QueueItem: TXP_QueueItem read FQueueItem;
    property QueueGroup: String read GetQueueGroup;
    property Manager: TXP_Manager read FManager;
    property ID: TXP_ID read FID;
    property Status: TXP_Status read FStatus;
  end;

  TISP_SyncSessions = class(TXP_Base)

  end;

  TXP_QueueItem = class(TObject)
  private
    FIsThread: Boolean;
    FProcess: TXP_Base;
    FParent: TXP_QueueList;
    FDestroyed: Boolean;
    FOnEnd: TXP_ItemEndEvent;
    FIsRunning: Boolean;
    procedure DoProcessThreadEnd(AProc: TXP_Base);
    procedure DoEnd;
    function GetTerminated: Boolean;
  protected
    property Terminated: Boolean read GetTerminated;
    procedure Terminate;
  public
    property Destroyed: Boolean read FDestroyed;
    constructor Create(AParent: TXP_QueueList; AProcess: TXP_Base; AIsThread: Boolean);
    destructor Destroy; override;
    property Process: TXP_Base read FProcess;
    property IsThread: Boolean read FIsThread;
    property Parent: TXP_QueueList read FParent;
    procedure Run(AEnd: TXP_ItemEndEvent = nil);
    property OnEnd: TXP_ItemEndEvent read FOnEnd write FOnEnd;
    property IsRunning: Boolean read FIsRunning;

  end;

  TXP_QueueList = class(TList)
  private
    FInQueue: Boolean;
    FIsRunningQueue: Boolean;
    FParent: TXP_Manager;
    FName: String;
    FRunningItem: TXP_QueueItem;
    FHighID: Integer;
    procedure DoQueueItemEnd(AItem: TXP_QueueItem);
    function GetItem(AIndex: Integer): TXP_QueueItem;
  protected
    procedure DoQueueChange(AItem: TXP_QueueItem; Action: TListNotification);
    function Add(Item: Pointer): Integer; reintroduce;
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;
    function GetHighID: Integer;
  public
    property RunningItem: TXP_QueueItem read FRunningItem;
    constructor Create(AName: String; AParent: TXP_Manager); reintroduce;
    destructor Destroy; override;
    property Parent: TXP_Manager read FParent;
    procedure Next;
    procedure Start;
    procedure Stop;
    function AddProcessAsync(AProc: TXP_Base): TXP_QueueItem;
    function AddProcess(AProc: TXP_Base): TXP_QueueItem;
    property Name: String read FName;
    property Items[AIndex: Integer]: TXP_QueueItem read GetItem;
    procedure TerminateAll;
  end;

  TXP_Manager = class(TComponent)
  private
    FQueues: TList;
    FOnProcessChange: TXP_ProcessEvent;
    FListeners: TList;
    FOnQueueChange: TXP_ChangeQueueEvent;
    FLowID: Integer;
    FOnProcessFinished: TXP_Event;
    FOnProcessException: TXP_ProcessExceptionEvent;
    procedure SyncProcess(AProc: TXP_Process);
    function GetQueue(AName: String): TXP_QueueList;
    function GetDefaultQueue: TXP_QueueList;
    function GetListener(AIndex: Integer): TXP_Handle;
  protected
    function GetLowID: Integer;
    function ListenersCount: Integer;
    property Listener[AIndex: Integer]: TXP_Handle read GetListener;
    procedure DoProcessException(AProc: TXP_Process; E: Exception; var AHandle: Boolean);
    procedure DoProcessChange(AProc: TXP_Process);
    procedure DoProcessFinish(AProc: TXP_Process);
    procedure DoQueueChange(AQueue: TXP_QueueList; AItem: TXP_QueueItem; Action: TListNotification);
    procedure InitProcess(AProc: TXP_Base);
  public
    procedure ExecuteProcess(AProc: TXP_Base);
    procedure ExecuteProcessAsync(AProc: TXP_Base; AEndProc: TXP_EndEvent = nil);

    procedure AddListener(AObject: TXP_Handle);
    procedure RemoveListener(AObject: TXP_Handle);

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property OnProcessChange: TXP_ProcessEvent read FOnProcessChange write FOnProcessChange;
    property OnProcessException: TXP_ProcessExceptionEvent read FOnProcessException write FOnProcessException;

    property OnQueueChange: TXP_ChangeQueueEvent read FOnQueueChange write FOnQueueChange;
    property OnProcessFinished: TXP_Event read FOnProcessFinished write FOnProcessFinished;

    property DefaultQueue: TXP_QueueList read GetDefaultQueue;
    property Queue[AName: String]: TXP_QueueList read GetQueue;
    function IndexOfQueue(AName: String): Integer;

  end;

  TXP_Handle = class(TComponent)
  private
    FOnProcessChange: TXP_ProcessEvent;
    FOnQueueChange: TXP_ChangeQueueEvent;
    FQueueGroupName: String;
    FOnProcessException: TXP_ProcessExceptionEvent;
    FOnProcessFinish: TXP_ProcessEvent;
    procedure SetQueueGroupName(const Value: String);
  protected
    function ValidQueue(AItm: TXP_QueueItem): Boolean;
    procedure DoProcessChange(AProc: TXP_Process);
    procedure DoProcessFinish(AProc: TXP_Process);
    procedure DoQueueChange(AQueue: TXP_QueueList; AItem: TXP_QueueItem; Action: TListNotification);
    procedure DoProcessException(AProc: TXP_Process; E: Exception; var AHandle: Boolean);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property QueueGroupName: String read FQueueGroupName write SetQueueGroupName;
    property OnProcessChange: TXP_ProcessEvent read FOnProcessChange write FOnProcessChange;
    property OnProcessFinish: TXP_ProcessEvent read FOnProcessFinish write FOnProcessFinish;
    property OnProcessException: TXP_ProcessExceptionEvent read FOnProcessException write FOnProcessException;
    property OnQueueChange: TXP_ChangeQueueEvent read FOnQueueChange write FOnQueueChange;
  end;

var
  Manager: TXP_Manager;

implementation

uses
  Types, Forms, Math;

{ TISP_Base }

procedure TXP_Base.GenerateNewID;
begin
  FID.HighID := 0;
  if Assigned(QueueItem) then
    FID.HighID := QueueItem.Parent.GetHighID;
  FID.LowID := Manager.GetLowID;
end;

function TXP_Base.GetQueueGroup: String;
begin
  Result := '';
  if Assigned(QueueItem) and Assigned(QueueItem.Parent) then
    Result := QueueItem.Parent.Name;
end;

procedure TXP_Base.After;
begin
end;

procedure TXP_Base.Before;
begin
end;

constructor TXP_Base.Create;
begin
  FTerminated := False;
  FInited := False;
end;

function TXP_Base.DoException(AProc: TXP_Process; E: Exception): Boolean;
begin
  FCurrentHandle := False;
  FCurrentProc := AProc;
  FCurrentException := E;
  AProc.Syncronize(SyncDoException);
  Result := FCurrentHandle;
end;

procedure TXP_Base.DoFinish(AProc: TXP_Process);
begin
  FCurrentProc := AProc;
  AProc.Syncronize(SyncDoFinish);
end;

procedure TXP_Base.Init;
begin
  if not FInited then
  begin
    FInited := True;
    FManager := XAF.Process.Manager;
    GenerateNewID;
  end;
end;

procedure TXP_Base.InternalAfter;
begin
  FStatus := xsEnd;
  After;
end;

procedure TXP_Base.InternalBefore;
begin
  FStatus := xsStart;
  Before;
  FStatus := xsRunning;
end;

procedure TXP_Base.InternalExecute(AProc: TXP_Process);
begin
  try
    AProc.Max := 100;
    AProc.Position := 0;
    try
      Execute(AProc);
    finally
      AProc.Position := AProc.Max;
      DoFinish(AProc);
    end;
  except
    on e:Exception do
      if not DoException(AProc, e) then
        raise;
  end;
end;

procedure TXP_Base.SyncDoException;
begin
  Manager.DoProcessException(FCurrentProc, FCurrentException, FCurrentHandle);
  if (not FCurrentHandle) and Assigned(FOnException) then
  begin
    FOnException(Self, FCurrentException, FCurrentHandle);
  end;
end;

procedure TXP_Base.SyncDoFinish;
begin
  if Assigned(FOnFinish) then
    FOnFinish(Self);
  Manager.DoProcessFinish(FCurrentProc);
end;

procedure TXP_Base.Terminate;
begin
  FTerminated := True;
end;

{ TInlinService }

procedure TXP_Manager.AddListener(AObject: TXP_Handle);
begin
  FListeners.Add(AObject);
end;

constructor TXP_Manager.Create(AOwner: TComponent);
begin
  inherited;
  FLowID := 0;
  FQueues := TList.Create;
  FListeners := TList.Create;
end;

destructor TXP_Manager.Destroy;
var
  I: Integer;
begin
  for I := 0 to FQueues.Count - 1 do
    TXP_QueueList(FQueues[I]).Free;
  FQueues.Free;
  FListeners.Free;
  inherited;
end;

procedure TXP_Manager.ExecuteProcess(AProc: TXP_Base);
var
  p: TXP_Process;
begin

  InitProcess(AProc);

  p := TXP_Process.Create(AProc);
  AProc.InternalBefore;
  try
    p.OnChange := SyncProcess;
    AProc.InternalExecute(p);
  finally
    AProc.InternalAfter;
    p.Free;
  end;
end;

procedure TXP_Manager.ExecuteProcessAsync(AProc: TXP_Base; AEndProc: TXP_EndEvent);
begin
  Manager.InitProcess(AProc);
  TXP_Thread.Create(AProc, SyncProcess, AEndProc);
end;

function TXP_Manager.GetDefaultQueue: TXP_QueueList;
begin
  Result := Queue['default'];
end;

function TXP_Manager.GetQueue(AName: String): TXP_QueueList;
var
  num: INteger;
begin
  num := IndexOfQueue(AName);
  if num > -1 then
    Result := TXP_QueueList(FQueues[num])
  else begin
    Result := TXP_QueueList.Create(AName, Self);
    FQueues.Add(Result);
    Result.Start;
  end;
end;

function TXP_Manager.GetListener(AIndex: Integer): TXP_Handle;
begin
  Result := TXP_Handle(FListeners[AIndex]);
end;

function TXP_Manager.GetLowID: Integer;
begin
  Inc(FLowID);
  Result := FLowID;
end;

function TXP_Manager.IndexOfQueue(AName: String): Integer;
var
  I: Integer;
begin
  AName := LowerCase(AName);
  Result := -1;
  for I := 0 to FQueues.Count - 1 do
    if LowerCase(TXP_QueueList(FQueues[I]).Name) = AName then
    begin
      Result := I;
      Exit;
    end;
end;

procedure TXP_Manager.InitProcess(AProc: TXP_Base);
begin
  AProc.Init;
end;

function TXP_Manager.ListenersCount: Integer;
begin
  Result := FListeners.Count;
end;

procedure TXP_Manager.RemoveListener(AObject: TXP_Handle);
begin
  FListeners.Remove(AObject);
end;

procedure TXP_Manager.DoProcessChange(AProc: TXP_Process);
var
  I: Integer;
begin
  I := FListeners.Count - 1;
  while I >= 0 do
  begin
    Listener[I].DoProcessChange(AProc);
    Dec(I);
    if I >= FListeners.Count then
      I := FListeners.Count - 1;
  end;

  if Assigned(FOnProcessChange) then
    FOnProcessChange(Self, AProc);

end;

procedure TXP_Manager.DoProcessException(AProc: TXP_Process; E: Exception;
  var AHandle: Boolean);
var
  I: Integer;
begin

  I := FListeners.Count - 1;
  while I >= 0 do
  begin
    Listener[I].DoProcessException(AProc, E, AHandle);
    if AHandle then
      Exit;
    Dec(I);
    if I >= FListeners.Count then
      I := FListeners.Count - 1;
  end;

  if Assigned(FOnProcessException) then
    FOnProcessException(Self, AProc, E, AHandle);

end;

procedure TXP_Manager.DoProcessFinish(AProc: TXP_Process);
var
  I: Integer;
begin

  I := FListeners.Count - 1;
  while I >= 0 do
  begin
    Listener[I].DoProcessFinish(AProc);
    Dec(I);
    if I >= FListeners.Count then
      I := FListeners.Count - 1;
  end;

  if Assigned(FOnProcessFinished) then
    FOnProcessFinished(AProc);
end;

procedure TXP_Manager.DoQueueChange(AQueue: TXP_QueueList; AItem: TXP_QueueItem;
  Action: TListNotification);
var
  I: Integer;
begin

  I := FListeners.Count - 1;
  while I >= 0 do
  begin
    Listener[I].DoQueueChange(AQueue, AItem, Action);
    Dec(I);
    if I >= FListeners.Count then
      I := FListeners.Count - 1;
  end;

  if Assigned(FOnQueueChange) then
    FOnQueueChange(Self, AQueue, AItem, Action);
end;

procedure TXP_Manager.SyncProcess(AProc: TXP_Process);
begin
  DoProcessChange(AProc);
end;

{ TISP_Process }

procedure TXP_Process.BeginUpdate;
begin
  Inc(FInUpdate);
end;

constructor TXP_Process.Create(AProc: TXP_Base);
begin
  SetLength(FLevelMax, 0);
  SetLength(FLevelSizes, 0);
  SetLength(FLevelPositions, 0);
  FLevel := 0;
  FProcess := AProc;
  FInUpdate := 0;
  FLogData := TStringList.Create;
end;

procedure TXP_Process.DecLevel;
begin
  if Level = 0 then Exit;

  BeginUpdate;
  try
    Max := FLevelMax[Level-1];
    Position := FLevelPositions[Level-1] + FLevelSizes[Level-1];
    Level := Level - 1;

    SetLength(FLevelSizes, Level);
    SetLength(FLevelPositions, Level);
    SetLength(FLevelMax, Level);

  finally
    EndUpdate;
  end;

end;

destructor TXP_Process.Destroy;
begin
  FLogData.Free;
  inherited;
end;

procedure TXP_Process.DoChange;
begin
  if FInUpdate > 0 then
    Exit;
  Syncronize(SyncDoChange);
end;

procedure TXP_Process.EndUpdate;
begin
  Dec(FInUpdate);
  if FInUpdate = 0 then
    DoChange;
end;

function TXP_Process.GetDisplayMax: Integer;
begin
  if Level > 0 then
    Result := Round(FLevelMax[0] * 1000)
  else
    Result := Round(Max * 1000);
end;

function TXP_Process.GetDisplayPosition: Integer;
var
  r, p: Extended;
  I: Integer;
  v: Extended;
begin

  if Level = 0 then
  begin
    Result := Round(Position * 1000);
    Exit;
  end;

  r := 1;
  p := 0;

  for I := 0 to Level - 1 do
  begin

    if I = 0 then
    begin
      p := FLevelPositions[I];
      r := 1;
    end else begin
      v := (FLevelPositions[I] / FLevelMax[I]);
      p := p + (FLevelSizes[I-1] * v) *  r;
      r := r / FLevelMax[I];
    end;

  end;

  p := p + FLevelSizes[Level - 1] * (Position / Max) * r;

  Result := Round(p * 1000);

end;

function TXP_Process.GetLogLine: String;
begin
  Result := '';
  if FLogData.Count > 0 then
    Result := FLogData[FLogData.Count - 1];
end;

procedure TXP_Process.IncLevel(ASize: Extended);
begin
  SetLength(FLevelSizes, Level + 1);
  SetLength(FLevelPositions, Level + 1);
  SetLength(FLevelMax, Level + 1);
  FLevelSizes[Level] := ASize;
  FLevelPositions[Level] := Position;
  FLevelMax[Level] := Max;
  Level := Level + 1;
end;

procedure TXP_Process.Log(AStr: String);
begin
  FLogData.Add(AStr);
  DoChange;
end;

procedure TXP_Process.SetMax(Value: Extended);
begin
  Value := Math.Max(1, Value);
  if FMax <> Value then
  begin
    FMax := Value;
    FPosition := Math.Min(FPosition, Max);
    DoChange;
  end;
end;

procedure TXP_Process.SetPosition(Value: Extended);
begin
  Value := Math.Min(Value, Max);
  if FPosition <> Value then
  begin
    FPosition := Value;
    DoChange;
  end;
end;

procedure TXP_Process.SetLevel(Value: Integer);
begin
  if FLevel <> Value then
  begin
    Value := Math.Max(Value, 0);
    FLevel := Value;
  end;
end;

procedure TXP_Process.SetValues(ALog: String; APosition, AMax: Integer);
begin
  BeginUpdate;
  try
    Self.Position := APosition;
    Self.Max := AMax;
    Self.Log(ALog);
  finally
    EndUpdate;
  end;
end;

procedure TXP_Process.SetValues(APosition, AMax: Integer);
begin
  BeginUpdate;
  try
    Self.Position := APosition;
    Self.Max := AMax;
  finally
    EndUpdate;
  end;
end;

{$IFDEF DELPHI2009_UP}
procedure TXP_Process.Syncronize(AThreadProc: TThreadProcedure);
begin
  AThreadProc();
end;
{$ENDIF}

procedure TXP_Process.SyncDoChange;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TXP_Process.Syncronize(AMethod: TThreadMethod);
begin
  AMethod();
end;

{ TISP_Thread }

procedure TXP_Thread.AsyncProcess(AProc: TXP_ProcessThread);
begin
  FTmpProc := AProc;
  Synchronize(DoSyncProcess);
end;

constructor TXP_Thread.Create(AProc: TXP_Base; AChange: TXP_Event; AEndProc: TXP_EndEvent);
begin
  FProcess := AProc;
  FreeOnTerminate := True;
  FOnProcessChange := AChange;
  FOnProcessEnd := AEndProc;
  inherited Create(False);
end;

destructor TXP_Thread.Destroy;
begin
  FProcess.Free;
  inherited;
end;

procedure TXP_Thread.DoSyncProcess;
begin
  if Assigned(FOnProcessChange) then
    FOnProcessChange(FTmpProc);
end;

procedure TXP_Thread.Execute;
var
  p: TXP_ProcessThread;
begin
  inherited;
  p := TXP_ProcessThread.Create(Process, Self);
  Synchronize(Process.InternalBefore);
  try
    p.OnChange := AsyncProcess;
    Process.InternalExecute(p);
  finally
    Synchronize(Process.InternalAfter);
    p.Free;
  end;
  Synchronize(SyncEnd);
end;

procedure TXP_Thread.SyncEnd;
begin
  if Assigned(FOnProcessEnd) then
    FOnProcessEnd(Process);
end;

{ TISP_ProcessThread }

constructor TXP_ProcessThread.Create(AProc: TXP_Base; AThread: TXP_Thread);
begin
  FThread := AThread;
  inherited Create(AProc);
end;

procedure TXP_ProcessThread.DoChange;
begin
  if FInUpdate > 0 then
    Exit;
  inherited;
  if Assigned(FOnChange) then
  Syncronize(SyncDoChangeThread);
end;

{$IFDEF DELPHI2009_UP}
procedure TXP_ProcessThread.Syncronize(AThreadProc: TThreadProcedure);
begin
  Thread.Synchronize(AThreadProc);
end;
{$ENDIF}

procedure TXP_ProcessThread.SyncDoChangeThread;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TXP_ProcessThread.Syncronize(AMethod: TThreadMethod);
begin
  Thread.Synchronize(AMethod);
end;

{ TIS_QueueItem }

constructor TXP_QueueItem.Create(AParent: TXP_QueueList; AProcess: TXP_Base; AIsThread: Boolean);
begin
  FDestroyed := False;
  FParent := APArent;
  FProcess := AProcess;
  FProcess.FQueueItem := Self;
  FIsThread := AIsThread;
  inherited Create();
  Parent.Add(Self);
  Manager.InitProcess(FProcess);
end;

destructor TXP_QueueItem.Destroy;
begin
  // Thread always Free
  if not IsThread and Assigned(FProcess) then
    FreeAndNil(FProcess);
  Parent.Remove(Self);
  FDestroyed := True;
  inherited;
end;

procedure TXP_QueueItem.DoProcessThreadEnd(AProc: TXP_Base);
begin
  DoEnd;
end;

function TXP_QueueItem.GetTerminated: Boolean;
begin
  Result := Process.Terminated;
end;

procedure TXP_QueueItem.Run(AEnd: TXP_ItemEndEvent);
begin

  FIsRunning := True;

  if Assigned(AEnd) then
    FOnEnd := AEnd;

  if IsThread then
    Parent.Parent.ExecuteProcessAsync(Process, DoProcessThreadEnd)
  else begin
    try
      Parent.Parent.ExecuteProcess(Process);
    finally
      DoEnd;
    end;
  end;
end;

procedure TXP_QueueItem.Terminate;
begin
  Process.Terminate;
end;

procedure TXP_QueueItem.DoEnd;
begin
  FIsRunning := False;
  Free;
  if Assigned(FOnEnd) then
    FOnEnd(Self);
end;

{ TIS_QueueList }

function TXP_QueueList.Add(Item: Pointer): Integer;
begin
  Result := inherited Add(Item);
end;

function TXP_QueueList.AddProcess(AProc: TXP_Base): TXP_QueueItem;
var
  n: Boolean;
begin
  n := FInQueue and (Count = 0);
  Result := TXP_QueueItem.Create(Self, AProc, False);
  if n then Next;
end;

procedure TXP_QueueList.TerminateAll;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
  begin
    Items[I].Terminate;
  end;
end;

function TXP_QueueList.AddProcessAsync(AProc: TXP_Base): TXP_QueueItem;
var
  n: Boolean;
begin
  n := FInQueue and (Count = 0);
  Result := TXP_QueueItem.Create(Self, AProc, True);
  if n then Next;
end;

constructor TXP_QueueList.Create(AName: String; AParent: TXP_Manager);
begin
  FHighID := 0;
  FName := AName;
  FIsRunningQueue := False;
  FInQueue := False;
  FParent := AParent;
  inherited Create();
end;

destructor TXP_QueueList.Destroy;
begin
  TerminateAll;
  inherited;
end;

procedure TXP_QueueList.DoQueueChange(AItem: TXP_QueueItem;
  Action: TListNotification);
begin
  Parent.DoQueueChange(Self, AItem, Action);
end;

procedure TXP_QueueList.DoQueueItemEnd(AItem: TXP_QueueItem);
begin
  FIsRunningQueue := False;
  FRunningItem := nil;
  if FInQueue then
    Next;
end;

function TXP_QueueList.GetItem(AIndex: Integer): TXP_QueueItem;
begin
  Result := TXP_QueueItem(inherited Items[AIndex]);
end;

function TXP_QueueList.GetHighID: Integer;
begin
  Inc(FHighID);
  Result := FHighID;
end;

procedure TXP_QueueList.Next;
begin
  if Count > 0 then
  begin
    FIsRunningQueue := True;
    FRunningItem := Items[0];
    Items[0].Run(DoQueueItemEnd);
  end;
end;

procedure TXP_QueueList.Notify(Ptr: Pointer; Action: TListNotification);
var
  itm: TXP_QueueItem;
begin
  inherited;

  itm := TXP_QueueItem(Ptr);
  DoQueueChange(itm, Action);

end;

procedure TXP_QueueList.Start;
begin
  FInQueue := True;
  if not FIsRunningQueue then
    Next;
end;

procedure TXP_QueueList.Stop;
begin
  FInQueue := False;
end;

{ TXP_Handle }

constructor TXP_Handle.Create(AOwner: TComponent);
begin
  inherited;
  Manager.AddListener(Self);
end;

destructor TXP_Handle.Destroy;
begin
  Manager.RemoveListener(Self);
  inherited;
end;

procedure TXP_Handle.DoProcessChange(AProc: TXP_Process);
begin
  if Assigned(FOnProcessChange) and ValidQueue(AProc.Process.QueueItem) then
    FOnProcessChange(Self, AProc);
end;

procedure TXP_Handle.DoProcessException(AProc: TXP_Process; E: Exception;
  var AHandle: Boolean);
begin
  if Assigned(FOnProcessException) and ValidQueue(AProc.Process.QueueItem) then
    FOnProcessException(Self, AProc, E, AHandle);
end;

procedure TXP_Handle.DoProcessFinish(AProc: TXP_Process);
begin
  if Assigned(FOnProcessFinish) and ValidQueue(AProc.Process.QueueItem) then
    FOnProcessFinish(Self, AProc);
end;

procedure TXP_Handle.DoQueueChange(AQueue: TXP_QueueList; AItem: TXP_QueueItem;
  Action: TListNotification);
begin
  if Assigned(FOnQueueChange) and ValidQueue(AItem) then
    FOnQueueChange(Self, AQueue, AItem, Action);
end;

procedure TXP_Handle.SetQueueGroupName(const Value: String);
begin
  FQueueGroupName := Value;
end;

function TXP_Handle.ValidQueue(AItm: TXP_QueueItem): Boolean;
begin
  Result := (FQueueGroupName = '') or (AItm.Parent.Name = FQueueGroupName);
end;

{ TXP_ID }

class operator TXP_ID.Implicit(AValue: TXP_ID): Integer;
begin
  Result := AValue.Id;
end;

initialization
  Manager := TXP_Manager.Create(nil);

finalization
  Manager.Free;

end.
